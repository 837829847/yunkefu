/**
 * {host}	服务器host，如 http://kefu.xxx.com
 * {socketUrl} 	socketurl，如： ws://119.3.209.5:8081
 * {zuoxiid}	客服坐席的id，跟谁聊天，这里就是谁的id
 * {color}  文字图片颜色
 * {backgroundColor} 背景颜色
 * {autoHelloTime} 对应kefu.autoHelloTime
 * {autoHelloText} 对应kefu.autoHelloText
 */

var kefu_site_color = '{color}';
var kefu_site_backgroundColor = '{backgroundColor}';
var kefu_site_host = '{host}';
var kefu_site_socketUrl = '{socketUrl}';
var kefu_zuoxi_id = '{zuoxiid}';
var kefu_autoHelloTime = {autoHelloTime};
var kefu_autoHelloText = '{autoHelloText}';

//加载kefu资源
function loadKefu(){
	var head0 = document.getElementsByTagName('head')[0];

	/* 引入js */
	var msgScript = document.createElement("script");  //创建一个script标签
	msgScript.type = "text/javascript";
	msgScript.src = 'https://res.zvo.cn/msg/msg.js';
	head0.appendChild(msgScript);

	var kefuScript = document.createElement("script");  //创建一个script标签
	kefuScript.type = "text/javascript";
	kefuScript.src = 'https://res.zvo.cn/kefu/kefu.js';
	head0.appendChild(kefuScript);

	/* 引入css */
	/*
	var styleSss = document.createElement('link');
	styleSss.type='text/css';
	styleSss.rel = 'stylesheet';
	styleSss.href = 'https://res.zvo.cn/kefu/css/style.css';
	head0.appendChild(styleSss);
	*/
	

	/* 创建list、chat 的div */
	var listDiv = document.createElement('div');
	listDiv.id = 'list';
	listDiv.style.display = 'none';
	document.body.appendChild(listDiv);

	var chatDiv = document.createElement('div');
	chatDiv.id = 'chat';
	chatDiv.style.position = 'fixed';
	chatDiv.style.top = '0px';
	chatDiv.style.zIndex = '99999999';
	document.body.appendChild(chatDiv);

	var onlineKefuInterval = setInterval(function(){
		if(typeof(kefu) != 'undefined'){
			
			kefu.util.loadCss('https://res.zvo.cn/kefu/css/style.css');
			
			//在 kefu.api.domain 设置之上，加载 KefuJsInterface 接口扩展的插件的 jsFile ， 下面这个注释会被动态替换为插件要引入的jsFile，不要删除。比如会被替换为 try{ kefu.util.synchronizesLoadJs('http://www.zvo.cn/xxxxxxx.js'); }catch(e){ console.log(e); }
			//{KefuJsInterface.kefuJsAppendJsFile}
			
			kefu.api.domain = kefu_site_host; //http://kefu.wfyuntu.com
			kefu.socket.url = kefu_site_socketUrl; //ws://119.3.209.5:8081
			
			//在 initKefu() 之上，加载 KefuJsInterface 接口扩展的插件的 jsCode
			//{KefuJsInterface.kefuJsAppendJsCode}
			
			initKefu();
			clearInterval(onlineKefuInterval);//停止
	    	console.log('onlineKefuInterval stop');
		}		
	}, 500);
		
}


function initKefu(){

	//kefu.js 接口设置
	//kefu.api.getMyUser = host+'/kefu/chat/user/init.json';
	//kefu.api.getChatOtherUser = host+'/kefu/chat/zuoxi/getUserByZuoxiId.json';
	//kefu.api.chatLog = host+'/kefu/chat/log/log.json';
	//kefu.api.uploadImage = host+'/kefu/chat/file/uploadImage.json';

	kefu.extend.onlineKefu = {};

	kefu.ui.list.renderAreaId = 'list';
	kefu.ui.chat.renderAreaId = 'chat';

	if(kefu.client.isMobile()){
		//手机
		kefu.mode='mobile';
		kefu.extend.onlineKefu.initChat = function(){
			document.getElementById('back').onclick = function(){
				document.getElementById('chat').innerHTML = '';
				kefu.ui.list.entry();
				document.getElementsByTagName('html')[0].style.fontSize = originalHtmlSize;
			};
			
			//计算rem，免得有的手机看起来文字非常小
			var kefu_html = document.getElementsByTagName('html')[0];
			var originalHtmlSize = kefu_html.style.fontSize;	//当前网页本身原始的font-size
			//375调试的正常宽度   16是调试的字体大小
			var kefu_font_size = kefu_html.offsetWidth/375*16;
			//设置rem - px 比例
			kefu_html.style.fontSize = kefu_font_size+'px';
		};
		
		//在线客服的显示位置，更靠右下角一点
		wangmarket_site_kefu_zuoxi.style.right = '1rem';
		wangmarket_site_kefu_zuoxi.style.bottom = '1rem';
	}else{
		//pc
		kefu.mode='pc';
		kefu.ui.chat.html = kefu.ui.chat.pc.html;

		/* pc chat */
		kefu.extend.onlineKefu={
			initChat:function(){
				kefu.chat.shuruType = 'jianpan'
				kefu.chat.shuruTypeChange();
				
				kefu.ui.chat.pc.moveInit();	//鼠标移动
				//kefu.ui.chat.pc.sizeChange.moveInit();	//拖动大小
				document.getElementById('close').innerHTML = kefu.ui.images.close.replace(/{color}/g,kefu.extendIconColor);
			}
		};
		
		/* 重写 图片插件的放大方法,点击后直接在新窗口打开 */
		kefu.extend.image.fullScreen = function(imagesUrl){
			window.open(imagesUrl);
		}
	}

	//kefu.js 初始化
	kefu.init();

	//重写socket的消息接收
	var oldSocketOnMessage = kefu.socket.onmessage;
	kefu.socket.onmessage = function(res){
		var messageJson = JSON.parse(res.data);
		if(messageJson.type == 'MSG'){
			//如果是普通的沟通消息，那么打开聊天窗口
			if(kefu.currentPage != 'chat'){
				kefu.ui.chat.entry(kefu_zuoxi_id);
			}
		}
		oldSocketOnMessage(res);
	}
	
}

//调起回话	
//kefu.ui.chat.entry('403');

function showKefu(){
	if(typeof(kefu) == 'undefined'){
		loadKefu();
	}

	var kefuhtml = `
			<div>	
		    <!--?xml version="1.0" standalone="no"?--><svg t="1605358813111" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3142" xmlns:xlink="http://www.w3.org/1999/xlink" width="200" height="200" style="
		    width: 4rem;
		    height: 4rem;
		"><defs><style type="text/css"></style></defs><path d="M866.575086 434.735265c0-194.090487-180.02412-351.989703-401.315108-351.989702-221.263359 0-401.287479 157.899216-401.287479 351.989702 0 108.500133 57.523111 211.490785 154.545842 277.869589v75.30408c0 22.018479 12.637832 42.795689 31.818682 52.710502 8.990768 4.88731 19.114336 7.465019 29.237905 7.465019 12.141528 0 23.873734-3.490497 32.497135-9.479909l46.337351-30.357402c1.043772-0.651846 2.563382-1.61887 4.28049-3.039219l35.759435-23.536043c22.444175 3.356444 44.890396 5.053086 66.809615 5.053086 221.292012-0.001023 401.316132-157.898193 401.316132-351.989703zM465.266118 740.620931c-22.310122 0-45.252647-1.972934-68.195172-5.863545a23.418362 23.418362 0 0 0-16.71366 3.509941l-44.614104 29.38833a21.309328 21.309328 0 0 0-2.563382 1.969865c0.027629-0.033769 0.027629-0.056282 0.027629-0.056282-0.045025 0-0.343831 0.196475-1.045819 0.632403l-46.131666 30.255071c-1.542122 1.037632-7.906064 2.727111-13.885242-0.515746-4.257977-2.219551-7.245008-7.163143-7.245008-12.033058v-87.654362c0.004093-7.849782-4.0175-15.163351-10.666944-19.402909-90.090854-57.471946-143.879921-149.4815-143.879921-246.115374 0-168.661328 159.211095-305.885666 354.905102-305.885665 195.721637 0 354.932732 137.224338 354.932732 305.885665S660.981614 740.620931 465.266118 740.620931zM324.923766 388.3007c-22.011316 0-39.9642 17.775852-39.964201 39.561018 0 21.787212 17.952884 39.562041 39.964201 39.562041 22.005176 0 39.95806-17.775852 39.95806-39.562041 0-21.785166-17.952884-39.561018-39.95806-39.561018z m149.33926 0c-22.005176 0-39.9642 17.775852-39.964201 39.561018 0 21.787212 17.958 39.562041 39.964201 39.562041 22.007223 0 39.9642-17.775852 39.9642-39.562041 0-21.785166-17.955954-39.561018-39.9642-39.561018z m149.340283 0c-22.005176 0-39.962154 17.775852-39.962154 39.561018 0 21.787212 17.956977 39.562041 39.962154 39.562041 22.011316 0 39.9642-17.775852 39.9642-39.562041 0-21.785166-17.952884-39.561018-39.9642-39.561018z m329.451384 168.889525c-4.194532-17.090237-10.403954-34.232662-18.516726-50.948369-5.65786-11.6749-19.660782-16.533558-31.348985-10.870582-11.665691 5.66707-16.519232 19.742646-10.861372 31.419594 6.644327 13.729699 11.754718 27.714202 15.122419 41.524742 3.737114 15.490809 5.641487 31.847336 5.641487 48.631604 0 71.375607-38.892799 140.161227-104.040565 183.989431a23.52581 23.52581 0 0 0-10.381441 19.515472v59.953464c0 4.881171-2.432399 9.356089-6.580882 12.274558-7.263427 2.341325-12.005428 2.044566-16.384156-0.907673l-33.206286-21.859867a25.157983 25.157983 0 0 0-2.77009-2.182712l-28.876677-19.461237a23.484877 23.484877 0 0 0-16.84362-3.703345c-65.518203 10.595312-129.455397 0.410346-184.866405-29.403681a23.385617 23.385617 0 0 0-17.880229-1.820461 23.443945 23.443945 0 0 0-13.907755 11.405771c-6.137791 11.437493-1.852184 25.694195 9.559727 31.838125 61.965284 33.326013 132.731 45.642526 205.285456 35.781948l18.815531 12.69923c2.382257 2.269693 4.764514 3.731998 6.414084 4.626367l32.382524 21.298072c10.039657 6.712889 21.794375 10.263761 34.006512 10.263761 8.389064 0 17.024745-1.680269 26.421766-5.109367a23.215748 23.215748 0 0 0 3.485381-1.618871c19.660782-11.116175 31.872918-31.847336 31.872918-54.121641v-47.739281c71.889307-52.794413 114.426099-132.574435 114.426099-215.719087 0.002047-20.50194-2.335185-40.593534-6.968715-59.755965z" p-id="3143" fill="{color}"></path></svg>
			</div>
		        <div style=" padding-bottom: 0.2rem; text-align: center;font-size: 1rem; ">在线咨询</div>
	`;


	var wangmarket_site_kefu_zuoxi = document.createElement("div");
	wangmarket_site_kefu_zuoxi.style = 'position: fixed; right: 4rem; bottom: 4rem; width: auto; height: auto; background: cornsilk; border-radius: 0.3rem; padding: 0.5rem; cursor: pointer;';
	wangmarket_site_kefu_zuoxi.onclick = function(){ 
		kefu.ui.chat.entry(kefu_zuoxi_id); 
	};
	wangmarket_site_kefu_zuoxi.innerHTML = kefuhtml;
	wangmarket_site_kefu_zuoxi.id = 'wangmarket_site_kefu_zuoxi';
	wangmarket_site_kefu_zuoxi.style.backgroundColor = '#EEEEEE';
	wangmarket_site_kefu_zuoxi.style.zIndex = '99999998';	//因为聊天窗口的z-index 是 99999999
	document.body.appendChild(wangmarket_site_kefu_zuoxi);
	
}

//设置文字、图片的颜色。传入如 #343434
function setKefuColor(color){
	try{
		//svg颜色
		document.getElementById('wangmarket_site_kefu_zuoxi').getElementsByTagName('path')[0].setAttribute('fill',color);
	}catch(e){ console.log(e); }
	//文字颜色
	var wangmarket_site_kefu_zuoxi_child_divs = document.getElementById('wangmarket_site_kefu_zuoxi').getElementsByTagName('div');
	for(var i = 0; i < wangmarket_site_kefu_zuoxi_child_divs.length; i++){
		wangmarket_site_kefu_zuoxi_child_divs[i].style.color = color;
	}
}

//设置背景的颜色。传入如 #343434
function setKefuBackgroundColor(color){
	document.getElementById('wangmarket_site_kefu_zuoxi').style.backgroundColor = color;
}

//客服主动跟访客发起询问
function autoHelloText(){
	var onlineKefuInterval = setInterval(function(){
		if(typeof(kefu) != 'undefined'){
			
			//强制给设定一个，上此聊天的人就是当前这个坐席，那么就不会拉取客服坐席的欢迎语了
			kefu.chat.otherUser = {
				id:kefu_zuoxi_id
			}
			//调起对话窗口
			kefu.ui.chat.entry(kefu_zuoxi_id); 
			document.getElementById(kefu.document.iframeId).style.display = ''; 
			
			//发送欢迎语
			var sendMessage = {
				extend:{},
				info:"success",
				result:"1",
				sendId:kefu_zuoxi_id,
				receiveId:kefu.user.id,
				text:kefu_autoHelloText,
				time:new Date().getTime(),
				token:kefu.token.get(),
				type:"MSG"
			};
			kefu.ui.chat.appendMessage(sendMessage);
			
			clearInterval(onlineKefuInterval);//停止
		}		
	}, 500);
}

//调起显示客服按钮
showKefu();
setKefuColor(kefu_site_color);
setKefuBackgroundColor(kefu_site_backgroundColor);

//自动打招呼
if(kefu_autoHelloTime > -1){
	setTimeout(autoHelloText, kefu_autoHelloTime);
}