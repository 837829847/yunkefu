/**
 * https://api.kefu.leimingyun.com/	服务器host，如 http://kefu.xxx.com
 * wss://api.kefu.leimingyun.com/websocket 	socketurl，如： ws://119.3.209.5:8081
 * 365fef747a9e493fb631b621ee36eed1	客服坐席的id，跟谁聊天，这里就是谁的id
 * kefu_site_color  文字图片颜色
 * kefu_site_backgroundColor 背景颜色
 * {autoHelloTime} 对应kefu.autoHelloTime
 * {autoHelloText} 对应kefu.autoHelloText
 */

var kefu_site_color = '{color}';
var kefu_site_backgroundColor = '{backgroundColor}';
var kefu_site_host = '{host}';
var kefu_site_socketUrl = '{socketUrl}';
var kefu_zuoxi_id = '{zuoxiid}';
var kefu_autoHelloTime = '{autoHelloTime}';
var kefu_autoHelloText = '{autoHelloText}';

//加载kefu资源
function loadKefu(){
	var head0 = document.getElementsByTagName('head')[0];

	/* 引入js */
	var msgScript = document.createElement("script");  //创建一个script标签
	msgScript.type = "text/javascript";
	msgScript.src = 'https://res.zvo.cn/msg/msg.js';
	head0.appendChild(msgScript);
	
	var msgScript = document.createElement("script");  //创建一个script标签
	msgScript.type = "text/javascript";
	msgScript.src = 'https://res.zvo.cn/wm/wm.js';
	head0.appendChild(msgScript);

	var kefuScript = document.createElement("script");  //创建一个script标签
	kefuScript.type = "text/javascript";
	kefuScript.src = 'https://res.zvo.cn/kefu/kefu.js';
	//kefuScript.src = '/template/2/kefu.js';
	head0.appendChild(kefuScript);

	var onlineKefuInterval = setInterval(function(){
		console.log('onlineKefuInterval === ');
		if(typeof(kefu) != 'undefined'){
			kefu.document.set('wangmarket_site_kefu_iframe');
			
			var iframeHead0 = kefu.document.get().getElementsByTagName('head')[0];

			/* 引入css */
			/*
			var styleSss = kefu.document.get().createElement('link');
			styleSss.type='text/css';
			styleSss.rel = 'stylesheet';
			styleSss.href = 'https://res.zvo.cn/kefu/css/style.css';
			iframeHead0.appendChild(styleSss);
			*/
			kefu.util.loadCss('https://res.zvo.cn/kefu/css/style.css');
			
			/*
			var styleCss = kefu.document.get().createElement('link');
			styleCss.type='text/css';
			styleCss.rel = 'stylesheet';
			styleCss.href = kefu_site_host+'/template/2/news.css';
			iframeHead0.appendChild(styleCss);
			*/
			kefu.util.loadCss(kefu_site_host+'/template/2/news.css');
		
			/* 创建list、chat 的div */
			var listDiv = kefu.document.get().createElement('div');
			listDiv.id = 'list';
			listDiv.style.display = 'none';
			kefu.document.get().body.appendChild(listDiv);
		
			var chatDiv = kefu.document.get().createElement('div');
			chatDiv.id = 'chat';
			chatDiv.style.position = 'fixed';
			chatDiv.style.top = '0px';
			chatDiv.style.zIndex = '99999999';
			kefu.document.get().body.appendChild(chatDiv);
			
			//写入css style
			var style = kefu.document.get().createElement('style');
			style.innerHTML = '#head,#pc #chat_footer #input_area #footerButton .send{background-color:' + kefu_site_color + '!important;}';
			kefu.document.get().body.appendChild(style);
			
			kefu.document.get().kefu = parent.kefu;
			
			//在 kefu.api.domain 设置之上,加载 KefuJsInterface 接口扩展的插件的 jsFile ， 下面这个注释会被动态替换为插件要引入的jsFile，不要删除。比如会被替换为 try{ kefu.util.synchronizesLoadJs('http://www.zvo.cn/xxxxxxx.js'); }catch(e){ console.log(e); }
			//{KefuJsInterface.kefuJsAppendJsFile}
			
			kefu.api.domain = kefu_site_host; //http://kefu.wfyuntu.com
			kefu.socket.url = kefu_site_socketUrl; //ws://119.3.209.5:8081
			
			//图标颜色
			kefu.ui.color.extendIconColor = kefu_site_color;
			kefu.ui.color.shuruTypeColor = kefu_site_color;
			//发送按钮
			//kefu.document.get().getElementById('sendButton').style.backgroundColor=kefu_site_color;
			
			//在 initKefu() 之上，加载 KefuJsInterface 接口扩展的插件的 jsCode
			//{KefuJsInterface.kefuJsAppendJsCode}
			
			initKefu();
			clearInterval(onlineKefuInterval);//停止
	    	console.log('onlineKefuInterval stop');
		}		
	}, 500);
}


function initKefu(){
	kefu.extend.onlineKefu = {};

	kefu.ui.list.renderAreaId = 'list';
	kefu.ui.chat.renderAreaId = 'chat';

	if(kefu.client.isMobile()){
		//手机
		kefu.mode='mobile';
		kefu.extend.onlineKefu.initChat = function(){
			kefu.document.get().getElementById('back').onclick = function(){
				//kefu.document.get().getElementById('chat').innerHTML = '';
				//kefu.ui.list.entry();
				//kefu.document.get().getElementsByTagName('html')[0].style.fontSize = originalHtmlSize;
				document.getElementById(kefu.document.iframeId).style.display='none';
			};
			
			//计算rem，免得有的手机看起来文字非常小
			var kefu_html = kefu.document.get().getElementsByTagName('html')[0];
			var originalHtmlSize = kefu_html.style.fontSize;	//当前网页本身原始的font-size
			//375调试的正常宽度   16是调试的字体大小
			var kefu_font_size = kefu_html.offsetWidth/375*16;
			if(kefu_font_size == null || kefu_font_size == 0){
				kefu_font_size = 16;
			}
			//设置rem - px 比例
			kefu_html.style.fontSize = kefu_font_size+'px';
		};
		
		//在线客服的显示位置，更靠右下角一点
		wangmarket_site_kefu_zuoxi.style.right = '1rem';
		wangmarket_site_kefu_zuoxi.style.bottom = '1rem';
		
		//调起客服的iframe
		document.getElementById(kefu.document.iframeId).style.bottom = '0px';
		document.getElementById(kefu.document.iframeId).style.height = '100%';
		document.getElementById(kefu.document.iframeId).style.width = '100%';
		document.getElementById(kefu.document.iframeId).style.right = '0px';
		document.getElementById(kefu.document.iframeId).style.left = '0px';
		document.getElementById(kefu.document.iframeId).style.maxWidth = 'none';
	}else{
		//pc
		kefu.mode='pc';
		kefu.ui.chat.html = kefu.ui.chat.pc.html;

		/* pc chat */
		kefu.extend.onlineKefu={
			initChat:function(){
				kefu.chat.shuruType = 'jianpan'
				kefu.chat.shuruTypeChange();
				
//				kefu.ui.chat.pc.moveInit();	//鼠标移动
				//kefu.ui.chat.pc.sizeChange.moveInit();	//拖动大小
				//console.log(kefu)
				kefu.document.get().getElementById('close').innerHTML = kefu.ui.images.close.replace(/#000000/g,kefu.extendIconColor);
				kefu.document.get().getElementById('close').onclick=function(){
					document.getElementById(kefu.document.iframeId).style.display = 'none';
					kefu.currentPage = 'list';
				}
			}
		};
		
		/* 重写 图片插件的放大方法,点击后直接在新窗口打开 */
		kefu.extend.image.fullScreen = function(imagesUrl){
			window.open(imagesUrl);
		}
	}

	//kefu.js 初始化
	kefu.init();

	//重写socket的消息接收
	var oldSocketOnMessage = kefu.socket.onmessage;
	kefu.socket.onmessage = function(res){
		var messageJson = JSON.parse(res.data);
		if(messageJson.type == 'MSG'){
			//如果是普通的沟通消息，那么打开聊天窗口
			if(kefu.currentPage != 'chat'){
				if(kefu.chat.otherUser != null && kefu.chat.otherUser.id != null && kefu.chat.otherUser.id == kefu_zuoxi_id){
					//聊天对象没变，只是打开窗口就行，不用在创建链接
					
				}else{
					//创建新的通道
					//kefu.ui.chat.entry(kefu_zuoxi_id);
				}
				
				kefu.ui.chat.entry(kefu_zuoxi_id);
				document.getElementById(kefu.document.iframeId).style.display = '';
			}
		}
		oldSocketOnMessage(res);
	}
	
}

//调起回话	
//kefu.ui.chat.entry('403');

function showKefu(){
	var kefuhtml = `
			<div class="kefubox" style="width: 60px; height: 60px; display: flex; justify-content: center; align-content: center; align-items: center;  border-radius: 50%;">	
		    	<!--?xml version="1.0" standalone="no"?-->
				<svg t="1641625351781" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="40348" data-spm-anchor-id="a313x.7781069.0.i9" width="60" height="60"><path d="M512 512m-448 0a448 448 0 1 0 896 0 448 448 0 1 0-896 0Z" fill="#1296db" p-id="40349" data-spm-anchor-id="a313x.7781069.0.i8" class=""></path><path d="M418.6 715.6H339v-61.4c-31.9-38.7-51-88.2-51-142.2 0-123.7 100.3-224 224-224s224 100.3 224 224-100.3 224-224 224c-33.4 0-65-7.3-93.4-20.4z m-25.5-157c76.2 100.8 159.8 100.8 237.7 0.1 5.6-7.2 4.2-17.5-3.2-23s-17.8-4.1-23.4 3.1c-64.4 83.3-121.5 83.3-184.4 0.1-5.5-7.3-15.9-8.8-23.3-3.4-7.4 5.5-8.9 15.8-3.4 23.1z" fill="#FFFFFF" p-id="40350"></path></svg>
			</div>
			<style>#head,#pc #chat_footer #input_area #footerButton .send{background-color:` + kefu_site_color + `!important;}</style>
	`;


	var wangmarket_site_kefu_zuoxi = document.createElement("div");
	wangmarket_site_kefu_zuoxi.style = 'position: fixed !important;bottom: 3rem !important;width: auto !important;height: auto !important;background: rgb(53, 173, 242) !important;cursor: pointer !important;overflow: hidden !important;right: 40px !important;padding: 0px !important;border-radius: 50% !important;z-index: 99999998;';
	wangmarket_site_kefu_zuoxi.onclick = function(){ 
		kefu.ui.chat.entry(kefu_zuoxi_id); 
		document.getElementById(kefu.document.iframeId).style.display = ''; 
	};
	wangmarket_site_kefu_zuoxi.innerHTML = kefuhtml;
	wangmarket_site_kefu_zuoxi.id = 'wangmarket_site_kefu_zuoxi';
	wangmarket_site_kefu_zuoxi.style.backgroundColor = '#EEEEEE';
	wangmarket_site_kefu_zuoxi.style.zIndex = '99999998';	//因为聊天窗口的z-index 是 99999999
	document.body.appendChild(wangmarket_site_kefu_zuoxi);
	
	//iframe,显示聊天窗口
	var wangmarket_site_kefu_iframe = document.createElement("iframe");
	wangmarket_site_kefu_iframe.style = 'bottom: 8rem !important;outline: none !important;visibility: visible !important;resize: none !important;box-shadow: none !important;overflow: visible !important;background: none transparent !important;opacity: 1 !important;top: auto !important;right: 36px !important;left: auto !important;position: fixed !important;border: 0px !important;min-height: 179px !important;min-width: 360px !important;max-width: 360px !important;padding: 0px !important;margin: 0px !important;transition-property: none !important;transform: none !important;width: 360px !important;height: 510px !important;z-index: auto !important;cursor: none !important;float: none !important;border-radius: unset !important;pointer-events: auto !important;clip: auto !important;';
	wangmarket_site_kefu_iframe.frameBorder = '0';
	wangmarket_site_kefu_iframe.id = 'wangmarket_site_kefu_iframe';
	wangmarket_site_kefu_iframe.style.zIndex = '99999999';	//因为聊天窗口的z-index 是 99999999
	wangmarket_site_kefu_iframe.style.display = 'none';	//默认不显示iframe
	document.body.appendChild(wangmarket_site_kefu_iframe);
	
	//加载客服
	if(typeof(kefu) == 'undefined'){
		loadKefu();
	}
//	var bns = document.getElementById('wangmarket_site_kefu_zuoxi');
//	bns.onclick = function(){
//	
//	}
}

//设置文字、图片的颜色。传入如 #343434
function setKefuColor(color){
	try{
		//svg颜色
		document.getElementById('wangmarket_site_kefu_zuoxi').getElementsByTagName('path')[0].setAttribute('fill',color);
		console.log(document.getElementById('close'));
	}catch(e){ console.log(e); }
	//文字颜色
	var wangmarket_site_kefu_zuoxi_child_divs = document.getElementById('wangmarket_site_kefu_zuoxi').getElementsByTagName('div');
	for(var i = 0; i < wangmarket_site_kefu_zuoxi_child_divs.length; i++){
		wangmarket_site_kefu_zuoxi_child_divs[i].style.color = color;
	}
}

//设置背景的颜色。传入如 #343434
function setKefuBackgroundColor(color){
	document.getElementById('wangmarket_site_kefu_zuoxi').style.backgroundColor = color;
}

//客服主动跟访客发起询问
function autoHelloText(){
	var onlineKefuInterval = setInterval(function(){
		if(typeof(kefu) != 'undefined'){
			
			//强制给设定一个，上此聊天的人就是当前这个坐席，那么就不会拉取客服坐席的欢迎语了
			kefu.chat.otherUser = {
				id:kefu_zuoxi_id
			}
			//调起对话窗口
			kefu.ui.chat.entry(kefu_zuoxi_id); 
			document.getElementById(kefu.document.iframeId).style.display = ''; 
			
			//发送欢迎语
			var sendMessage = {
				extend:{},
				info:"success",
				result:"1",
				sendId:kefu_zuoxi_id,
				receiveId:kefu.user.id,
				text:kefu_autoHelloText,
				time:new Date().getTime(),
				token:kefu.token.get(),
				type:"MSG"
			};
			kefu.ui.chat.appendMessage(sendMessage);
			
			clearInterval(onlineKefuInterval);//停止
	    	console.log('onlineKefuInterval-- stop');
		}		
	}, 500);
	
}

//调起显示客服按钮
showKefu();
setKefuColor(kefu_site_color);
setKefuBackgroundColor(kefu_site_backgroundColor);

//自动打招呼
if(kefu_autoHelloTime > -1){
	setTimeout(autoHelloText, kefu_autoHelloTime*1000);
}