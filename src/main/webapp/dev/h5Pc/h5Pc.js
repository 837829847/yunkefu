if(typeof(msg) == 'undefined'){
	wm.load.synchronizesLoadJs('https://res.zvo.cn/msg/msg.js');
}
if(typeof(kefu) == 'undefined'){
	wm.load.synchronizesLoadJs('https://res.zvo.cn/kefu/kefu.js');
}
wm.load.css('https://res.zvo.cn/kefu/css/style.css');

var host = 'https://api.kefu.leimingyun.com/';
kefu.socket.url = 'wss://api.kefu.leimingyun.com/websocket';

//kefu.js 接口设置
//kefu.api.getMyUser = host+'/kefu/chat/user/init.json';
////kefu.api.getMyUser = host+'/kefu/inset/login.json';	//三方系统对接
//kefu.api.getChatOtherUser = host+'/kefu/chat/zuoxi/getUserByZuoxiId.json';
//kefu.api.chatLog = host+'/kefu/chat/log/log.json';
//kefu.api.uploadImage = host+'/kefu/chat/file/uploadImage.json';

/* pc chat */
kefu.extend.onlineKefu={
	initChat:function(){
		kefu.chat.shuruType = 'jianpan'
		kefu.chat.shuruTypeChange();
		
		kefu.ui.chat.pc.moveInit();	//鼠标移动
		//kefu.ui.chat.pc.sizeChange.moveInit();	//拖动大小
		document.getElementById('close').innerHTML = kefu.ui.images.close.replace(/{color}/g,kefu.ui.color.extendIconColor);
	}
};

//html加载完后执行
window.onload=function (){
	/* 创建list、chat 的div */
	var listDiv = document.createElement('div');
	listDiv.id = 'list';
	listDiv.style.display = 'none';
	document.body.appendChild(listDiv);

	var chatDiv = document.createElement('div');
	chatDiv.id = 'chat';
	chatDiv.style.position = 'fixed';
	chatDiv.style.top = '0px';
	chatDiv.style.zIndex = '99999999';
	document.body.appendChild(chatDiv);
	
	//电脑界面模式
	kefu.mode='pc';
	kefu.ui.chat.html = kefu.ui.chat.pc.html;	
	kefu.ui.list.renderAreaId = 'list';
	kefu.ui.chat.renderAreaId = 'chat';
	
	//kefu.js 初始化
	kefu.init();
}

