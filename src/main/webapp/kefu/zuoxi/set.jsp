<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="../common/head.jsp">
	<jsp:param name="title" value="设置"/>
</jsp:include>
<style>
body{
	padding: 20px;
}
</style>

<table class="layui-table layui-form" lay-even lay-skin="nob" style="margin:0px; padding:0px;">
	<tbody>
		<tr class="kefuSetInfo">
			<td style="width:150px;">坐席ID</td>
			<td style="cursor: pointer;">
				<span id="zuoxi_id">加载中...</span>
			</td>
		</tr>
		<tr class="kefuSetInfo">
			<td id="nickname_td">客服名字</td>
			<td onclick="updateNickname();" style="cursor: pointer;">
				<span id="nickname">加载中...</span>
				<i class="layui-icon" style="padding-left:8px; font-size:21px;">&#xe642;</i> 
			</td>
		</tr>
		<tr class="kefuSetInfo">
			<td id="head_td">客服头像</td>
			<td>
				<a id="headAId" href="${head }" title="点击预览原图" target="_black"><img id="kefuHead" style="max-height: 25px;height: 25px; width: 25px; border-radius: 25px;" alt="加载中..." /></a>
				<button type="button" class="layui-btn layui-btn-primary layui-btn-sm" id="uploadKefuHeadButton" style="margin-left:20px;">
					<i class="layui-icon">&#xe67c;</i>上传图片
				</button>
			</td>
		</tr>
		
	</tbody>
</table>

<div style="color:#a2a2a2; text-align:left; padding-top:10px; padding-bottom: 10px; padding-left:20px;">
		提示：<br/>
		1.鼠标放到左侧文字描述,可显示当前说明<br/>
</div>

<script type="text/javascript">
//自适应弹出层大小
var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
parent.layer.iframeAuto(index);

layui.use('form', function(){
	var form = layui.form;
	
	form.on('switch(useKefu)', function(data){
		useKefuChange(data.elem.checked);
		updateUseKefu(data.elem.checked? '1':'0');	//将改动同步到服务器，进行保存
	});
	
	form.on('switch(useEmail)', function(data){
		useEmailChange(data.elem.checked);
		updateUseEmail(data.elem.checked? '1':'0');	//将改动同步到服务器，进行保存
	});
	
	
	//美化是否启用的开关控件
	$(".layui-form-switch").css("marginTop","-2px");
	
});

layui.use('upload', function(){
	var upload = layui.upload;
	upload.render({
    	elem: '#uploadKefuHeadButton' //绑定元素
		,url: '/kefu/zuoxi/updateHead.json?token='+wm.token.get() //上传接口
		,field: 'head'
		,before: function(obj){
			msg.loading('上传中');
		}
		,done: function(res){
			msg.close();
			console.log(res);
			msg.success('上传成功');
			document.getElementById('kefuHead').src = res.url+'?x-oss-process=image/resize,h_25';
			document.getElementById('headAId').href  = res.url;
		}
		,error: function(){
			//请求异常回调
			msg.close();
			msg.failure('上传出错');
		}
	});
});

//是否使用客服的开关发生改变触发  use  true:开启使用状态
function useKefuChange(use){
	if(use){
		//使用
		$(".kefuSetInfo").css("opacity","1.0");
	}else{
		//不使用
		$(".kefuSetInfo").css("opacity","0.3");
	}
}


//是否使用当前离现实自动发送邮件提醒的开关发生改变触发  use  true:开启使用状态
function useEmailChange(use){
	if(use){
		//使用
		$(".emailSetInfo").css("opacity","1.0");
	}else{
		//不使用
		$(".emailSetInfo").css("opacity","0.3");
	}
}
useEmailChange('${im.useOffLineEmail}' == 1);

//修改客服名字，修改昵称
function updateNickname(){
	layer.prompt({
			title: '请输入客服名字',
			value: document.getElementById('nickname').innerHTML,
		},
		function(value, index, elem){
			parent.msg.loading('保存中');
			post('/kefu/zuoxi/updateNickname.json',{"nickname":value},function(data){
				parent.msg.close();
				if(data.result != '1'){
					parent.msg.failure(data.info);
				}else{
					layer.close(index);
					location.reload();
					parent.msg.success('操作成功');
				}
			});
			
		});
}


msg.loading('加载中');
//加载当前坐席信息
post('/kefu/zuoxi/getZuoxi.json',{},function(zuoxi){
	msg.close();    //关闭“更改中”的等待提示

	if(zuoxi.result != '1'){
		msg.failure(zuoxi.info);
	}else {
		//获取成功
		document.getElementById('nickname').innerHTML = zuoxi.nickname;
		document.getElementById('zuoxi_id').innerHTML = zuoxi.id;
		if(zuoxi.head == null || zuoxi.head.length < 6){
			document.getElementById('kefuHead').style.display = 'none';
		}else{
			//加载坐席图像
			document.getElementById('kefuHead').src = zuoxi.head+'?x-oss-process=image/resize,h_25';
		}
		
		layui.use('form', function(){
			layui.form.render();;
		});
	}
});


//鼠标跟随提示
$(function(){
	
	//客服名字
	var nickname_td_index = 0;
	$("#nickname_td").hover(function(){
		nickname_td_index = layer.tips('对方所看到的客服的名字', '#nickname_td', {
			tips: [2, '#0FA6A8'], //还可配置颜色
			time:0,
			tipsMore: true,
			area : ['250px' , 'auto']
		});
	},function(){
		layer.close(nickname_td_index);
	})
	
	//客服头像
	var head_td_index = 0;
	$("#head_td").hover(function(){
		head_td_index = layer.tips('对方所看到的客服的头像。请使用正方形的图片', '#head_td', {
			tips: [2, '#0FA6A8'], //还可配置颜色
			time:0,
			tipsMore: true,
			area : ['250px' , 'auto']
		});
	},function(){
		layer.close(head_td_index);
	})
	
	
});	
</script>
<jsp:include page="../common/foot.jsp"></jsp:include>  