<%@page import="com.xnx3.wangmarket.Authorization"%>
<%@page import="com.xnx3.j2ee.entity.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<jsp:include page="../../common/head.jsp">
	<jsp:param name="title" value="管理后台"/>
</jsp:include>
<script src="/js/fun.js"></script>
<script src="/js/admin/commonedit.js"></script>

<script>
//通过get方式传递过来的token
var token = getUrlParams('token');
	if(token != null && token.length > 10){
		shop.setToken(token);	//更新浏览器token缓存
		//如果当前已经登录过了，那么，获取api中 getUser() 看是否还在登录状态 ,因为 shop.isLogin() 获取的只是此访客之前是否已经登录过，至于是否已经超时退出，这个 shop.isLogin() 是不知道的，所以还要请求服务端，看看有没有超时退出登录状态了
		/* post('/shop/store/api/store/getStore.json',{},function(data){
			if(data.result == 1){
				//登录了，更新用户信息
			}
		}); */
	}
</script>

<style>
body{margin: 0;padding: 0px;height: 100%;overflow: hidden;}
#editPanel{
	position: absolute;
    top: 0px;
    width:150px;
}
#editPanel span{
	width:100%;
}
.menu{
	width:150px;
	height:100%;
	background-color: #393D49;
	position: absolute;
}
.menu ul li{
	cursor: pointer;
}
/*左侧的一级菜单的图标*/
.firstMenuIcon{
	font-size:16px;
	padding-right:8px;
	font-weight: 700;
}
/*左侧的一级菜单的文字描述*/
.firstMenuFont{
}

/* 二级菜单 */
.menu .layui-nav-item .layui-nav-child .subMenuItem{
	padding-left:48px;
	font-size: 13px;
}
.layui-nav-tree .layui-nav-item a:hover {
	background-color: #FFFFFF;
}
.layui-nav-tree .layui-nav-child dd.layui-this, .layui-nav-tree .layui-nav-child dd.layui-this a, .layui-nav-tree .layui-this, .layui-nav-tree .layui-this>a, .layui-nav-tree .layui-this>a:hover{
	background-color: #FFFFFF;
}

/** 功能插件的一级菜单 **/
.layui-nav-itemed>a, .layui-nav-tree .layui-nav-title a, .layui-nav-tree .layui-nav-title a:hover{
	color: rgb(51, 51, 51)!important;;
}
.layui-nav .layui-nav-more{
	border-top-color: #612525ba;
}
.layui-nav .layui-nav-mored, .layui-nav-itemed>a .layui-nav-more {
	border-color: transparent transparent #612525ba;
}

/** 功能插件二级菜单 **/
.layui-nav-tree .layui-nav-child dd.layui-this, .layui-nav-tree .layui-nav-child dd.layui-this a, .layui-nav-tree .layui-this, .layui-nav-tree .layui-this>a, .layui-nav-tree .layui-this>a:hover {
	color: rgb(51, 51, 51);
}
.layui-nav-itemed>.layui-nav-child {
    display: block;
    padding: 0;
    background-color: #eaedf0!important;
}
.layui-nav-tree .layui-nav-child a {
    color: rgb(51, 51, 51);
}
.layui-nav-tree .layui-nav-child, .layui-nav-tree .layui-nav-child a:hover {
	color: rgb(51, 51, 51);
}
</style>

<div id="leftMenu" class="layui-nav layui-nav-tree layui-nav-side menu">
	<ul class="">
		
		<div id="menuHtml"></div>
	
		<li class="layui-nav-item">
			<a href="javascript:loadUrl('welcome.jsp');" class="itemA">
				<i class="layui-icon firstMenuIcon">&#xe620;</i>
				<span class="firstMenuFont">客服设置</span>
			</a>
		</li>
		<li class="layui-nav-item" id="question_li">
			<a href="javascript:loadUrl('/kefu/admin/question/list.jsp');" class="itemA">
				<i class="layui-icon firstMenuIcon">&#xe6b2;</i>
				<span class="firstMenuFont">常见问题</span>
			</a>
		</li>
		<li class="layui-nav-item" id="kefuJS">
			<a href="javascript:loadUrl('/kefu/admin/kefuJS/index.jsp');" class="itemA">
				<i class="layui-icon firstMenuIcon">&#xe635;</i>
				<span class="firstMenuFont">客服JS</span>
			</a> 
		</li>
		<!-- <li class="layui-nav-item">
			<a href="javascript:loadUrl('/pc.html');" class="itemA">
				<i class="layui-icon firstMenuIcon">&#xe642;</i>
				<span class="firstMenuFont">坐席</span>
			</a>
		</li> -->
		
		<!-- 
		<li class="layui-nav-item" id="zuoxi_li">
			<a href="javascript:loadUrl('/kefu/admin/zuoxi/list.jsp');" class="itemA">
				<i class="layui-icon firstMenuIcon">&#xe770;</i>
				<span class="firstMenuFont">坐席管理</span>
			</a>
		</li>
		 -->
		 
		<li class="layui-nav-item" id="zuoxi_li">
			<a href="javascript:loadUrl('/kefu/admin/zuoxi/pc.jsp');" class="itemA">
				<i class="layui-icon firstMenuIcon">&#xe63a;</i>
				<span class="firstMenuFont">在线坐席</span>
			</a>
		</li>
	
		<div id="menuAppend" style="">
			<!-- 插件扩展菜单项。追加的值如： -->
			<!-- <li class="layui-nav-item" >
				<a href="/user/logout.do">
					<i class="layui-icon firstMenuIcon">&#xe633;</i>
					<span class="firstMenuFont">退出登陆</span>
				</a>
			</li>
			 -->
		</div>
		<!-- <li class="layui-nav-item" id="zuoxi_li">
			<a href="javascript:loadUrl('/kefu/admin/tiyan/index.jsp');" class="itemA">
				<i class="layui-icon firstMenuIcon">&#xe638;</i>
				<span class="firstMenuFont">快速体验</span>
			</a>
		</li>  -->
		<li class="layui-nav-item" id="updatePassword" style="display:none;">
			<a href="javascript:storeUpdatePassword();" id="xiugaimima" class="itemA">
				<i class="layui-icon firstMenuIcon">&#xe642;</i>
				<span class="firstMenuFont">更改密码</span>
			</a>
		</li>

		<li class="layui-nav-item" id="logout" style="display:none;">
			<a href="javascript:logout();" class="itemA">
				<i class="layui-icon firstMenuIcon">&#xe633;</i>
				<span class="firstMenuFont">退出登陆</span>
			</a>
		</li>
		<script>
			var insetKefu = getUrlParam('insetKefu');
			if(insetKefu == null || insetKefu != 'true'){
				//不是从第三方平台登录进来的，那么显示更改密码跟退出登录
				document.getElementById('updatePassword').style.display = '';
				document.getElementById('logout').style.display = '';
				document.getElementById('kefuJS').style.display = '';
				
			}
		</script>
	</ul>
</div>


<div id="content" style="width: 100%;height:100%;position: absolute;left: 150px;word-wrap: break-word;border-right: 150px;box-sizing: border-box; border-right-style: dotted;">
	<iframe name="iframe" id="iframe" frameborder="0" style="width:100%;height:100%;box-sizing: border-box;"></iframe>
</div>

<script>
//菜单颜色
document.getElementById('leftMenu').style.backgroundColor='#EAEDF1';
$(".itemA").css("color","#333333");

layui.use('element', function(){
  var element = layui.element;
});

/**
 * 在主体内容区域iframe中加载制定的页面
 * url 要加载的页面的url
 */
function loadUrl(url){
	document.getElementById("iframe").src=url + "?time=" + (new Date()).getTime();
}

//修改密码
function storeUpdatePassword(){
	layer.prompt({
		  formType: 0,
		  value: '',
		  title: '请输入旧密码'
	}, function(value1, index, elem){
		layer.close(index);
		layer.prompt({
			  formType: 0,
			  value: '',
			  title: '请输入新密码'
		}, function(value, index, elem){
			layer.close(index);
			msg.loading('更改中');
			$.post("/kefu/admin/user/updatePassword.json", { "newPassword": value,"oldPassword":value1},
				function(data){
					msg.close();
					if(data.result != '1'){
						msg.failure(data.info);
					}else{
						msg.failure('修改成功！新密码：'+value);
					}
				}
			, "json");
		});
	});
	
}

//退出登录
function logout(){
	msg.loading('退出中');
	post('/kefu/admin/user/logout.json',{},function(data){
		msg.close();
		if(data.result == '1'){
			msg.success('已退出',function(){
				window.location.href="/login.do";
			});
		}else{
			msg.failure(data.info);
		}
	});
}


//向扩展菜单的div中，加入html。也就是往里再增加别的菜单。 appendHtml要追加的html，这里一般都是追加li
function menuAppend(appendHtml){
	document.getElementById("menuAppend").innerHTML = document.getElementById("menuAppend").innerHTML + appendHtml; 
}

try{
	//如果从网市场插件进来的，要关闭进入中的提示
	parent.msg.close();
}catch(e){
	console.log(e);
}

loadUrl('welcome.jsp');


$(function(){
	//常见问题
	var question_li_tipindex = 0;
	$("#question_li").hover(function(){
		question_li_tipindex = layer.tips('当用户打开跟你的聊天时，会自动将客服设置中自定义的欢迎语 + 这里的常见问题发给用户。用户点击某条问题时，系统自动将答案回复给用户。', '#question_li', {
			tips: [2, '#0FA6A8'], //还可配置颜色
			time:0,
			tipsMore: true,
			area : ['310px' , 'auto']
		});
	},function(){
		layer.close(question_li_tipindex);
	})

	//kefuJS
	var kefuJS_tipindex = 0;
	$("#kefuJS").hover(function(){
		kefuJS_tipindex = layer.tips('您可以将咨询入口放到任何地方，比如放到网站中，让网站拥有客服咨询功能。<br/>这里仅限懂html代码的朋友使用，如果你不懂html，此处你不用看', '#kefuJS', {
			tips: [2, '#0FA6A8'], //还可配置颜色
			time:0,
			tipsMore: true,
			area : ['310px' , 'auto']
		});
	},function(){
		layer.close(kefuJS_tipindex);
	})

	//在线坐席
	var zuoxi_li_tipindex = 0;
	$("#zuoxi_li").hover(function(){
		zuoxi_li_tipindex = layer.tips('没事就点开这个页面，一直停留在在线坐席，这样你就会保持在线状态。当用户咨询时，会在这里直接出现咨询的消息，您就可以在第一时间跟用户交流了。(如果不在这里，用户给你发消息，你是收不到的)', '#zuoxi_li', {
			tips: [2, '#0FA6A8'], //还可配置颜色
			time:0,
			tipsMore: true,
			area : ['310px' , 'auto']
		});
	},function(){
		layer.close(zuoxi_li_tipindex);
	})
	
});


//加载插件菜单
wm.post('getPluginMenu.json', {}, function(data){
	console.log(data);
	menuAppend(data.info);
});
	

</script>


</body>
</html>