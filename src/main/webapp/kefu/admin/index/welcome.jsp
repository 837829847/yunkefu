<%@page import="com.xnx3.j2ee.shiro.ShiroFunc"%>
<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="../../common/head.jsp">
	<jsp:param name="title" value="设置"/>
</jsp:include>
<style>
body{
	padding: 20px;
}
</style>
<script src="/<%=Global.CACHE_FILE %>Role_role.js"></script>
<script src="/js/fun.js"></script>
<script src="/js/admin/commonedit.js" type="text/javascript"></script>

<table class="layui-table layui-form" lay-even lay-skin="nob" style="margin:0px; padding:0px;">
	<tbody>
		<tr style="display:none;">
			<td style="min-width:110px; width:150px;" id="useKefu_td">在线客服</td>
			<td style="width:200px;">
				<span id="useKefu_span"></span>
				<span id="switch_span" style="margin-left:-50px; width:1px; height:1px;">&nbsp;</span>
			</td>
		</tr>
		<tr class="kefuSetInfo">
			<td style="width:150px;">kefuid</td>
			<td style="cursor: pointer;">
				<span id="kefu_chatid">加载中...</span>
			</td>
		</tr>
		<tr class="kefuSetInfo">
			<td id="autoReply_td">欢迎语</td>
			<td onclick="updateAutoReply();" style="cursor: pointer;">
				<span id="autoReply">加载中...</span>
				<i class="layui-icon" style="padding-left:8px; font-size:21px;">&#xe642;</i> 
			</td>
		</tr>
		<tr class="kefuSetInfo">
			<td id="OfflineAutoReply_td">离线自动回复</td>
			<td onclick="updateOfflineAutoReply();" style="cursor: pointer;">
				<span id="OfflineAutoReply">加载中...</span>
				<i class="layui-icon" style="padding-left:8px; font-size:21px;">&#xe642;</i> 
			</td>
		</tr>
	
		<tr  class="kefuSetInfo"> 
			<td style="width:150px;" colspan="2">&nbsp;</td>
		</tr>
		<tr class="kefuSetInfo">
			<td style="width:150px; font-weight: 600" colspan="2">是否主动跟访客打招呼:</td>
		</tr>
		
		<tr class="kefuSetInfo">
			<td id="autoHelloTime_td">几秒后打招呼</td>
			<td  style="cursor: pointer;">
				<span id="autoHelloTime" onclick="updateAutoHelloTime();">加载中...</span>
				<i class="layui-icon" style="padding-left:8px; font-size:21px;" onclick="updateAutoHelloTime();">&#xe642;</i> 
				<span id="autoHelloTimeTishi" style="padding-left:15px; cursor: pointer;" onclick="msg.alert('如果设置为 > 0 则是启用 <br/> 如果设置为 -1则是不启用');"></span>
			</td>
		</tr>
		<tr  class="kefuSetInfo">
			<td id="OfflineAutoReply_td">主动打招呼的内容</td>
			<td onclick="updateAutoHelloText();" style="cursor: pointer;">
				<span id="autoHelloText">加载中...</span>
				<i class="layui-icon" style="padding-left:8px; font-size:21px;">&#xe642;</i> 
			</td>
		</tr>
		
		<tr class="kefuSetInfo">
			<td style="width:150px;" colspan="2">&nbsp;</td>
		</tr>
		<tr class="kefuSetInfo">
			<td style="width:150px; font-weight: 600" colspan="2">默认坐席:</td>
		</tr>
		<tr class="kefuSetInfo" style="display:none;">
			<td style="width:150px;">坐席ID</td>
			<td style="cursor: pointer;">
				<span id="zuoxi_id">加载中...</span>
			</td>
		</tr>
		<tr class="kefuSetInfo">
			<td id="nickname_td">客服名字</td>
			<td onclick="updateNickname();" style="cursor: pointer;">
				<span id="nickname">加载中...</span>
				<i class="layui-icon" style="padding-left:8px; font-size:21px;">&#xe642;</i> 
			</td>
		</tr>
		<tr class="kefuSetInfo">
			<td id="head_td">客服头像</td>
			<td>
				<a id="headAId" href="${head }" title="点击预览原图" target="_black"><img id="kefuHead" style="max-height: 25px;height: 25px; width: 25px; border-radius: 25px;" alt="加载中..." /></a>
				<button type="button" class="layui-btn layui-btn-primary layui-btn-sm" id="uploadKefuHeadButton" style="margin-left:20px;">
					<i class="layui-icon">&#xe67c;</i>上传图片
				</button>
			</td>
		</tr>
		<!-- 
		<tr class="kefuSetInfo">
			<td id="useEmail_td">邮件通知</td>
			<td style="width:200px;">
				<input type="checkbox" id="switchInputId_email" name="useEmail" value="1" lay-filter="useEmail" lay-skin="switch" lay-text="开启|关闭" <c:if test="${im.useOffLineEmail == 1}">checked</c:if>>
				<span id="switch_span_email" style="margin-left:-50px; width:1px; height:1px;">&nbsp;</span>
			</td>
		</tr>
		<tr class="kefuSetInfo emailSetInfo">
			<td id="email_td">邮箱地址</td>
			<td onclick="updateEmail();" style="cursor: pointer;">
				${im.email }
				<i class="layui-icon" style="padding-left:8px; font-size:21px;">&#xe642;</i> 
			</td>
		</tr>
		<tr class="kefuSetInfo emailSetInfo">
			<td id="email_shengyunul">
				剩余条数
				<script> var email_shengyu_explain='亲，现在内测阶段，发送邮件条数无限制！放心随便用就行了！<br/>若您有使用此项服务，在内测免费阶段结束前半月，我们工作人员会主动联系通知您，确认是否继续使用，绝无其他另收、乱收费！<br/>内测结束后的费用大致是每发送一条邮件收费0.03到0.01元之间，具体多少带内测结束时公布。也就是通常几块钱就够您使用一年的！价格低廉公道，按量收取，用多少付多少！用不了会一直给您存着！'; </script>
			</td>
			<td onclick="layer.msg(email_shengyu_explain);" style="cursor: pointer;">
				无限！
				<i class="layui-icon" style="padding-left:8px; font-size:21px;">&#xe642;</i> 
			</td>
		</tr>
		 -->
	</tbody>
</table>

<div style="color:#a2a2a2; text-align:left; padding-top:10px; padding-bottom: 10px; padding-left:20px;">
		提示：<br/>
		1.鼠标放到左侧文字描述,可显示当前说明<br/>
</div>

<script type="text/javascript">
//自适应弹出层大小
var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
parent.layer.iframeAuto(index);

layui.use('form', function(){
	var form = layui.form;
	
	form.on('switch(useKefu)', function(data){
		useKefuChange(data.elem.checked);
		updateUseKefu(data.elem.checked? '1':'0');	//将改动同步到服务器，进行保存
	});
	
	form.on('switch(useEmail)', function(data){
		useEmailChange(data.elem.checked);
		updateUseEmail(data.elem.checked? '1':'0');	//将改动同步到服务器，进行保存
	});
	
	
	//美化是否启用的开关控件
	$(".layui-form-switch").css("marginTop","-2px");
	
});

layui.use('upload', function(){
	var upload = layui.upload;
	upload.render({
    	elem: '#uploadKefuHeadButton' //绑定元素
		,url: '/kefu/admin/zuoxi/updateHead.json?token='+shop.getToken() //上传接口
		,field: 'head'
		,before: function(obj){
			msg.loading('上传中');
		}
		,done: function(res){
			msg.close();
			console.log(res);
			msg.success('上传成功');
			document.getElementById('kefuHead').src = res.url+'?x-oss-process=image/resize,h_25';
			document.getElementById('headAId').href  = res.url;
		}
		,error: function(){
			//请求异常回调
			msg.close();
			msg.failure('上传出错');
		}
	});
});

//是否使用客服的开关发生改变触发  use  true:开启使用状态
function useKefuChange(use){
	if(use){
		//使用
		$(".kefuSetInfo").css("opacity","1.0");
	}else{
		//不使用
		$(".kefuSetInfo").css("opacity","0.3");
	}
}


//是否使用当前离现实自动发送邮件提醒的开关发生改变触发  use  true:开启使用状态
function useEmailChange(use){
	if(use){
		//使用
		$(".emailSetInfo").css("opacity","1.0");
	}else{
		//不使用
		$(".emailSetInfo").css("opacity","0.3");
	}
}
useEmailChange('${im.useOffLineEmail}' == 1);

//修改客服名字，修改昵称
function updateNickname(){
	layer.prompt({
			title: '请输入客服名字',
			value: document.getElementById('nickname').innerHTML,
		},
		function(value, index, elem){
			parent.msg.loading('保存中');
			post('/kefu/admin/zuoxi/updateNickname.json',{"nickname":value},function(data){
				parent.msg.close();
				if(data.result != '1'){
					parent.msg.failure(data.info);
				}else{
					layer.close(index);
					location.reload();
					parent.msg.success('操作成功');
				}
			});
			
		});
}

//修改离线时，接受的邮箱地址
function updateEmail(){
	layer.prompt({
			title: '请输入您的邮箱地址',
			value: '${im.email}',
		},
		function(value, index, elem){
			msg.loading('保存中');
			post('/kefu/update.json',{"name":"email","value":value},function(data){
				msg.close();
				if(data.result != '1'){
					msg.failure(data.info);
				}else{
					layer.close(index);
					location.reload();
					parent.msg.success('操作成功');
				}
			});
			
		});
}

//修改客服不在线时自动回复的内容
function updateAutoReply(){
	layer.prompt({
			title: '欢迎语',
			formType: 2,
			maxlength:200,
			value: document.getElementById('autoReply').innerHTML,
		},
		function(value, index, elem){
			parent.msg.loading('保存中');
			post('/kefu/admin/kefu/update.json',{"name":"autoReply","value":value},function(data){
				parent.msg.close();
				if(data.result != '1'){
					parent.msg.failure(data.info);
				}else{
					layer.close(index);
					location.reload();
					parent.msg.success('操作成功');
				}
			});
		
		});
}

//修改客服不在线时自动回复的内容
function updateOfflineAutoReply(){
	layer.prompt({
			title: '不在线时，自动回复的内容',
			formType: 2,
			maxlength:200,
			value: document.getElementById('OfflineAutoReply').innerHTML,
		},
		function(value, index, elem){
			parent.msg.loading('保存中');
			post('/kefu/admin/kefu/update.json',{"name":"offlineAutoReply","value":value},function(data){
				parent.msg.close();
				if(data.result != '1'){
					parent.msg.failure(data.info);
				}else{
					layer.close(index);
					location.reload();
					parent.msg.success('操作成功');
				}
			});
		
		});
}


//修改客服自动发送欢迎的招呼的时间
function updateAutoHelloTime(){
	layer.prompt({
			title: '自动打招呼的等待时间(单位：秒)',
			value: document.getElementById('autoHelloTime').innerHTML,
		},
		function(value, index, elem){
			parent.msg.loading('保存中');
			post('/kefu/admin/kefu/update.json',{"name":"autoHelloTime","value":value},function(data){
				parent.msg.close();
				if(data.result != '1'){
					parent.msg.failure(data.info);
				}else{
					layer.close(index);
					location.reload();
					parent.msg.success('操作成功');
				}
			});
			
		});
}


//修改 访客进入网站后，几秒后自动给访客发一条消息，消息的内容便是在这里修改
function updateAutoHelloText(){
	layer.prompt({
			title: '主动跟访客打招呼的内容',
			formType: 2,
			maxlength:200,
			value: document.getElementById('autoHelloText').innerHTML,
		},
		function(value, index, elem){
			parent.msg.loading('保存中');
			post('/kefu/admin/kefu/update.json',{"name":"autoHelloText","value":value},function(data){
				parent.msg.close();
				if(data.result != '1'){
					parent.msg.failure(data.info);
				}else{
					layer.close(index);
					location.reload();
					parent.msg.success('操作成功');
				}
			});
		
		});
}

//修改当前客服是否使用
function updateUseKefu(value){
	msg.loading('修改中');
	post('/kefu/update.json',{"name":"use", "value":value},function(data){
		msg.close();
		console.log(data);
		if(data.result != '1'){
			msg.failure(data.info);
		}else{
			msg.success('操作成功');
		}
	});
}

//修改当前离线邮箱接受消息是否使用
function updateUseEmail(value){
	post('/kefu/admin/kefu/update.json',{"name":"useOffLineEmail", "value":value},function(data){
		msg.close();
		if(data.result != '1'){
			msg.failure(data.info);
		}else{
			msg.success('操作成功');
		}
	});
}


parent.msg.loading('加载中');
post('/kefu/admin/kefu/getKefu.json',{},function(data){
	parent.msg.close();
	parent.checkLogin(data);	//验证登录状态。如果未登录，那么跳转到登录页面
	
	if(data.result == '2'){
		parent.msg.failure('请先登陆');
		parent.window.location.href="/login.do";
	}else if(data.result != '1'){
		parent.msg.failure(data.info);
	}else {
		//登录成功
		useKefuChange(data.kefu.useKefu == 1);
		document.getElementById('useKefu_span').innerHTML = '<input type="checkbox" id="switchInputId" name="use" value="1" lay-filter="useKefu" lay-skin="switch" lay-text="开启|关闭" '+(data.kefu.useKefu == 1 ? 'checked':'')+'>';
		document.getElementById('autoReply').innerHTML = data.kefu.autoReply;
		//离线自动回复
		document.getElementById('OfflineAutoReply').innerHTML = data.kefu.offlineAutoRaply;
		//客服的id
		document.getElementById('kefu_chatid').innerHTML = 'kefuchatid_'+data.kefu.chatid;
		//自动打招呼
		document.getElementById('autoHelloTime').innerHTML = data.kefu.autoHelloTime;
		document.getElementById('autoHelloText').innerHTML = data.kefu.autoHelloText;
		document.getElementById('autoHelloTimeTishi').innerHTML = data.kefu.autoHelloTime > 0 ? '(已启用)':'(未启用)';
		
		
		//加载默认坐席
		post('/kefu/admin/zuoxi/getZuoxiByUserid.json',{userid: data.kefu.userid},function(zuoxi){
			parent.msg.close();    //关闭“更改中”的等待提示
		
			if(zuoxi.result != '1'){
				parent.msg.failure(zuoxi.info);
			}else {
				//获取成功
				console.log(zuoxi);
				document.getElementById('nickname').innerHTML = zuoxi.nickname;
				document.getElementById('zuoxi_id').innerHTML = zuoxi.id;
				if(zuoxi.head == null || zuoxi.head.length < 6){
					document.getElementById('kefuHead').style.display = 'none';
				}else{
					//加载坐席图像
					document.getElementById('kefuHead').src = zuoxi.head+'?x-oss-process=image/resize,h_25';
				}
				
				layui.use('form', function(){
					layui.form.render();;
				});
				
			}
		});
	}
});


//鼠标跟随提示
$(function(){
	//是否启用在线客服
	var td_useKefu_index = 0;
	$("#useKefu_td").hover(function(){
		td_useKefu_index = layer.tips('是否启用在线客服？<br/>启用在线客服后，会在网站底部中间位置出现在线客服功能，访客可通过此跟您进行实时沟通；同时您也可以看到当前网站有多少访客在浏览，还可以向访客主动发起会话进行沟通！', '#useKefu_td', {
			tips: [2, '#0FA6A8'], //还可配置颜色
			time:0,
			tipsMore: true,
			area : ['250px' , 'auto']
		});
	},function(){
		layer.close(td_useKefu_index);
	})
	
	
	//主动给访客发送打招呼的时间
	var autoHelloTime_td_index = 0;
	$("#autoHelloTime_td").hover(function(){
		autoHelloTime_td_index = layer.tips('主动给访问网站的访客打招呼。比如设置为10，则是访客打开您的网站后，10秒后自动给这个访客发送打招呼的信息，引着访客跟你沟通.<br/>这里填写的单位是秒。<br/>如果设置为 > 0 则是启用<br/>如果设置为 -1则是不启用', '#autoHelloTime_td', {
			tips: [2, '#0FA6A8'], //还可配置颜色
			time:0,
			tipsMore: true,
			area : ['250px' , 'auto']
		});
	},function(){
		layer.close(autoHelloTime_td_index);
	})
	
	//客服名字
	var nickname_td_index = 0;
	$("#nickname_td").hover(function(){
		nickname_td_index = layer.tips('对方所看到的客服的名字', '#nickname_td', {
			tips: [2, '#0FA6A8'], //还可配置颜色
			time:0,
			tipsMore: true,
			area : ['250px' , 'auto']
		});
	},function(){
		layer.close(nickname_td_index);
	})
	
	//客服头像
	var head_td_index = 0;
	$("#head_td").hover(function(){
		head_td_index = layer.tips('对方所看到的客服的头像。请使用正方形的图片', '#head_td', {
			tips: [2, '#0FA6A8'], //还可配置颜色
			time:0,
			tipsMore: true,
			area : ['250px' , 'auto']
		});
	},function(){
		layer.close(head_td_index);
	})
	
	//欢迎语
	var autoReply_td_index = 0;
	$("#autoReply_td").hover(function(){
		autoReply_td_index = layer.tips('当客户问问题，打开跟自己的聊天窗口时，会自动发送给客户的内容。请输入100字以内', '#autoReply_td', {
			tips: [2, '#0FA6A8'], //还可配置颜色
			time:0,
			tipsMore: true,
			area : ['250px' , 'auto']
		});
	},function(){
		layer.close(autoReply_td_index);
	})
	
	//当客服不在时，访客问问题时，会自动回复给访客的内容
	var OfflineAutoReply_td_index = 0;
	$("#OfflineAutoReply_td").hover(function(){
		OfflineAutoReply_td_index = layer.tips('当客服不在，客户问问题时，会自动回复给客户的内容。请输入100字以内', '#OfflineAutoReply_td', {
			tips: [2, '#0FA6A8'], //还可配置颜色
			time:0,
			tipsMore: true,
			area : ['250px' , 'auto']
		});
	},function(){
		layer.close(OfflineAutoReply_td_index);
	})
	
	//是否启用离线时邮件通知
	var useEmail_td_index = 0;
	$("#useEmail_td").hover(function(){
		useEmail_td_index = layer.tips('当您不在线时，网站访客通过在线客服想您提问问题、对话时，是否将访客的对话发送到您指定的邮箱进行实时提醒？<hr/>提示：手机上可使用QQ、微信实时接收邮件，或者可以下载个邮箱客户端，随时都能在第一时间接收到访客的咨询。', '#useEmail_td', {
			tips: [3, '#0FA6A8'], //还可配置颜色
			time:0,
			tipsMore: true,
			area : ['350px' , 'auto']
		});
	},function(){
		layer.close(useEmail_td_index);
	})
	
	//离线邮件通知的邮箱地址
	var email_td_index = 0;
	$("#email_td").hover(function(){
		email_td_index = layer.tips('请输入您离线时，接收访客信息的邮箱。若没有邮箱，可以填写上您的QQ邮箱', '#email_td', {
			tips: [2, '#0FA6A8'], //还可配置颜色
			time:0,
			tipsMore: true,
			area : ['250px' , 'auto']
		});
	},function(){
		layer.close(email_td_index);
	})
	
	//离线邮件通知的邮件剩余条数
	var email_shengyunul_index = 0;
	$("#email_shengyunul").hover(function(){
		email_shengyunul_index = layer.tips(email_shengyu_explain, '#email_shengyunul', {
			tips: [3, '#0FA6A8'], //还可配置颜色
			time:0,
			tipsMore: true,
			area : ['380px' , 'auto']
		});
	},function(){
		layer.close(email_shengyunul_index);
	})
	
});	
</script>
<jsp:include page="../../common/foot.jsp"></jsp:include>  