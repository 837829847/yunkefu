<%@page import="com.xnx3.j2ee.shiro.ShiroFunc"%>
<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="../../common/head.jsp">
	<jsp:param name="title" value="快速体验"/>
</jsp:include>
<style>
body{
	padding: 20px;
}
</style>
<link rel="stylesheet" href="//res.zvo.cn/kefu/css/style.css">
<script src="//res.zvo.cn/kefu/kefu.js"></script>

<div id="list" style="display:none;"></div>
<div id="chat" style="position: fixed; z-index: 99999999; "></div>

<div style="color:#a2a2a2; text-align:left; padding-top:10px; padding-bottom: 10px; padding-left:20px;">
		提示：<br/>
		这个页面就是给你临时看效果的，让你能看到你当前的设置，在别人(咨询的人)看来，是这个样子的<br/>
		你可以点击右小角的聊天图标来看看客户发起咨询的样子。
</div>

<script>
msg.loading('加载中');
post('/kefu/admin/kefu/getKefu.json',{},function(data){
	parent.checkLogin(data);	//验证登录状态。如果未登录，那么跳转到登录页面

	if(data.result != '1'){
		msg.failure(data.info);
	}else {
		//登录成功
		
		//加载默认坐席
		post('/kefu/admin/zuoxi/getZuoxiByUserid.json',{userid: data.kefu.userid},function(zuoxiData){
			msg.close();    //关闭“更改中”的等待提示
			if(zuoxiData.result != '1'){
				msg.failure(zuoxiData.info);
			}else {
				//获取成功，加载js
				//wm.load.synchronizesLoadJs('/siteKefu.js?id='+zuoxiData.id);
				
				var head= document.getElementsByTagName('head')[0]; 
				var script= document.createElement('script'); 
				script.type= 'text/javascript'; 
				script.src= '/siteKefu.js?id='+zuoxiData.id;
				script.onload = script.onreadystatechange = function() {
					loadKefu();
				}
				head.appendChild(script); 


				
			}
		});
	}
});
</script>
<script src="http://localhost:8080/siteKefu.js?id=9752d379cbe641039de8c68974319bb9"></script>
<jsp:include page="../../common/foot.jsp"></jsp:include>  