<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<jsp:include page="/wm/common/head.jsp">
	<jsp:param name="title" value="编辑"/>
</jsp:include>
<form id="form" class="layui-form" action="" style="padding:20px; padding-top:35px; margin-bottom: 10px;">
	<div class="layui-form-item">
		<label class="layui-form-label">客服昵称</label>
		<div class="layui-input-block">
			<input type="text" name="nickname" id="nickname" class="layui-input" value="" >
		</div>
	</div>
	<div class="layui-form-item">
		<label class="layui-form-label">登录用户名</label>
		<div class="layui-input-block">
			<input type="text" name="username" id="username" class="layui-input" value="" >
		</div>
	</div>
	<div class="layui-form-item">
		<label class="layui-form-label">登录密码</label>
		<div class="layui-input-block">
			<input type="text" name="password" id="password" class="layui-input" value="" >
		</div>
	</div>

	<div class="layui-form-item">
		<div class="layui-input-block">
			<a class="layui-btn" onclick="commit()">立即创建</a>
			<button type="reset" class="layui-btn layui-btn-primary">重置</button>
		</div>
	</div>
</form>
<script>

//自适应弹出层大小
var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
// 提交修改添加信息
function commit() {
	var data = {
		username:document.getElementById('username').value,
		password:document.getElementById('password').value,
		nickname:document.getElementById('nickname').value
	};

	parent.parent.msg.loading("保存中");
	wm.post("/kefu/admin/zuoxi/save.json?",data, function (result) {
		parent.parent.msg.close();
		checkLogin(result);	//验证登录状态。如果未登录，那么跳转到登录页面
		
		if(result.result == '1'){
			parent.parent.msg.success("添加成功", function(){
				parent.layer.close(index);	//关闭当前窗口
				parent.location.reload();	//刷新父窗口列表
			});
		}else if(result.result == '0'){
			parent.parent.msg.failure(result.info);
		}else{
			parent.parent.msg.failure("修改失败");
		}
	});

	return false;
}

</script>

<jsp:include page="/wm/common/foot.jsp"></jsp:include>