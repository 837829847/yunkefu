<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><!DOCTYPE html>
<jsp:include page="../../common/head.jsp">
	<jsp:param name="title" value="在线坐席"/>
</jsp:include>
<script src="/module/wm/wm.js"></script>
<script src="//res.zvo.cn/kefu/kefu.js"></script>
<link rel="stylesheet" type="text/css" href="//res.zvo.cn/kefu/css/style.css" media="screen">

<style>
/*版权信息*/
#pc #chat_footer #copyright{
	display:none;
}
#list{
	overflow-y: auto!important;
	overflow-x: hidden!important;
	background-color: #f7f7f7!important;
	border-right: 1px solid #f2f2f2;
	box-shadow: 1px 1px 10px 0 #ddd;
	max-width: 40rem; 
	width: 22rem;
	position: absolute; 
	background: aliceblue;  
	height: 100%; 
	overflow: scroll;
}
</style>
</head>
<body>

<div id="list"></div>
<div style="float: left;  max-width: 400px;">
	<div id="chat"></div>
</div>
<div id="tishi" style=" position: absolute; bottom: 0.5rem; left: 50%; margin-left: -9rem; color: gray; font-size: 1.5rem; opacity: 0.5;">
	当前客服坐席在线中，等待客户发起咨询
</div>

<script type="text/javascript">
var config = {};

function loadKefu(){
	var host = config.api;
	kefu.api.domain = host;
	kefu.socket.url = config.socket;
	 
	//kefu.js 接口设置
	kefu.extend.order.requestApi = 'http://xxxxx.com/plugin/kefu/getOrderList.json';
	
	//指定css远程路径
	kefu.extend.order.css = 'https://kefudev.obs.cn-north-4.myhuaweicloud.com//Users/apple/git/kefu_js/extend/order/style.css';
	kefu.extend.goods.css = 'https://kefudev.obs.cn-north-4.myhuaweicloud.com//Users/apple/git/kefu_js/extend/goods/style.css';
	
	//kefu.js 初始化
	//kefu.init();
	//出现列表页
	//kefu.ui.list.entry();
	//kefu.notification.use=false;
	//初始化pc端客服坐席
	kefu.ui.chat.pc.init();
	//出现列表页
	kefu.ui.list.entry();

}

msg.loading('加载中');
post('/kefu/chat/config/getConfig.json',{},function(data){
	msg.close();
	
	if(data.result != '1'){
		msg.failure(data.info);
	}else {
		//成功
		
		//加载插件相关
		if(data.jsFile != null && typeof(data.jsFile) != 'undefined' && data.jsFile.length > 0){
			for(var f = 0; f < data.jsFile.length; f++){
				kefu.util.synchronizesLoadJs(data.jsFile[f]);
			}
		}
		if(typeof(data.jsCode) != 'undefined' && data.jsCode != '' && data.jsCode.length > 0){
			eval(data.jsCode);
		}
		
		console.log(data);
		config = data;
		loadKefu();
	}
});

//显示底部的提示信息，是否在线状态
function showTishi(){
	var currentTime = Date.parse(new Date())/1000;
	currentTimeStr = formatTime(currentTime, "h:m:s");
	try {
		if(kefu.socket.socket.readyState == 1){
			document.getElementById('tishi').innerHTML = '当前客服坐席在线中，等待客户发起咨询  -- '+currentTimeStr;
		}else{
			document.getElementById('tishi').innerHTML = '<span style="color:red;">当前客服坐席掉线，正在重新连接中...... </span>'+currentTimeStr;
		}
	} catch (e) {
		console.log(e);
	}
}

//循环执行，每隔3秒钟执行一次showalert（）
window.setInterval(showTishi, 1000);

</script>

</body>
</html>