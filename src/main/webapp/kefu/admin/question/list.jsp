<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<jsp:include page="../../common/head.jsp">
	<jsp:param name="name" value="常见问题列表"/>
</jsp:include>
<style type="text/css">
body{
	padding: 20px;
	padding-top:15px;
}
.layui-table img {
    max-width: 49px;
    max-height:29px;
}
.toubu_xnx3_search_form {
    padding-top: 0px;
    padding-bottom: 10px;
}
</style>
<jsp:include page="../../common/list/formSearch_formStart.jsp" ></jsp:include>
	<a class="layui-btn layui-btn-normal" onclick="addOrUpdate(0)" style=""><i class="layui-icon" style="font-size: 14px;">添加常见问题</i></a>
</form>
<table class="aui-table-responsive layui-table iw_table" style="color: black;font-size: 14px;">
  <thead>
    <tr>
        <th style="text-align:left; width: 20%;">问题</th>
        <th style="text-align:left;">回答</th>
         <th style="text-align:center;width: 130px;">最后修改时间</th>
        <th style="text-align:center; width: 100px;">操作</th>
    </tr> 
  </thead>
  <tbody id="list">
  		<!-- list 这个div里面的html内容，其实就是购物车中，多个商品中，其中某一个商品的模版。当购物车中有多个商品时，会复制出多个 div id="item{id}" 来 
			其中可用的变量：
			{title} 问
			{answer} 答
			{updatetime} 最后修改时间
		-->
		<tr>
			<td style="text-align:left;">{title}</td>
			<td style="text-align:left;">{answer}</td>
			<td style="text-align:center;">{updatetime}</td>
			<td style="text-align:center;">
				<!-- 修改 -->
				<a class="layui-btn layui-btn-sm" onclick="addOrUpdate('{id}')" style=""><i class="layui-icon">&#xe642;</i></a>	 
			
				<!-- 删除 -->
				<a class="layui-btn layui-btn-sm" onclick="deleteMes('{id}')" style=""><i class="layui-icon">&#xe640;</i></a>
			</td>
		</tr>
  </tbody>
</table>

 
<div style="color: gray;margin-top: 10px; text-align:right;">
	<span style="padding-right:20px;">操作按钮提示</span>
	<span style="padding-right:20px;"><i class="layui-icon">&#xe642;</i>&nbsp;:&nbsp;编辑</span>
	<span style="padding-right:20px;"><i class="layui-icon">&#xe640;</i>&nbsp;:&nbsp;删除</span>
</div>

<div style="color:#a2a2a2; text-align:left; padding-top:10px; padding-bottom: 10px; padding-left:20px;">
	建议常见问题不要超过8条。 当用户打开跟你的聊天时，会自动将欢迎语+常见问题发给用户，如果常见问题太多，那么，相当难看！
</div>

<script type="text/javascript">
function deleteMes(id){
	var dtp_confirm = layer.confirm('确定要删除该常见问题？', {
		  btn: ['确认','取消'] //按钮
	}, function(){
		layer.close(dtp_confirm);
		parent.msg.loading("删除中");    //显示“操作中”的等待提示
		post('/kefu/admin/question/delete.json?id=' + id,{}, function(data){
		    parent.msg.close();    //关闭“操作中”的等待提示
			checkLogin(data);	//验证登录状态。如果未登录，那么跳转到登录页面
			if(data.result == '1'){
		        parent.msg.success('操作成功');
		        window.location.reload();	//刷新当前页
		     }else if(data.result == '0'){
		         parent.msg.failure(data.info);
		     }else{
		         parent.msg.failure();
		     }
		});
	}, function(){
		
	});
}


// 跳转添加或者修改页面 id 问题的id
function addOrUpdate(id){
	if(id == 0) {
		id = '';
	}
	 layer.open({
		type: 2, 
		title:'编辑页面', 
		area: ['500px', '400px'],
		shadeClose: true, //开启遮罩关闭
		content: '/kefu/admin/question/edit.jsp?id=' + id
	});	 
}

</script>

<script>
//列表的模版
var orderTemplate = document.getElementById("list").innerHTML;
function templateReplace(item){
	return orderTemplate.replace(/\{id\}/g, item.id)
				.replace(/\{title\}/g, item.title)
				.replace(/\{answer\}/g, item.answer)
				.replace(/\{updatetime\}/g, formatTime(item.updatetime,'Y-M-D'))
				;
}

/**
 * 获取分类列表数据
 * @param currentPage 要查看第几页，如传入 1
 */
function list(currentPage){
var data = {
	'currentPage':currentPage,
	'everyNumber':'20',	//这里每页显示2条数据
	//'title':document.getElementById('title').value,
};
	parent.msg.loading('加载中');
	post('/kefu/admin/question/list.json' ,data,function(data){
	parent.msg.close();    //关闭“更改中”的等待提示
	checkLogin(data);	//判断是否登录

		//已登陆
		if(data.result == '0'){
			msg.failure(data.info);
		}else if(data.result == '1'){
			//成功

			//列表
			var html = '';
			for(var index in data.list){
				var item = data.list[index];
				//只显示已选中的商品
				html = html + templateReplace(item);
			}
			document.getElementById("list").innerHTML = html;
			//分页
			//page.render(data.page);
		}
	});
}
//刚进入这个页面，加载第一页的数据
list(1);
</script>
<jsp:include page="../../common/foot.jsp"></jsp:include>