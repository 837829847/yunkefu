<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<jsp:include page="../../common/head.jsp">
	<jsp:param name="title" value="编辑"/>
</jsp:include>
<form id="form" class="layui-form" action="" style="padding:20px; padding-top:35px; margin-bottom: 10px;">
	<input type="hidden" id="id" name="id">

	<div class="layui-form-item">
		<label class="layui-form-label">问题</label>
		<div class="layui-input-block">
			<input type="text" name="title" id="title" class="layui-input" value="" >
		</div>
	</div>
	<div class="layui-form-item">
		<label class="layui-form-label">答案</label>
		<div class="layui-input-block">
			<textarea class="layui-textarea" name="answer" id="answer"></textarea>
		</div>
	</div>

	<div class="layui-form-item">
		<div class="layui-input-block">
			<a class="layui-btn" onclick="commit()">立即保存</a>
			<button type="reset" class="layui-btn layui-btn-primary">重置</button>
		</div>
	</div>
</form>
<script>

//自适应弹出层大小
var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
// 提交修改添加信息
function commit() {
	document.getElementById('answer').value = document.getElementById('answer').value.replace(/\n/g,"[br]");	//换行替换
	var d = $("form").serialize();
	if($("#title").val() == ''){
		msg.failure("请输入标题");
		return ;
	}
	if($("#answer").val() == ''){
		msg.failure("请输入答案");
		return ;
	}
	
	//表单序列化
	parent.parent.msg.loading("保存中");
	post("/kefu/admin/question/save.json?"+d,{}, function (result) {
		parent.parent.msg.close();
		checkLogin(result);	//验证登录状态。如果未登录，那么跳转到登录页面
		if(result.result == '1'){
			parent.parent.msg.success("操作成功", function(){
				parent.layer.close(index);	//关闭当前窗口
				parent.location.reload();	//刷新父窗口列表
			});
		}else if(result.result == '0'){
			parent.parent.msg.failure(result.info);
		}else{
			parent.parent.msg.failure("修改失败");
		}
	}, "text");

	return false;
}

msg.loading('加载中');
var id = getUrlParams('id');
var obj;

post('/kefu/admin/question/edit.json',{id:id},function(data){
msg.close();    //关闭“更改中”的等待提示
checkLogin(data);	//验证登录状态。如果未登录，那么跳转到登录页面

	if(data.result != '1'){
		msg.failure(data.info);
	}else{
		//成功
		obj = data.question;
		document.getElementById('title').value = obj.title;
		document.getElementById('id').value = obj.id;
		document.getElementById('answer').value = obj.answer.replace(/\[br\]/g,"\n");
	}
});
</script>

<jsp:include page="../../common/foot.jsp"></jsp:include>