<%@page import="com.xnx3.j2ee.shiro.ShiroFunc"%>
<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="../../common/head.jsp">
	<jsp:param name="title" value="kefuJS"/>
</jsp:include>
<style>
body{
	padding: 20px;
}
</style>
<script src="/<%=Global.CACHE_FILE %>Role_role.js"></script>
<script src="/js/fun.js"></script>
<script src="/js/admin/commonedit.js" type="text/javascript"></script>

<table class="layui-table layui-form" lay-even lay-skin="nob" style="margin:0px; padding:0px;">
	<tbody>
		<tr class="kefuSetInfo">
			<td id="nickname_td" style="width:120px;">文字、图片颜色</td>
			<td  style="cursor: pointer;" >
				<div class="layui-input-inline" style="width: 120px;">
        				<input type="text" value="" placeholder="请选择颜色" class="layui-input" id="color" > 
      			</div>
				<i class="layui-icon" style="padding-left:8px; font-size:21px; font-size: 15px;" id="color1" ></i> 
			</td>
		</tr>
		<tr class="kefuSetInfo">
			<td id="head_td">背景颜色</td>
			<td>
				<div class="layui-input-inline" style="width: 120px;">
        				<input type="text" value="" placeholder="请选择颜色" class="layui-input" id="backgroundColor" > 
        				
      			</div>
				<i class="layui-icon" style="padding-left:8px; font-size:21px; font-size: 15px;" id="backgroundColor1" "></i> 
			</td>
		</tr>
		<tr class="kefuSetInfo">
		</tr>
		<tr class="kefuSetInfo">
			<td id="head_td">网站中使用</td>
			<td >
				
				<div id="jssrc">
					<textarea class="layui-textarea"><script src="{api}/siteKefu.js?id={zuoxiid}"></script></textarea>
				</div>
				将以上代码加到你网站的最底部，随便底部的哪个位置就可以。
			</td>
		</tr>
	</tbody>
</table>


<script type="text/javascript">
//显示实时客服控件
//showKefu();


//自适应弹出层大小
var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
//parent.layer.iframeAuto(index);
var kefuJS = {};

layui.use(['form','colorpicker'], function(){
	var form = layui.form //获取form模块
	 ,colorpicker = layui.colorpicker; //获得colorpicker模块

	colorpicker.render({
	    elem: '#color1'	//绑定元素
		
	    ,done: function(color){
	      //把color赋值给#color
	      $('#color').val(color);
	      updateColor();
	    }
		,change: function(color){
			setKefuColor(color);
	      //$('#wangmarket_site_kefu_zuoxi div,#wangmarket_site_kefu_zuoxi div svg path').css('color', color);
	    }
	  });

	colorpicker.render({
	    elem: '#backgroundColor1' //绑定元素
	    ,done: function(color1){
	      //把color赋值给#backgroundColor
	      $('#backgroundColor').val(color1);
	      updateBackgroundColor();
	    }
		,change: function(color1){
			setKefuBackgroundColor(color1);
	      //$('#wangmarket_site_kefu_zuoxi').css('background-color', color1);
	    }
	  });	
});

function updateColor(){
	//获得color的值
    kefuJS.color = $("#color").val();
    //保存
    save();
}
function updateBackgroundColor(){
	//获得backgroundColor的值
    kefuJS.backgroundColor = $("#backgroundColor").val();
    //保存
	save(); 
}
//保存
function save(){
	
	post('/kefu/admin/kefuJS/save.json',kefuJS,function(data){
		msg.close();
		parent.checkLogin(data);	//验证登录状态。如果未登录，那么跳转到登录页面
		
		if(data.result != '1'){
			msg.failure(data.info);
		}else {
			//成功
			msg.success('成功');
		}
	});
}

//重写 siteKefu.js 的打开对话框方法
//kefu.ui.chat.entry
var kefu = {
	ui:{
		chat:{
			entry:function(){
				console.log('无动作');
				msg.info('我只是实时显示当前客服图标的颜色的，方便你设置颜色而已，点我没啥用。');
			}
		}
	}
};
//加载kefujs 的示例
function loadKefuJS(zuoxiid){
	var head0 = document.getElementsByTagName('head')[0];
	var msgScript = document.createElement("script");  //创建一个script标签
	msgScript.type = "text/javascript";
	msgScript.src = '/siteKefu.js?id='+zuoxiid;
	head0.appendChild(msgScript);
}


msg.loading('加载中');
post('/kefu/admin/kefuJS/getKefuJS.json',{},function(data){
	parent.checkLogin(data);	//验证登录状态。如果未登录，那么跳转到登录页面
	
	if(data.result != '1'){
		msg.failure(data.info);
	}else {
		//成功
		kefuJS = data.kefuJS;
		document.getElementById('color').value = data.kefuJS.color;
		document.getElementById('backgroundColor').value = data.kefuJS.backgroundColor;
		/* document.getElementById('color').innerHTML = data.kefuJS.color;
		document.getElementById('backgroundColor').innerHTML = data.kefuJS.backgroundColor; */
		//setKefuColor(data.kefuJS.color);
		//setKefuBackgroundColor(data.kefuJS.backgroundColor);
		
		post('/kefu/admin/zuoxi/getZuoxiByUserid.json',{userid: data.kefu.userid},function(zuoxi){
			loadKefuJS(zuoxi.id);
			
			post('/kefu/chat/config/getNetConfig.json',{},function(config){
				msg.close();
				
				if(config.result != '1'){
					msg.failure(config.info);
				}else {
					//成功
					console.log(config);
					//显示js的路径
					document.getElementById('jssrc').innerHTML = document.getElementById('jssrc').innerHTML.trim().replace('{zuoxiid}',zuoxi.id).replace('{api}',config.api);
				}
			});
			
		});
		
	}
});
</script>
<jsp:include page="../../common/foot.jsp"></jsp:include>  