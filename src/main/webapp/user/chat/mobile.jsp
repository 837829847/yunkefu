<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><!DOCTYPE html>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, height=device-height, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="format-detection" content="telephone=no">
<title>LayIM 移动版</title>

<link rel="stylesheet" href="/module/layim/css/layui.mobile.css">
<script src="https://res.zvo.cn/shop/shop.js"></script>
<script src="https://res.zvo.cn/request/request.js"></script>
</head>
<body>

<script src="/module/layim/layui.js"></script>
<script>
layui.config({
  version: true
}).use('mobile', function(){
  var mobile = layui.mobile
  ,layim = mobile.layim
  ,layer = mobile.layer;
  
  //建立WebSocket通讯
  //注意：如果你要兼容ie8+，建议你采用 socket.io 的版本。下面是以原生WS为例
  var socket = new WebSocket('ws://localhost:8081');
  
  //连接成功时触发
  socket.onopen = function(){
    socket.send(JSON.stringify({
			  'type': 'open' //第一次联通，登录
			  ,'token':shop.getToken()
			})); 
  };
	
	//监听收到的消息
  socket.onmessage = function(res){
    //res为接受到的值，如 {"emit": "messageName", "data": {}}
    //emit即为发出的事件名，用于区分不同的消息
	var json = JSON.parse(res.data);
	console.log(json);
	  layim.getMessage(json);
  };
  
  
  
  //提示层
  layer.msg = function(content){
    return layer.open({
      content: content
      ,skin: 'msg'
      ,time: 2 //2秒后自动关闭
    });
  };
  
//加载好友列表
request.post('/friendList.json',{}, function(data){
	layim.config({
		init: data.data
	});
});

layim.config({
    //上传图片接口
    uploadImage: {
      url: '/upload/image' //（返回的数据格式见下文）
      ,type: '' //默认post
    }
    
    //上传文件接口
    ,uploadFile: {
      url: '/upload/file' //（返回的数据格式见下文）
      ,type: '' //默认post
    }
    
    
    //扩展聊天面板工具栏
    ,tool: [{
      alias: 'code'
      ,title: '代码'
      ,iconUnicode: '&#xe64e;'
    }]
    
    //扩展更多列表
    ,moreList: [{
      alias: 'find'
      ,title: '发现'
      ,iconUnicode: '&#xe628;' //图标字体的unicode，可不填
      ,iconClass: '' //图标字体的class类名
    },{
      alias: 'share'
      ,title: '分享与邀请'
      ,iconUnicode: '&#xe641;' //图标字体的unicode，可不填
      ,iconClass: '' //图标字体的class类名
    }]
    
    //,tabIndex: 1 //用户设定初始打开的Tab项下标
    //,isNewFriend: false //是否开启“新的朋友”
    ,isgroup: true //是否开启“群聊”
    //,chatTitleColor: '#c00' //顶部Bar颜色
    //,title: 'LayIM' //应用名，默认：我的IM
});
  //监听点击“新的朋友”
  layim.on('newFriend', function(){
    layim.panel({
      title: '新的朋友' //标题
      ,tpl: '<div style="padding: 10px;">自定义模版，{{d.data.test}}</div>' //模版
      ,data: { //数据
        test: '么么哒'
      }
    });
  });
  
  //查看聊天信息
  layim.on('detail', function(data){
    //console.log(data); //获取当前会话对象
    layim.panel({
      title: data.name + ' 聊天信息' //标题
      ,tpl: '<div style="padding: 10px;">自定义模版，<a href="http://www.layui.com/doc/modules/layim_mobile.html#ondetail" target="_blank">参考文档</a></div>' //模版
      ,data: { //数据
        test: '么么哒'
      }
    });
  });
  
  //监听点击更多列表
  layim.on('moreList', function(obj){
    switch(obj.alias){
      case 'find':
        layer.msg('自定义发现动作');
        
        //模拟标记“发现新动态”为已读
        layim.showNew('More', false);
        layim.showNew('find', false);
      break;
      case 'share':
        layim.panel({
          title: '邀请好友' //标题
          ,tpl: '<div style="padding: 10px;">自定义模版，{{d.data.test}}</div>' //模版
          ,data: { //数据
            test: '么么哒'
          }
        });
      break;
    }
  });
  
  //监听返回
  layim.on('back', function(){
    //如果你只是弹出一个会话界面（不显示主面板），那么可通过监听返回，跳转到上一页面，如：history.back();
  });
  
  //监听自定义工具栏点击，以添加代码为例
  layim.on('tool(code)', function(insert, send){
    insert('[pre class=layui-code]123[/pre]'); //将内容插入到编辑器
    send();
  });
  
  //监听发送消息
  layim.on('sendMessage', function(data){
    var To = data.to;
    //console.log(data);
		//监听到上述消息后，就可以轻松地发送socket了，如：
	  socket.send(JSON.stringify({
	    token: shop.getToken() //随便定义，用于在服务端区分消息类型
	    ,data: data
	  })); 
  });
  
  //监听查看更多记录
  layim.on('chatlog', function(data, ul){
    console.log(data);
    layim.panel({
      title: '与 '+ data.name +' 的聊天记录' //标题
      ,tpl: '<div style="padding: 10px;">这里是模版，{{d.data.test}}</div>' //模版
      ,data: { //数据
        test: 'Hello'
      }
    });
  });
  
  //模拟"更多"有新动态
  layim.showNew('More', true);
  layim.showNew('find', true);
});
</script>
</body>
</html>

