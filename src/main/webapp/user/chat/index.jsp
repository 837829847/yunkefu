<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>1LayIM 3.x PC版本地演示</title>

<link rel="stylesheet" href="/module/layim/css/layui.css">
<style>
html{background-color: #333;}
</style>
<script src="https://res.zvo.cn/shop/shop.js"></script>
</head>
<body>


<script src="/module/layim/layui.js"></script>
<script>

if(!/^http(s*):\/\//.test(location.href)){
  alert('请部署到localhost上查看该演示');
}


layui.use('layim', function(layim){
  //建立WebSocket通讯
  //注意：如果你要兼容ie8+，建议你采用 socket.io 的版本。下面是以原生WS为例
  var socket = new WebSocket('ws://localhost:8081');
  
  //连接成功时触发
  socket.onopen = function(){
    socket.send(JSON.stringify({
			  'type': 'open' //第一次联通，登录
			  ,'token':shop.getToken()
			})); 
  };
  
	//监听收到的消息
	socket.onmessage = function(res){
	    //res为接受到的值，如 {"emit": "messageName", "data": {}}
	    //emit即为发出的事件名，用于区分不同的消息
		var json = JSON.parse(res.data);
		console.log(json);
		layim.getMessage(json);
	};
  
  //基础配置
  layim.config({

    //初始化接口
    init: {
      url: '/friendList.json'
      ,type: 'post'
      ,data: {}
    }

    //查看群员接口
    ,members: {
      url: 'json/getMembers.json'
      ,data: {}
    }
    
    //上传图片接口
    ,uploadImage: {
      url: '/upload/image' //（返回的数据格式见下文）
      ,type: '' //默认post
    } 
    
    //上传文件接口
    ,uploadFile: {
      url: '/upload/file' //（返回的数据格式见下文）
      ,type: '' //默认post
    }
    
    ,isAudio: true //开启聊天工具栏音频
    ,isVideo: true //开启聊天工具栏视频
    
    //扩展工具栏
    ,tool: [{
      alias: 'code'
      ,title: '代码'
      ,icon: '&#xe64e;'
    }]
    
    //,brief: true //是否简约模式（若开启则不显示主面板）
    
    //,title: 'WebIM' //自定义主面板最小化时的标题
    //,right: '100px' //主面板相对浏览器右侧距离
    //,minRight: '90px' //聊天面板最小化时相对浏览器右侧距离
    ,initSkin: '5.jpg' //1-5 设置初始背景
    //,skin: ['aaa.jpg'] //新增皮肤
    ,isfriend: false //是否开启好友
    ,isgroup: false //是否开启群组
    //,min: true //是否始终最小化主面板，默认false
    ,notice: true //是否开启桌面消息提醒，默认false
    //,voice: false //声音提醒，默认开启，声音文件为：default.mp3
    
    ,msgbox: layui.cache.dir + 'css/modules/layim/html/msgbox.html' //消息盒子页面地址，若不开启，剔除该项即可
    ,find: layui.cache.dir + 'css/modules/layim/html/find.html' //发现页面地址，若不开启，剔除该项即可
    ,chatLog: layui.cache.dir + 'css/modules/layim/html/chatlog.html' //聊天记录页面地址，若不开启，剔除该项即可
    
  });

  //监听在线状态的切换事件
  layim.on('online', function(data){
    console.log(data);
  });
  
  //监听签名修改
  layim.on('sign', function(value){
    //console.log(value);
  });

  //监听自定义工具栏点击，以添加代码为例
  layim.on('tool(code)', function(insert){
    layer.prompt({
      title: '插入代码'
      ,formType: 2
      ,shade: 0
    }, function(text, index){
      layer.close(index);
      insert('[pre class=layui-code]' + text + '[/pre]'); //将内容插入到编辑器
    });
  });
  
  //监听layim建立就绪
  layim.on('ready', function(res){
    //console.log(res.mine);
    //layim.msgbox(5); //模拟消息盒子有新消息，实际使用时，一般是动态获得
  });

  //监听发送消息
  layim.on('sendMessage', function(data){
    var To = data.to;
    console.log(data);
	
    
   /*  if(To.type === 'friend'){
      layim.setChatStatus('<span style="color:#FF5722;">对方正在输入。。。</span>');
    } */
    
      //监听到上述消息后，就可以轻松地发送socket了，如：
	  socket.send(JSON.stringify({
	    token: shop.getToken() //随便定义，用于在服务端区分消息类型
	    ,data: data
	  })); 
  });

  //监听查看群员
  layim.on('members', function(data){
    //console.log(data);
  });
  
  //监听聊天窗口的切换
  layim.on('chatChange', function(res){
    var type = res.data.type;
    console.log(res)
    if(type === 'friend'){
      //模拟标注好友状态
      //layim.setChatStatus('<span style="color:#FF5722;">在线</span>');
    } else if(type === 'group'){
      //模拟系统消息
      layim.getMessage({
        system: true
        ,id: res.data.id
        ,type: "group"
        ,content: '模拟群员'+(Math.random()*100|0) + '加入群聊'
      });
    }
  });

});
</script>
</body>
</html>
