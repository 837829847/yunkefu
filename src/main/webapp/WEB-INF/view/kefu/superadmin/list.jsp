<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/wm/common/head.jsp">
	<jsp:param name="title" value="客服管理"/>
</jsp:include>

<jsp:include page="/wm/common/list/formSearch_formStart.jsp" ></jsp:include>
	<jsp:include page="/wm/common/list/formSearch_input.jsp">
		<jsp:param name="iw_label" value="用户名"/>
		<jsp:param name="iw_name" value="username"/>
	</jsp:include>
	<input class="layui-btn iw_list_search_submit" type="submit" value="搜索" />
	
	<div style="float: right;">
		<script type="text/javascript"> orderBy('id_DESC=编号,lasttime_DESC=最后登陆时间'); </script>
	</div>
	<a href="add.do" class="layui-btn layui-btn-normal" style="float: right; margin-right:10px;">开通客服</a>
</form>	


<table class="layui-table iw_table">
	<thead>
		<tr>
			<th>用户编号</th>
			<th>登陆用户名</th>
			<th>最后上线时间</th>
			<th>状态</th>
			<th>操作</th>
		</tr> 
	</thead>
	<tbody>
		<c:forEach items="${list}" var="obj">
			<tr>
				<td style="width:75px;" class="ignore"><a href="">${obj['id'] }</a></td>
				<td class="ignore" style="width:100px;">${obj['username'] }</td>
				<td style="width:100px;" class="ignore"><x:time linuxTime="${obj['lasttime'] }" format="yy-MM-dd hh:mm"></x:time></td>
				<td style="width:150px;">
					<c:choose>
					<c:when test="${obj['isfreeze'] == null or obj['isfreeze'] == 0 or obj['isfreeze'] == '' }">
						正常 <botton class="layui-btn layui-btn-sm" onclick="freeze('${obj['kefuid'] }','${obj['username'] }');" style="margin-left: 3px;">冻结</botton>
					</c:when>
					<c:when test="${obj['isfreeze'] == 1 }">
						已冻结 <botton class="layui-btn layui-btn-sm" onclick="unFreeze('${obj['kefuid'] }','${obj['username'] }');" style="margin-left: 3px;">解冻</botton>
					</c:when>
					</c:choose>
				</td>
				<td style="width:100px;">
					<botton class="layui-btn layui-btn-sm" onclick="updatePassword('${obj['id'] }','${obj['username'] }');" style="margin-left: 3px;">改密</botton>
					<botton class="layui-btn layui-btn-sm" onclick="deleteKefu('${obj['kefuid'] }','${obj['username'] }');" style="margin-left: 3px;">删除</botton>
					
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<!-- 通用分页跳转 -->
<jsp:include page="/wm/common/page.jsp" ></jsp:include>

<div style="padding: 20px;color: gray;">
	<div>操作按钮提示:</div>
	<div>冻结：将客服冻结。冻结的客服（包括这个客服旗下的坐席账号）无法登陆。</div>
	<div>解冻：将冻结的客服解除冻结状态，解冻后客服恢复正常可登录状态。</div>
	<div>改密：更改密码。当用户忘记密码时，对其进行更改密码</div>
	<div>删除：删除客服以及客服下的所有坐席。</div>
</div>

<script type="text/javascript">
//冻结
function freeze(kefuid, name){
	layer.confirm('确定要冻结'+name+'吗?<br/>冻结后其将无法登录', {icon: 3, title:'确认冻结'}, function(index){
		parent.msg.loading('冻结中');
		$.getJSON("freeze.json?kefuid="+kefuid,function(result){
			parent.msg.close();
			if(result.result != '1'){
				msg.failure(result.info);
			}else{
				parent.msg.success('已冻结');
				location.reload();
			}
		});
		layer.close(index);
	});
}

//解除冻结，解冻
function unFreeze(kefuid, name){
	layer.confirm('确定要解冻'+name+'吗?<br/>解冻后其将会恢复正常使用', {icon: 3, title:'确认解冻'}, function(index){
		parent.msg.loading('解除中');
		$.getJSON("unFreeze.json?kefuid="+kefuid,function(result){
			parent.msg.close();
			if(result.result != '1'){
				msg.failure(result.info);
			}else{
				parent.msg.success('已解冻');
				location.reload();
			}
		});
		layer.close(index);
	});
}

//给我创建的站点修改站点密码
function updatePassword(userid, name){
	layer.prompt({
		formType: 0,
		value: '',
		title: '给'+name+'改密码，请输入新密码',
	}, function(value, index, elem){
		parent.msg.loading('更改中');
		$.post(
			"updatePassword.json", 
			{ "newPassword": value, userid:userid }, 
			function(data){
				parent.msg.close();	//关闭“更改中”的等待提示
				if(data.result != '1'){
					parent.msg.failure(data.info);
				}else{
					parent.msg.success('更改成功');
					location.reload();
				}
			}, 
		"json");
		
	});
}

function deleteKefu(id, name){
	var dtp_confirm = layer.confirm('确定要删除客服【'+name+'】？', {
		  btn: ['确认','取消'] //按钮
	}, function(){
		layer.close(dtp_confirm);
		parent.msg.loading("删除中");    //显示“操作中”的等待提示
		post('/kefu/superadmin/delete.json?id=' + id,{}, function(data){
		    parent.msg.close();    //关闭“操作中”的等待提示
			checkLogin(data);	//验证登录状态。如果未登录，那么跳转到登录页面
			if(data.result == '1'){
		        parent.msg.success('操作成功');
		        window.location.reload();	//刷新当前页
		     }else if(data.result == '0'){
		         parent.msg.failure(data.info);
		     }else{
		         parent.msg.failure();
		     }
		});
	}, function(){
		
	});
}


</script>

<jsp:include page="/wm/common/foot.jsp"></jsp:include> 