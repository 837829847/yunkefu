<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><!DOCTYPE html>
<jsp:include page="/wm/common/head.jsp">
	<jsp:param name="title" value="在线坐席"/>
</jsp:include>
<script src="/module/wm/wm.js"></script>
<script src="//res.zvo.cn/kefu/kefu.js"></script>
<link rel="stylesheet" type="text/css" href="//res.zvo.cn/kefu/css/style.css" media="screen">
<script src="/plugin/videoCall/js/kefujs_extend_videoCall.js"></script>

<style>
/*版权信息*/
#pc #chat_footer #copyright{
	display:none;
}
#list{
	overflow-y: auto!important;
	overflow-x: hidden!important;
	background-color: #f7f7f7!important;
	border-right: 1px solid #f2f2f2;
	box-shadow: 1px 1px 10px 0 #ddd;
	max-width: 40rem; 
	width: 22rem;
	position: absolute; 
	background: aliceblue;  
	height: 100%; 
	overflow: scroll;
}
</style>
</head>
<body>

<div id="list"></div>
<div style="float: left;  max-width: 400px;">
	<div id="chat"></div>
</div>
<div id="tishi" style=" position: absolute; bottom: 0.5rem; left: 50%; margin-left: -9rem; color: gray; font-size: 1.5rem; opacity: 0.5;">
	当前客服坐席在线中，等待客户发起咨询
</div>

<script type="text/javascript">
/****** 文件插件 ******/
kefu.extend.file.icon = '<?xml version="1.0" standalone="no"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg t="1619840321163" class="icon" viewBox="0 0 1127 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4177" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><style type="text/css"></style></defs><path d="M989.541 940.667h-858.923c0 0-68.409 10.642-68.409-80.572v-737.306c0 0 1.521-82.091 85.132-82.091h301.003c0 0 36.486-7.601 66.89 39.525 28.884 45.607 45.607 74.491 45.607 74.491 0 0 10.642 12.161 34.965 12.161-21.283 0 387.655 0 387.655 0 0 0 68.409-7.601 68.409 68.409v629.371c0 0 10.642 76.012-62.33 76.012zM925.692 362.984c0-18.243-15.202-33.445-33.445-33.445h-668.896c-19.763 0-34.965 15.203-34.965 33.445v3.040c0 19.763 15.202 34.965 34.965 34.965h668.896c18.243 0 33.445-15.203 33.445-34.965v-3.040z" fill="{color}" p-id="4178"></path></svg>';
/****** 文件插件结束 ******/

var config = {};

function loadKefu(){
	var host = config.api;
	kefu.api.domain = host;
	kefu.socket.url = config.socket;
	 
	//kefu.js 初始化
	//kefu.init();
	//出现列表页
	//kefu.ui.list.entry();
	//kefu.notification.use=false;
	//初始化pc端客服坐席
	kefu.ui.chat.pc.init();
	//出现列表页
	kefu.ui.list.entry();
}

msg.loading('加载中');
post('/kefu/chat/config/getConfig.json',{},function(data){
	msg.close();
	
	if(data.result != '1'){
		msg.failure(data.info);
	}else {
		//成功
		if(data.js != '' && data.js.length > 0){
			eval(data.js);
		}
		
		console.log(data);
		config = data;
		loadKefu();
	}
});

//显示底部的提示信息，是否在线状态
function showTishi(){
	var currentTime = Date.parse(new Date())/1000;
	currentTimeStr = formatTime(currentTime, "h:m:s");
	try {
		if(kefu.socket.socket.readyState == 1){
			document.getElementById('tishi').innerHTML = '当前客服坐席在线中，等待客户发起咨询  -- '+currentTimeStr;
		}else{
			document.getElementById('tishi').innerHTML = '<span style="color:red;">当前客服坐席掉线，正在重新连接中...... </span>'+currentTimeStr;
		}
	} catch (e) {
		console.log(e);
	}
}

//循环执行，每隔3秒钟执行一次showalert（）
window.setInterval(showTishi, 1000);

</script>

</body>
</html>