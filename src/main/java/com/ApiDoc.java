package com;

import com.xnx3.doc.JavaDoc;

public class ApiDoc {
	public static void main(String[] args) {
		JavaDoc doc = new com.xnx3.doc.JavaDoc("com.xnx3.kefu.chat");
//		doc.templatePath = "/Users/apple/Downloads/javadoc/";
		doc.templatePath = "http://res.zvo.cn.obs.cn-north-4.myhuaweicloud.com/javadoc/v1.10/";
		
		doc.name = "雷鸣云客服-API文档";
		doc.domain = "http://shop.imall.net.cn";
		doc.version = "1.3";
		doc.welcome = "雷鸣云客服对外开放接口及跟kefu.js对接的一些接口描述";
		
		doc.generateHtmlDoc();
	}
}
