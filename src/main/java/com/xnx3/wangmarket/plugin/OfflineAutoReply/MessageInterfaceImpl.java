package com.xnx3.wangmarket.plugin.OfflineAutoReply;

import com.xnx3.kefu.core.bean.ChatUserBean;
import com.xnx3.kefu.core.entity.Kefu;
import com.xnx3.kefu.core.pluginManage.interfaces.MessageInterface;
import com.xnx3.kefu.core.util.KefuUtil;
import com.xnx3.kefu.core.util.SocketUtil;
import com.xnx3.kefu.core.vo.MessageReceiveVO;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;

/**
 * 离线消息自动回复，消息接口的实现
 * @author 管雷鸣
 *
 */
public class MessageInterfaceImpl implements MessageInterface{

	@Override
	public void offlineMessage(Channel channel, MessageReceiveVO message) {
		//通过当前的Channel获取通道所属的用户
		ChatUserBean chatUser = SocketUtil.getUser(channel);
		if(chatUser == null){
			//异常的
			return;
		}
		
		Kefu kefu = KefuUtil.getKefu(message.getReceiveId());
		if(kefu != null && kefu.getOfflineAutoRaply() != null && kefu.getOfflineAutoRaply().length() > 1){
			//设置了自动回复
			SocketUtil.sendSystemMessage(message.getReceiveId(), chatUser.getChatid(), kefu.getOfflineAutoRaply());
		}else{
			//无自动回复，回复固定文字
			SocketUtil.sendSystemMessage(message.getReceiveId(), chatUser.getChatid(), "对方已经离线");
		}
		
		//推送离线消息
		//OfficeMessagePush.push(message);
	}

	@Override
	public void online(Channel channel) {
	}

	@Override
	public void offline(Channel channel) {
	}

	@Override
	public void autoReply(Channel channel, String otherChatId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean messageReceivedChat(Channel channel, MessageReceiveVO msg) {
		// TODO Auto-generated method stub
		return true;
	}

}
