package com.xnx3.wangmarket.plugin.ZuoxiBusy.util;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.xnx3.j2ee.util.ConsoleUtil;
import com.xnx3.kefu.core.util.SocketUtil;

import net.sf.json.JSONObject;

/**
 * 坐席排队方面的工具类
 * @author 管雷鸣
 *
 */
public class ZuoxiPaiduiUtil {
	/*
	 * 用户的排队状态记录。  
	 * key:用户的chatid，这里就是咨询用户的，不会是坐席的。因为这个就是用户是否是在排队等坐席
	 * value:用户当前所排队的坐席id，zuoxiid。 如果存在，且value长度>10则是正在排队等待；如果不存在或者value长度小于10则是未在排队中
	 */
	public static Map<String, String> userPaiduiStateMap;
	
	public static void main(String[] args) {
		String zuoxiid = "zzz";
		
		addChatidByPaiduiList(zuoxiid, "1");
		addChatidByPaiduiList(zuoxiid, "2");
		addChatidByPaiduiList(zuoxiid, "3");
		addChatidByPaiduiList(zuoxiid, "4");
		
		System.out.println(getChatidByPaiduiList(zuoxiid));
		System.out.println(getChatidByPaiduiList(zuoxiid));
		
		System.out.println(JSONObject.fromObject(paiduiMap));
	}
	
	
	/*
	 * 坐席排队的list。
	 * key:zuoxi.id
	 * value:这个坐席当前的排队人员。 
	 * 		list item 便是 排队用户的聊天id，chatid
	 */
	public static Map<String, List<String>> paiduiMap;
	static{
		paiduiMap = new HashMap<String, List<String>>();
		userPaiduiStateMap = new HashMap<String, String>();
	}
	
	/**
	 * 获取指定zuoxi的等待人员列表
	 * @param zuoxiid zuoxi.id
	 * @return 如果这个坐席没有等待的人，返回的也不会是空，只不过size() == 0
	 */
	public static List<String> getZuoxiPaiduiList(String zuoxiid){
		List<String> list = paiduiMap.get(zuoxiid);
		if(list == null){
			list = new LinkedList<String>();
			paiduiMap.put(zuoxiid, list);
		}
		return list;
	}

	/**
	 * 向排队中增加一个等待的用户chatid
	 * @param zuoxiid 排队的坐席id
	 * @param chatid 等待咨询的用户的chatid
	 */
	public static void addChatidByPaiduiList(String zuoxiid,String chatid){
		getZuoxiPaiduiList(zuoxiid).add(chatid);
		
		//标记这个用户正在排队中了
		userPaiduiStateMap.put(chatid, zuoxiid);
	}
	
	/**
	 * 从排队的里面，取第一个值。如果不在线，那就丢弃，继续往下取
	 * @param zuoxiid 排队的坐席id
	 * @return 如果当前排队里面没有排队的人了，那么返回null
	 */
	public static String getChatidByPaiduiList(String zuoxiid){
		List<String> list = getZuoxiPaiduiList(zuoxiid);
		
		String chatid = "";
		while(chatid.length() == 0){
			if(list.size() == 0){
				return null;
			}
			
			String chatid_linshi = list.get(0);
			//删除第一个元素
			list.remove(0);
			
			//将当前咨询用户的排队状态去掉，不在是排队状态
			userPaiduiStateMap.remove(chatid_linshi);
			ConsoleUtil.info("userPaiduiStateMap remove "+chatid_linshi+", size:"+userPaiduiStateMap.size());
			
			//判断这个chatid的通道是否存在，用户还是否在线
			if(SocketUtil.getChannel(chatid_linshi).size() > 0){
				chatid = chatid_linshi;
				break;
			}
			
			//不在线了，那就在获取下一个
			ConsoleUtil.info("chatid : "+chatid_linshi+"  socket不在线，继续获取下一个");
		}
		
		return chatid;
	}
	
}
