package com.xnx3.wangmarket.plugin.zuoxiHiddenPlugin;

import com.xnx3.j2ee.Func;
import com.xnx3.j2ee.entity.User;
import com.xnx3.j2ee.pluginManage.interfaces.SuperAdminIndexInterface;
import com.xnx3.j2ee.util.SessionUtil;

/**
 * 坐席后台中，不显示功能插件。功能插件只是总管理后台进行显示
 * @author 管雷鸣
 *
 */
public class Plugin implements SuperAdminIndexInterface{

	@Override
	public String superAdminIndexAppendHtml() {
		User user = SessionUtil.getUser();
		if(user == null) {
			return null;
		}
		
		if(Func.isAuthorityBySpecific(user.getAuthority(), com.xnx3.kefu.core.Global.ROLE_ID_ZUOXI+"")) {
			//坐席后台，不显示
			String js = "<script> document.getElementById('plugin').style.display = 'none'; </script>";
			return js;
		}
		
		return null;
	}
	
}
