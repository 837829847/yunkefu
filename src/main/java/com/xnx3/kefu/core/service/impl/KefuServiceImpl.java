package com.xnx3.kefu.core.service.impl;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Service;
import com.xnx3.Lang;
import com.xnx3.j2ee.entity.User;
import com.xnx3.j2ee.service.SqlCacheService;
import com.xnx3.j2ee.service.SqlService;
import com.xnx3.j2ee.service.UserService;
import com.xnx3.j2ee.util.ActionLogUtil;
import com.xnx3.j2ee.util.ConsoleUtil;
import com.xnx3.j2ee.util.SafetyUtil;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.kefu.core.Global;
import com.xnx3.kefu.core.entity.Kefu;
import com.xnx3.kefu.core.entity.Zuoxi;
import com.xnx3.kefu.core.service.KefuService;
import com.xnx3.kefu.core.util.SessionUtil;
import com.xnx3.kefu.core.util.UserUtil;
import com.xnx3.kefu.core.vo.KefuVO;

@Service("kefuService")
public class KefuServiceImpl implements KefuService {
	//从cache中，通过userid，取这个用户对应的kefu.id信息
//	public static String cache_getKefuidByUserid = "kefu:kefuUser:userid:{userid}"; 
	
	@Resource
	private SqlCacheService sqlCacheService;
	@Resource
	private UserService userService;
	@Resource
	private SqlService sqlService;
	
	@Override
	public KefuVO createKefu(HttpServletRequest request) {
		User user = new User();
		user.setUsername(Lang.uuid());
		user.setPassword(Lang.uuid());
		return createKefu(user, request);
	}

	@Override
	public Kefu getKefuByUserid(String userid) {
		if(!UserUtil.isNumber(userid)){
			return null;
		}
		int uid = Lang.stringToInt(userid, 0);
		if(uid == 0){
			return null;
		}
		
//		String key = cache_getKefuidByUserid.replace("{userid}", uid+"");
//		Object obj = CacheUtil.get(key);
//		int kefuid = 0;
//		if(obj == null){
//			
//			//直接从 kefu 表中取
//			Kefu kefu = sqlCacheService.findAloneByProperty(Kefu.class, "userid", uid);
//			if(kefu == null){
//				//客服不存在
//				kefuid = 0;
//			}else{
//				kefuid = kefu.getId();
//			}
//			//缓存kefuid
//			CacheUtil.setYearCache(key, kefuid);
//		}else{
//			kefuid = (int) obj;
//		}
//		
//		if(kefuid == 0){
//			//没有对应的客服
//			return null;
//		}
//		
//		Kefu kefu = sqlCacheService.findById(Kefu.class, kefuid);
		
		Kefu kefu = sqlCacheService.findAloneByProperty(Kefu.class, "userid", uid);
		return kefu;
	}

	@Override
	public KefuVO createKefu(User user, HttpServletRequest request) {
		KefuVO vo = new KefuVO();
		
		if(user.getNickname() == null || user.getNickname().equals("")){
			user.setNickname("客服名称");
		}
		if(user.getHead() == null || user.getHead().length() == 0){
			user.setHead("https://res.zvo.cn/kefu/images/head.png"); 	//默认头像
		}
		
		BaseVO regvo = userService.createUser(user, request);
		if(regvo.getResult() - BaseVO.FAILURE == 0){
			//出错
			ConsoleUtil.log(regvo.getInfo());
			vo.setBaseVO(regvo.getResult(), regvo.getInfo());
			return vo;
		}
		
		int userid = Lang.stringToInt(vo.getInfo(), 0);
		
		Kefu kefu = new Kefu();
		kefu.setUseOffLineEmail(Kefu.USE_FALSE);
		kefu.setUseKefu(Kefu.USE_TRUE);
		kefu.setUseOffLineSms(Kefu.USE_FALSE);
		kefu.setUserid(user.getId());
		kefu.setChatid(Lang.uuid());
		sqlService.save(kefu);
		
		if(kefu.getId() != null && kefu.getId() > 0){
			//将kefuid写入user中
			sqlService.executeSql("UPDATE user SET kefuid = "+kefu.getId()+" WHERE id = "+userid);
			vo.setKefu(kefu);
			
			//给这个客服开通默认的坐席
			Zuoxi zuoxi = new Zuoxi();
			zuoxi.setId(Lang.uuid());
			zuoxi.setKefuid(kefu.getId());
			zuoxi.setUserid(kefu.getUserid());
			sqlService.save(zuoxi);
			vo.setZuoxiid(zuoxi.getId());
		}else{
			//出错，客服保存出错，几率极小，日志记录
			vo.setBaseVO(BaseVO.FAILURE, "写入出错，请重试");
		}
		
		return vo;
	}

	@Override
	public Kefu getKefuByZuoxiid(String zuoxiid) {
		Zuoxi zuoxi = sqlCacheService.findById(Zuoxi.class, zuoxiid);
		if(zuoxi == null){
			return null;
		}
		//直接从 kefu 表中取
		Kefu kefu = sqlCacheService.findById(Kefu.class, zuoxi.getKefuid());
		return kefu;
	}

	@Override
	public Zuoxi getZuoxiByUserid(int userid) {
		Zuoxi zuoxi = sqlCacheService.findAloneByProperty(Zuoxi.class, "userid", userid);
		return zuoxi;
	}

	@Override
	public String getZuoxiidByUserid(int userid) {
		Zuoxi zuoxi = getZuoxiByUserid(userid);
		if(zuoxi == null){
			return "notfind_zuoxi_useridis"+userid;
		}
		
		return zuoxi.getId();
	}

	@Override
	public BaseVO createZuoxi(HttpServletRequest request, User user, Kefu kefu) {
		
		//添加用户
		BaseVO regvo = userService.createUser(user, request);
		if(regvo.getResult() - BaseVO.FAILURE == 0){
			//出错
			ConsoleUtil.log(regvo.getInfo());
			return regvo;
		}
		
		int userid = Lang.stringToInt(regvo.getInfo(), 0);
		
		//添加坐席
		Zuoxi zuoxi = new Zuoxi();
		zuoxi.setId(Lang.uuid());
		zuoxi.setKefuid(kefu.getId());
		zuoxi.setUserid(userid);
		sqlService.save(zuoxi);
		
		//组合返回值
		BaseVO vo = new BaseVO();
		vo.setBaseVO(BaseVO.SUCCESS, zuoxi.getId());
		return vo;
	}

	@Override
	public BaseVO createZuoxi(HttpServletRequest request, String username, String password, String nickname, Kefu kefu) {
		BaseVO vo = new BaseVO();
		if(username.length() < 1){
			vo.setBaseVO(BaseVO.FAILURE, "请输入登录用户名");
			return vo;
		}
		if(password.length() < 1){
			vo.setBaseVO(BaseVO.FAILURE, "请输入登录密码");
			return vo;
		}
		if(nickname.length() < 1){
			vo.setBaseVO(BaseVO.FAILURE, "请输入客服昵称");
			return vo;
		}
		
		
		// 添加
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		user.setNickname(SafetyUtil.xssFilter(nickname));
		user.setHead("https://res.zvo.cn/kefu/images/head.png");	//默认头像
		user.setReferrerid(kefu.getUserid());
		user.setAuthority(Global.ROLE_ID_ZUOXI+"");
		return createZuoxi(request, user, kefu);
	}
	
}
