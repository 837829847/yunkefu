package com.xnx3.kefu.core.service;

import javax.servlet.http.HttpServletRequest;
import com.xnx3.j2ee.entity.User;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.kefu.core.entity.Kefu;
import com.xnx3.kefu.core.entity.Zuoxi;
import com.xnx3.kefu.core.vo.KefuVO;

/**
 * 客服平台相关
 * @author 管雷鸣
 *
 */
public interface KefuService {
	
	/**
	 * 创建客服平台。比如在总后台创建客服、或者第三方平台自动创建客服时用。会自动创建User、Kefu。 这里创建的username、password都是随机的uuid
	 * <p>创建好的客服平台会自带一个坐席（自动创建一个默认坐席）</p>
	 * @return 创建好的kefu
	 */
	public KefuVO createKefu(HttpServletRequest request);
	

	/**
	 * 创建客服平台。比如在总后台创建客服、或者第三方平台自动创建客服时用。会自动创建User、Kefu
	 * <p>创建好的客服平台会自带一个坐席（自动创建一个默认坐席）</p>
	 * @param user 要创建的 {@link User} 对象 , new一个，然后至少要传入 user.username 、 user.password
	 * @return 创建好的kefu
	 */
	public KefuVO createKefu(User user, HttpServletRequest request);
	
	/**
	 * 通过userid，取这个用户的kefu信息。当然，这个是有缓存层的。
	 * 已废弃，使用 getKefuByChatid(chatid)
	 * @param userid user.id ， 如果传入的是游客的id（字符串），那么就直接返回null
	 * @return {@link Kefu} 如果没有，则返回null
	 * @deprecated
	 */
	public Kefu getKefuByUserid(String userid);
	
	/**
	 * 通过zuoxi.id，取这个用户的所属客服平台的kefu信息。当然，这个是有缓存层的。
	 * @param zuoxiid zuoxi.id
	 * @return {@link Kefu} 如果没有，则返回null
	 */
	public Kefu getKefuByZuoxiid(String zuoxiid);
	
	/**
	 * 通过 user.id 来获取这个用户的 {@link Zuoxi} 信息
	 * @param userid 用户的User.id
	 * @return 坐席，如果获取不到，则返回null
	 */
	public Zuoxi getZuoxiByUserid(int userid);
	
	/**
	 * 通过 user.id 来获取这个用户的 {@link Zuoxi}.id
	 * @param userid 用户的User.id
	 * @return 坐席id，如果获取不到，则返回 notfind_zuoxi_useridis_123 (123是具体传入的userid)
	 */
	public String getZuoxiidByUserid(int userid);
	
	/**
	 * 给某个客服平台中创建坐席。一个客服平台下有多个坐席，这里就是添加多个坐席的
	 * @param user 要开通的用户的一些信息，可以只设置 user.username 、 user.password(明文存入即可)
	 * @param kefu 要开通的坐席所属于哪个客服平台
	 * @return 如果成功，info为创建的坐席id（zuoxi.id）
	 */
	public BaseVO createZuoxi(HttpServletRequest request, User user, Kefu kefu);
	
	
	/**
	 * 给某个客服平台中创建坐席。一个客服平台下有多个坐席，这里就是添加多个坐席的
	 * @param username 登录坐席的用户名
	 * @param password 登录坐席的密码
	 * @param nickname 坐席的昵称
	 * @param kefu 要开通的坐席所属于哪个客服平台
	 * @return 如果成功，info为创建的坐席id（zuoxi.id）
	 */
	public BaseVO createZuoxi(HttpServletRequest request, String username, String password, String nickname, Kefu kefu);
	
	
	
	
}