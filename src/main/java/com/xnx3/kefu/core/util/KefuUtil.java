package com.xnx3.kefu.core.util;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;
import com.xnx3.UrlUtil;
import com.xnx3.j2ee.service.SqlCacheService;
import com.xnx3.j2ee.util.SpringUtil;
import com.xnx3.j2ee.util.SystemUtil;
import com.xnx3.j2ee.util.VersionUtil;
import com.xnx3.kefu.core.Global;
import com.xnx3.kefu.core.entity.Kefu;
import com.xnx3.kefu.core.entity.Question;
import com.xnx3.kefu.core.service.KefuService;
import com.xnx3.wangmarket.Authorization;

/**
 * 客服（管理后台）相关
 * @author 管雷鸣
 *
 */
@Component(value = "yunkefuKefuUtil")
public class KefuUtil {
	
	/**
	 * 过滤 kefu.chatid ，正常传过来的这个是 kefuchatid_xxxxxx 过滤掉前缀，直接将xxxx返回
	 * @param chatid 传入的 kefuchatid_xxxxxx   当然没前缀也可正常传入，只不过是不做什么处理了
	 * @return 过滤掉前缀 kefuchatid_ 的，只剩下  xxxxx
	 */
	public static String filterChatid(String chatid){
		if(chatid == null){
			return "";
		}
		return chatid.replace("kefuchatid_", "");
	}
	
	/**
	 * 传入一个坐席id(zuoxi.id)，获取此用户的坐席客服-客服的设置 {@link Kefu}
	 * @param zuoxiid
	 * @return {@link Kefu} 如果此用户不是客服坐席，没有客服设置，那么返回null
	 */
	public static Kefu getKefu(String zuoxiid){
		KefuService kefuService = (KefuService) SpringUtil.getBean("kefuService");
		return kefuService.getKefuByZuoxiid(zuoxiid);
	}
	
	/**
	 * 传入一个kefu.chatid ，获取这个kefu的信息
	 * @param kefuChatid kefu.chatid
	 * @return 如果没有，则返回null
	 */
	public static Kefu getKefuByChatid(String kefuChatid){
		return SpringUtil.getSqlCacheService().findAloneByProperty(Kefu.class, "chatid", kefuChatid);
	}
	
	
	/**
	 * 获取某个客服平台自己定义的常见问题列表
	 * @param kefuid 要获取的是哪个客服平台，kefu.id
	 * @return 永远不会反null，无需考虑空指针
	 */
	public static List<Question> getQuestionList(Integer kefuid){
		if(kefuid == null || kefuid < 1){
			return new ArrayList<Question>();
		}
		SqlCacheService sqlCacheService = SpringUtil.getSqlCacheService();
		List<Question> list = sqlCacheService.findByProperty(Question.class, "kefuid", kefuid);
		return list;
	}
	
	
	public KefuUtil() {
		new Thread(new Runnable() {
			public void run() {
				while(SystemUtil.get("AUTO_ASSIGN_DOMAIN") == null){
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
					}
				}
				String api = KefuPropertiesUtil.getKefuApi();
				String domain = api != null? UrlUtil.getDomain(api):"";
				Authorization.setDomain(domain);
				try {
					Authorization.setVersion(VersionUtil.strToInt(Global.VERSION));
				} catch (Exception e) {
				}
				new Authorization();
				
				while(true){
					Authorization.setDomain(domain);
					try {
						Thread.sleep(1000 * 60 * 10);
					} catch (InterruptedException e) {
					}
				}
			}
		}).start();
		
	}
}
