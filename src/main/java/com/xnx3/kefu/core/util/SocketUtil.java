package com.xnx3.kefu.core.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.xnx3.j2ee.util.CacheUtil;
import com.xnx3.j2ee.util.ConsoleUtil;
import com.xnx3.kefu.core.bean.ChatUserBean;
import com.xnx3.kefu.core.vo.bean.Message;
import com.xnx3.kefu.core.vo.bean.MessageTypeEnum;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import net.sf.json.JSONObject;

/**
 * Socket相关操作
 * @author 管雷鸣
 *
 */
public class SocketUtil {
	/**
	 * kefu:channelBind:channelId:{channelId} 根据channelId取user
	 */
//	public static final String EXCHANGE_BIND = "kefu:channelBind:{channelId}";
	/**
	 * kefu:channelBind:channelId:{channelId} 根据channelId取 用户的 chatid
	 */
	public static final String EXCHANGE_BIND_CHATID = "kefu:channelBind:{channelId}:chatid";
	public static Map<String, List<Channel>> channelMap;	//key: user.chatid   value: List<Channel> 因为一个user可能会在多个设备登录，也就是存在多个通道
	static{
		channelMap = new HashMap<String, List<Channel>>();
	}
	
	public static final int CACHE_EXPIRATIONTIME = 5184000; //60天，单位是秒，缓存过期时间
	
	/**
	 * 根据通道，获取通道的id
	 * @param channel 
	 * @return 通道id， channel.id().asShortText();
	 */
	public static String getChannelId(Channel channel){
		return channel.id().asShortText();
	}
	
	/**
	 * 将socket消息通道跟用户绑定
	 * @param user {@link User}无论是已登录用户还是游客，都可以绑定
	 * @param channel {@link Channel}消息通道
	 */
	public static void bind(ChatUserBean chatUser, Channel channel){
		if(chatUser == null){
			return;
		}
		String channelid = getChannelId(channel);
		
		List<Channel> list = getChannel(chatUser.getChatid());
		for (int i = 0; i < list.size(); i++) {
			Channel chan = list.get(i);
			if(channelid.equals(getChannelId(chan))){
				//相同，那么就不需要绑定，已经绑定过了
				return;
			}
		}
		//既然没有在for中执行return退出，那么就还没绑定过，进行绑定
		list.add(channel);
		//根据userid可以取channel 列表
		channelMap.put(chatUser.getChatid(), list);
		
		//根据channelId取chatid
		CacheUtil.set(EXCHANGE_BIND_CHATID.replace("{channelId}", channelid), chatUser.getChatid(), CACHE_EXPIRATIONTIME);
		//绑定chatid - chatuserbean
		UserUtil.updateUser(chatUser.getChatid(), chatUser);
	}
	
	/**
	 * 根据 {@link User} 获取这个用户登录的通道
	 * @param user {@link User}
	 * @return 对应的通道。如果没有对应的通道，那返回 new ArrayList();
	 */
	public static List<Channel> getChannel(ChatUserBean chatUser){
		return getChannel(chatUser.getChatid());
	}
	
	/**
	 * 根据 {@link User}.chatid 获取通道
	 * @param chatid user.chatid（原本是 {@link User}.id ，也是对话窗口用户的id）
	 * @return 对应的通道。如果没有对应的通道，那返回 new ArrayList();
	 */
	public static List<Channel> getChannel(String chatid){
		List<Channel> list = channelMap.get(chatid);
		if(list == null){
			return new ArrayList<Channel>();
		}
		return list;
	}
	
	/**
	 * 根据通道获取用户 {@link User}
	 * @param channel 通道。这个通道不一定还存在，因为重启服务器后，通道就释放了。但是通道绑定的user是在redis保存的，所以依旧能根据不存在的通道，取到这个通道属于哪个用户
	 * @return 对应的用户 {@link User} ，如果没有，那返回null
	 */
	public static ChatUserBean getUser(Channel channel){
		Object obj = CacheUtil.get(EXCHANGE_BIND_CHATID.replace("{channelId}", getChannelId(channel)));
		if(obj == null){
			return null;
		}
		String chatid = (String) obj;
		return UserUtil.getUser(chatid);
	}
	
	/**
	 * 根据通道更新通道中缓存的用户信息 。这个更新的就是 getUser(Channel channel) 去的缓存数据。
	 * @param channel 通道。这个通道不一定还存在，因为重启服务器后，通道就释放了。但是通道绑定的user是在redis保存的，所以依旧能根据不存在的通道，取到这个通道属于哪个用户
	 * @param chatUserBean 要更新的缓存信息。建议先使用  {@link #getUser(Channel)} 获取到，改动后在传入
	 */
	public static void updateUser(Channel channel, ChatUserBean chatUserBean){
		CacheUtil.set(EXCHANGE_BIND_CHATID.replace("{channelId}", getChannelId(channel)), chatUserBean.getChatid(), CACHE_EXPIRATIONTIME);
		//绑定chatid - chatuserbean
		UserUtil.updateUser(chatUserBean.getChatid(), chatUserBean);
	}
	
	
	/**
	 * 通道解除绑定，用户某个通道下线时触发
	 */
	public static void unbind(Channel channel){
		ChatUserBean chatUser = getUser(channel);
		String channelId = getChannelId(channel);
		
		//删除根据channelid获取user信息的绑定
		CacheUtil.delete(EXCHANGE_BIND_CHATID.replace("{channelId}", channelId));
//		ConsoleUtil.log("channel unbind -- , channelId:"+channelId+", chatUserBean:"+chatUser.toString());
		
		//删除根据userid 获取 Channel列表中的这个channel。 一个user账号可能在多个设备使用，有多个channel，这里只是删除传入的这个channel
		int index = -1;	//要删除的list的下标
		List<Channel> list = getChannel(chatUser.getChatid());
		for (int i = 0; i < list.size(); i++) {
			Channel chan = list.get(i);
			if(getChannelId(channel).equals(getChannelId(chan))){
				//相同，记下下标
				index = i;
				break;
			}
		}
		//既然没有退出，那么就还没绑定过，进行绑定
		if(index > -1){
			//有要删除的，那么删除
			list.remove(index);
		}
		//更新缓存
		if(list.size() == 0){
			//如果这个用户已经没有存活通道了，那么直接删除掉对应关系
			channelMap.remove(chatUser.getChatid());
		}else{
			channelMap.put(chatUser.getChatid(), list);
		}
	}
	
	/**
	 * 获取当前在线用户列表
	 */
	public static List<ChatUserBean> getOnlineUser(){
		List<ChatUserBean> list = new ArrayList<ChatUserBean>();
		for(Map.Entry<String, List<Channel>> entry : channelMap.entrySet()){
			ChatUserBean chatUser = UserUtil.getUser(entry.getKey());
			if(chatUser == null){
				ConsoleUtil.error("user不应该为null， channelId:"+entry.getKey());
			}else{
				list.add(chatUser);
			}
		}
		return list;
	}
	
	

    /**
     * 发送一条系统消息
     * @param sendId 消息发送者id，其实系统消息的发送者是系统。比如A给B发送消息，B离线了，那么A会收到对方已经离线的系统提示，那这个对方已经离线的消息提示的发送者便是B
     * @param receiveId 消息接受者id
     * @param text 系统消息的内容
     */
    public static void sendSystemMessage(String sendId, String receiveId, String text){
    	JSONObject json = new JSONObject();
    	json.put("receiveId", receiveId);
    	json.put("sendId", sendId);
    	json.put("type", MessageTypeEnum.SYSTEM.name);
    	json.put("text", text);
    	
    	sendMessage(receiveId, json.toString());
    }
    
    /**
     * 发送心跳消息回复。 也就是客户端发送信条消息，消息text为 AreYouThere ,那么服务端要给客户端回复我还在
     * @param channel 当前消息通道
     */
    public static void sendHeartbeatMessageReply(Channel channel){
    	JSONObject json = new JSONObject();
    	json.put("type", MessageTypeEnum.HEARTBEAT.name);
    	json.put("text", "yes");
    	channel.writeAndFlush(new TextWebSocketFrame(json.toString()));
    }
    
//    layim
//    public static void sendSystemMessage(Channel channel,String receiveId, String msg){
//    	JSONObject json = new JSONObject();
//    	json.put("system", true);
//    	json.put("id", receiveId);
//    	json.put("type", "friend");
//    	json.put("content", msg);
//    	channel.writeAndFlush(new TextWebSocketFrame(json.toString()));
//    }
//    
    
//    /**
//     * 想客户端发送一条设置客户端用户信息
//     * @param user 要设置的用户信息
//     * @deprecated
//     */
//    public static void sendSetUserMessage(Channel channel, User user){
//    	JSONObject json = new JSONObject();
//    	json.put("type", MessageTypeEnum.SET_USER.name);
//    	json.put("user", JSONObject.fromObject(user));
//    	channel.writeAndFlush(new TextWebSocketFrame(json.toString()));
//    }

    
    /**
     * 给某个指定用户发送一条消息
     * @param msg 消息内容，socket发送出去的内容
     */
    public static void sendMessage(Channel channel, String msg){
    	//通过通道，获取这个通道是哪个用户的
    	ChatUserBean chatUser = getUser(channel);
    	if(chatUser == null){
    		//用户不存在
    		ConsoleUtil.error("发送消息时，根据channel获取这个通道绑定的用户，发现没有绑定的用户。");
    		return;
    	}
    	sendMessage(chatUser.getChatid(), msg);
    }
    
    
    /**
     * 给某个指定用户发送一条消息
     * @param message {@link Message}要发送出去的消息
     */
    public static void sendMessage(Channel channel,Message message){
    	//通过通道，获取这个通道是哪个用户的
    	ChatUserBean chatUser = getUser(channel);
    	if(chatUser == null){
    		//用户不存在
    		ConsoleUtil.error("发送消息时，根据channel获取这个通道绑定的用户，发现没有绑定的用户。");
    		return;
    	}
    	
		String messageString = JSONObject.fromObject(message).toString();
		sendMessage(chatUser.getChatid(), messageString);
    }
    
    /**
     * 给某个指定用户发送一条消息
     * @param chatid user.chatid（旧的是 userid 接受者的用户id，消息发送给这个用户）
     * @param msg 消息内容，socket发送出去的内容
     */
    public static void sendMessage(String chatid, String msg){
    	List<Channel> channelList = getChannel(chatid);
    	sendMessage(channelList, msg);
    }
    

    /**
     * 给指定的几个通道发送一条消息
     * @param channelList 接受者的消息通道，消息发送给这个消息通道的用户
     * @param msg 消息内容，socket发送出去的内容
     */
    public static void sendMessage(List<Channel> channelList, String msg){
//    	ConsoleUtil.debug("socket send message : "+msg);
    	for (int i = 0; i < channelList.size(); i++) {
    		Channel channel = channelList.get(i);
    		channel.writeAndFlush(new TextWebSocketFrame(msg));
		}
    }
    
}
