package com.xnx3.kefu.core.util;

import com.xnx3.Lang;
import com.xnx3.j2ee.util.CacheUtil;
import com.xnx3.j2ee.util.ConsoleUtil;
import com.xnx3.kefu.core.Global;
import com.xnx3.kefu.core.bean.ChatUserBean;

/**
 * token(sessionid) 相关
 * @author 管雷鸣
 *
 */
public class TokenUtil {
	
	/**
	 * key： token（sessionid），    value:chatid
	 */
	public static final String SESSION_BIND_CHATID = "kefu:tokenBind:{token}:chatid";
	
//	/**
//	 * 将 token、User 进行绑定
//	 * @param token 本平台的token
//	 * @param user {@link User}
//	 */
//	public static void bind(String token, ChatUserBean chatUser){
//		if(chatUser == null){
//			return;
//		}
//		//根据token取user
//		CacheUtil.setYearCache(SESSION_BIND.replace("{token}", token), chatUser);
//	}
//	
	/**
	 * 将 token、user.chatid 进行绑定。
	 * <p>绑定chatid后，会自动检查 UserUtil 中 chatid是否有对应的chatUserBean，如果没有，那么还要插入到 UserUtil中。如果已有，那就不管 UserUtil的了</p>
	 * @param token 本平台的token，也就是sessionid
	 * @param chatid user.chatid
	 */
	public static void bind(String token, ChatUserBean chatUserBean){
		if(chatUserBean == null){
			return;
		}
		//根据token取user，过期时间 60 天
		CacheUtil.set(SESSION_BIND_CHATID.replace("{token}", token), chatUserBean.getChatid(), 5184000);
 		
		//判断 UserUtil 中是否已经有chatid - chatuserbean 的对应，如果没有，就在更新userUtil，如果有了，那就不用管了
		if(UserUtil.getUser(chatUserBean.getChatid()) == null){
			UserUtil.updateUser(chatUserBean.getChatid(), chatUserBean);
		}
	}
	
//	/**
//	 * 根据token获取用户 {@link User}
//	 * @param token 也就是sessionid，如果已登录用户，是sessionid，如果是未登录用户，那就是uuid
//	 * @return 对应的用户 {@link User} ，如果没有，那返回null
//	 */
//	public static ChatUserBean getUser(String token){
//		Object obj = CacheUtil.get(SESSION_BIND.replace("{token}", token));
//		if(obj == null){
//			return null;
//		}
//		return (ChatUserBean)obj;
//	}
	
	/**
	 * 根据token获取用户 {@link User}.chatid
	 * @param token 也就是sessionid，如果已登录用户，是sessionid，如果是未登录用户，那就是uuid
	 * @return 对应的用户 {@link User}.chatid ，如果没有，那返回null
	 */
	public static String getChatid(String token){
		Object obj = CacheUtil.get(SESSION_BIND_CHATID.replace("{token}", token));
		if(obj == null){
			return null;
		}
		return (String)obj;
	}

	/**
	 * 解除绑定，用户下线时触发
	 */
	public static void unbind(String token){
		String chatid = getChatid(token);
		if(chatid != null){
			CacheUtil.delete(SESSION_BIND_CHATID.replace("{token}", token));
		}
		ConsoleUtil.log("session unbind -- , token:"+token+",  chatid:"+chatid);
	}
	
	/**
	 * 根据token创建一个游客身份
	 * @param token
	 */
	public static ChatUserBean generateYoukeUser(String token){
		if(token == null || token.length() < 1){
			token = "notfind_"+Lang.uuid();	//token 不存在，也就是这个用户并没有在线，那生成一个
		}else if(token.indexOf("youke_") != 0){
			//创建的游客token，自动添加上 youke_ 前缀
			token = "youke_"+token;
		}
		ChatUserBean chatUser = new ChatUserBean();
		chatUser.setHead(Global.youkeHead);
		//chatUser.setId(token);
		chatUser.setNickname("游客"+token.substring(7, 11));
		chatUser.setType("youke");
		chatUser.setChatid("youke_"+Lang.uuid());
		return chatUser;
	}
}
