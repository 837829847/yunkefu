package com.xnx3.kefu.core.util;

import java.io.IOException;
import java.util.Properties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Component;

import com.xnx3.BaseVO;

/**
 * 读取 kefu.properties 属性，可以直接使用 KefuPropertiesUtil.getProperty("key"); 进行调用
 * @author 管雷鸣
 *
 */
@Component
public class KefuPropertiesUtil {
	private static Properties properties;	//application.properties
	
	static{
		new KefuPropertiesUtil();
	}
	public KefuPropertiesUtil() {
		if(properties == null){
			try {
	            Resource resource = new ClassPathResource("/kefu.properties");//
	            properties = PropertiesLoaderUtils.loadProperties(resource);
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		}
	}
    
	/**
	 * 获取 kefu.properties 的配置属性
	 * @param key 要获取的配置的名字，如 database.name
	 * @return 获取的配置的值。需要判断是否为null
	 */
    public static String getProperty(String key){
    	if(properties == null){
    		return null;
    	}
    	return properties.getProperty(key);
    }
    
    /**
     * 获取 kefu.properties 中配置的 kefu.api
     * @return 如果未配置返回 null； 如果已配置，返回格式如 http://localhost:8080
     */
    public static String getKefuApi() {
    	String api = KefuPropertiesUtil.getProperty("kefu.api");
    	if(api == null || api.length() == 0) {
			return null;
		}
		//容错，判断api是否末尾多加了/
		if(api.lastIndexOf("/") + 1 == api.length()) {
			//域名最后是以/结尾，那去掉最后的 /
			api = api.substring(0, api.length() -1);
		}
		return api;
    }
}
