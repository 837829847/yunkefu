package com.xnx3.kefu.core.util;

import java.util.HashMap;
import java.util.Map;

import com.xnx3.j2ee.bean.ActiveUser;
import com.xnx3.j2ee.entity.User;
import com.xnx3.j2ee.util.CacheUtil;

/**
 * 
 * @author 管雷鸣
 *
 */
public class SessionUtil extends com.xnx3.j2ee.util.SessionUtil{
	private static Map<String, ActiveUser> tokenActiveUserMap;	//当未使用redis时，进行token、 activeUser 进行绑定，可以通过token取用户信息。这里主要只是开发环境才会用
	static{
		tokenActiveUserMap = new HashMap<String, ActiveUser>();
	}
	
	/**
	 * 从Shrio的Session中，获取指定SessionId的用户的缓存信息
	 * @param token 也就是SessionId
	 * @return 获取到的 {@link ActiveUser} 如果当前tokne没有对应的信息，也就是未登录、或者假token，那么返回null
	 */
	public static ActiveUser getActiveUser(String token){
		ActiveUser activeUser = null;
		if(CacheUtil.isUseRedis()){
			//使用了redis，那么就从shiro中取
			activeUser = com.xnx3.j2ee.util.SessionUtil.getActiveUser(token);
		}else{
			//没有使用redis，那么从java 内存中取
			activeUser = tokenActiveUserMap.get(token);
		}
		
		return activeUser;
	}
	
	/**
	 * 当未使用redis时，进行token、 activeUser 进行绑定，可以通过token取用户信息。这里主要只是开发环境才会用
	 * @param token
	 * @param acriveUser
	 */
	public static void setActiveUserForNotRedis(String token, ActiveUser acriveUser){
		tokenActiveUserMap.put(token, acriveUser);
	}
	
	/**
	 * 获取当前用户是否是已登录的客服或坐席角色
	 * @return 如果是，则返回true
	 */
	public static boolean isKefuZuoxi() {
		User user = getUser();
		if(user == null) {
			//未登录
			return false;
		}
		if(user.getAuthority() == null) {
			return false;
		}
		
		if(user.getAuthority().equals("1") || user.getAuthority().equals("10")) {
			return true;
		}else {
			return false;
		}
	}
}
