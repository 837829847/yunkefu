package com.xnx3.kefu.core.util;

import java.util.List;
import com.xnx3.DateUtil;
import com.xnx3.json.JSONUtil;
import com.xnx3.kefu.core.bean.ChatUserBean;
import com.xnx3.kefu.core.entity.Kefu;
import com.xnx3.kefu.core.entity.Question;
import com.xnx3.kefu.core.util.UserUtil;
import com.xnx3.kefu.core.vo.MessageReceiveVO;
import com.xnx3.kefu.core.vo.bean.Message;
import com.xnx3.kefu.core.vo.bean.MessageTypeEnum;
import com.xnx3.kefu.core.vo.bean.SendMessage;
import net.sf.json.JSONObject;

/**
 * 消息包装，将消息字符串格式化为指定格式
 * @author 管雷鸣
 *
 */
public class MessageUtil {
	
	/**
	 * 加工，获取发送到socket客户端的消息字符串
	 * @param sendId 发送者id,user.id ，如果用户未登录，是游客，这里可以是uuid
	 * @param receiveId 接收者id,user.id ，如果用户未登录，是游客，这里可以是uuid
	 * @param content 要发送的内容
	 * @return 
	 */
	public static String sendLayIm(String sendId, String receiveId, String content){
		ChatUserBean user = UserUtil.getUser(sendId);
		
		SendMessage send = new SendMessage();
		send.setUsername(user.getNickname());
		send.setAvatar(user.getHead());
		send.setId(sendId);
		send.setType("friend");
		send.setContent(content);
		send.setCid("0");
		send.setMine(false);
		send.setFromid(sendId);
		send.setTimestamp(DateUtil.timeForUnix13());
		return JSONObject.fromObject(send).toString();
	}
	
	/**
	 * 获取某个客服刚接通自动发送的内容json
	 * @param userid 客服的user.id 
	 * @return 返回null，则是没有要发送的。 直接用 socket 发送这个字符串
	 */
	public static String autoReply(String userid){
		ChatUserBean user = UserUtil.getUser(userid);
		if(user == null || user.getType().equals("youke")){
			//用户是游客，自然也不会设自动回复
			return null;
		}
		//对方用户是会员，那么看会员是否有绑定到的Kefu，是客服坐席
		Kefu kefu = KefuUtil.getKefu(user.getChatid());
		if(kefu != null){
			//响应顾客，设定的客服自动回复的内容
			String text = kefu.getAutoReply();
			if(text == null){
				text = "";
			}
			//获取常见问题
			List<Question> list = KefuUtil.getQuestionList(kefu.getId());
			
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < list.size(); i++) {
				Question question = list.get(i);
				sb.append("[li]"+question.getTitle()+"[/li]");
			}
			if(sb.length() > 0){
				text = text + "[ul]"+sb.toString()+"[/ul]";
			}
			
			if(text.length() == 0){
				return null;
			}
			return text;
		}
		
		return null;
	}
	
	/**
	 * 加工，获取发送到socket客户端的消息字符串
	 * @param sendId 发送者id,user.id ，如果用户未登录，是游客，这里可以是uuid
	 * @param receiveId 接收者id,user.id ，如果用户未登录，是游客，这里可以是uuid
	 * @param content 要发送的内容
	 * @return 
	 */
	public static String send(MessageReceiveVO messageReceive){
		//验证用户真实性，是否真的是这个用户发送的
//		User sendUser = UserUtil.getUser(messageReceive.getSendId());
		return send(messageReceive.getType(), messageReceive.getSendId(), messageReceive.getReceiveId(), messageReceive.getText(), messageReceive.getExtend());
	}
	

	/**
	 * 加工，获取发送到socket客户端的消息字符串
	 * @param type 消息类型，取值于 {@link MessageTypeEnum}
	 * @param sendId 发送者id,user.id ，如果用户未登录，是游客，这里可以是uuid
	 * @param receiveId 接收者id,user.id ，如果用户未登录，是游客，这里可以是uuid
	 * @param text 要发送的内容
	 * @param extend 扩展，如果没有传入null
	 * @return socket发送的消息字符串
	 */
	public static String send(String type,String sendId, String receiveId,  String text, JSONObject extend){
		MessageReceiveVO message = new MessageReceiveVO();
		message.setType(type);
		message.setReceiveId(receiveId);
		message.setSendId(sendId);
		message.setText(text);
		if(extend == null){
			extend = new JSONObject();
		}
		message.setExtend(extend);
		
		return JSONObject.fromObject(message).toString();
	}
	
	/**
	 * 接收 自己的im 发送来的消息，进行转化
	 * @param text 发送来的消息。格式如： {"token":"06e39913-cc11-4f01-a9cf-2b6c4a7853d6","data":{"mine":{"username":"纸飞机","avatar":"http://cdn.firstlinkapp.com/upload/2016_6/1465575923433_33812.jpg","id":"100000","mine":true,"content":"2323"},"to":{"username":"Lemon_CC","id":"102101","avatar":"http://tp2.sinaimg.cn/1833062053/180/5643591594/0","sign":"","name":"Lemon_CC","type":"friend"}}}
	 */
	public static MessageReceiveVO receive(String text){
		MessageReceiveVO msg = new MessageReceiveVO();
		JSONObject json = JSONObject.fromObject(text);
		String type = JSONUtil.getString(json, "type");
		msg.setType(type);
		
		//心跳,这种消息只会携带text参数
		if(type.equals(MessageTypeEnum.HEARTBEAT.name)){
			msg.setText(JSONUtil.getString(json, "text"));
			return msg;
		}
		
		//其他类型的消息，都会携带token
		if(json.get("token") == null){
			msg.setBaseVO(MessageReceiveVO.FAILURE, "token为空");
			return msg;
		}
		msg.setToken(JSONUtil.getString(json, "token"));
		
		if(type.equals(MessageTypeEnum.CONNECT.name)){
			//刚打开socket
		}else if(type.equals(MessageTypeEnum.AUTO_REPLY.name)){
			//获取坐席所在的客服系统自动回复的信息
			msg.setSendId(JSONUtil.getString(json, "sendId"));
			msg.setReceiveId(JSONUtil.getString(json, "receiveId"));
		}else if(type.equals(MessageTypeEnum.EXTEND.name)){
			//扩展，比如发送商品、订单
			if(json.get("extend") != null){
				msg.setExtend(json.getJSONObject("extend"));
			}
			msg.setSendId(JSONUtil.getString(json, "sendId"));
			msg.setReceiveId(JSONUtil.getString(json, "receiveId"));
		}else{
			//正常的msg消息
			msg.setText(JSONUtil.getString(json, "text"));
			msg.setSendId(JSONUtil.getString(json, "sendId"));
			msg.setReceiveId(JSONUtil.getString(json, "receiveId"));
		}
		
		return msg;
	}
	
	/**
	 * 常见问题自动回复
	 * @param receivdUserId 消息接收方的userid
	 * @param text 消息内容
	 * @return 如果没有匹配到自动回复的内容，那么返回null ，如果匹配到了，那么返回自动回复的内容文本
	 */
	public static String autoReplyQuestion(String receivdUserId, String text){
		if(text == null){
			return null;
		}
		if(UserUtil.isYouke(receivdUserId)){
			//接收方是游客，自然也没有自动回复了
			return null;
		}
		//消息的接收用户
		ChatUserBean user = UserUtil.getUser(receivdUserId);
		
		//查出这个Kefu实体类
		Kefu kefu = KefuUtil.getKefu(user.getChatid());
		if(kefu == null){
			return null;
		}
		
		//查出这个客服平台的常见问题
		List<Question> list = KefuUtil.getQuestionList(kefu.getId());
		for (int i = 0; i < list.size(); i++) {
			Question question = list.get(i);
			if(question.getTitle().equalsIgnoreCase(text)){
				//一样，那么匹配上，返回答案
				return question.getAnswer();
			}
		}
		
		//没有匹配上的，那么返回null
		return null;
	}
	

	/**
	 * 接收layui im 发送来的消息，进行转化
	 * @param text 发送来的消息。格式如： {"token":"06e39913-cc11-4f01-a9cf-2b6c4a7853d6","data":{"mine":{"username":"纸飞机","avatar":"http://cdn.firstlinkapp.com/upload/2016_6/1465575923433_33812.jpg","id":"100000","mine":true,"content":"2323"},"to":{"username":"Lemon_CC","id":"102101","avatar":"http://tp2.sinaimg.cn/1833062053/180/5643591594/0","sign":"","name":"Lemon_CC","type":"friend"}}}
	 */
//	public static MessageReceiveVO receive(String text){
//		MessageReceiveVO msg = new MessageReceiveVO();
//		JSONObject json = JSONObject.fromObject(text);
//		if(json.get("token") == null){
//			msg.setBaseVO(MessageReceiveVO.FAILURE, "token为空");
//			return msg;
//		}
//		msg.setToken(JSONUtil.getString(json, "token"));
//		
//		String type = JSONUtil.getString(json, "type");
//		msg.setType(type);
//		if(type.equals(MessageTypeEnum.CONNECT.name)){
//			//刚打开socket
//			
//		}else if(type.equals(MessageTypeEnum.AUTO_REPLY.name)){
//			//获取坐席所在的客服系统自动回复的信息
//			msg.setReceiveId(JSONUtil.getString(json, "receiveId"));
//		}else{
//			//正常的msg消息
//			if(json.get("data") == null){
//				//msg.setBaseVO(MessageReceiveVO.FAILURE, "内容data为空");
//				return msg;
//			}
//			JSONObject dataJson = json.getJSONObject("data");
//			JSONObject mineJson = dataJson.getJSONObject("mine");
//			JSONObject toJson = dataJson.getJSONObject("to");
//			
//			msg.setText(JSONUtil.getString(mineJson, "content"));
//			msg.setSendId(JSONUtil.getString(mineJson, "id"));
//			msg.setReceiveId(JSONUtil.getString(toJson, "id"));
//		}
//		
//		return msg;
//	}
	
	/**
	 * 将 {@link MessageReceiveVO} 转化为 {@link Message}
	 * @param vo {@link MessageReceiveVO}
	 * @return {@link Message}
	 */
	public static Message messageReceiveVOToMessage(MessageReceiveVO vo){
		if(vo == null){
			return new Message();
		}
		return vo;
	}
}
