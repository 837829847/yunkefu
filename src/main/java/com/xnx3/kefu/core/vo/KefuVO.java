package com.xnx3.kefu.core.vo;

import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.kefu.core.entity.Kefu;

/**
 * Kefu vo
 * @author 管雷鸣
 *
 */
public class KefuVO extends BaseVO{
	
	private Kefu kefu;
	private String zuoxiid;

	public Kefu getKefu() {
		return kefu;
	}

	public void setKefu(Kefu kefu) {
		this.kefu = kefu;
	}

	public String getZuoxiid() {
		return zuoxiid;
	}

	public void setZuoxiid(String zuoxiid) {
		this.zuoxiid = zuoxiid;
	}
	
	
}
