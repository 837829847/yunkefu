package com.xnx3.kefu.core.vo.bean;

import com.xnx3.DateUtil;
import com.xnx3.j2ee.vo.BaseVO;

import net.sf.json.JSONObject;

/**
 * 消息主体
 * @author 管雷鸣
 *
 */
public class Message extends BaseVO  implements java.io.Serializable{
	/***** 消息体内的参数 *****/
	public String type;			//当前消息类型。传入 MessageTypeEnum 参数。 如果不传默认是 MessageTypeEnum.MSG.name;
	
	public String text;			//消息内容
	String sendId;			//发送方id。如果是游客，则是32位uuid，如果小于32位，那么就是已登录用户的id，强制转化为int即可
	String receiveId;		//接收方id
	long time;				//消息产生时间，发送时间，13位时间戳
	JSONObject extend;	//扩展，如果type为 EXTEND，这里便是扩展的信息
	
	public Message() {
		this.time = DateUtil.timeForUnix13();
	}
	
	public String getText() {
		if(text == null){
			text = "";
		}
		return text;
	}
	public String getType() {
		if(type == null){
			type = "";
		}
		return type;
	}
	
	public String getSendId() {
		if(sendId == null){
			sendId = "";
		}
		return sendId;
	}
	public void setSendId(String sendId) {
		this.sendId = sendId;
	}
	public String getReceiveId() {
		if(receiveId == null){
			receiveId = "";
		}
		return receiveId;
	}
	public void setReceiveId(String receiveId) {
		this.receiveId = receiveId;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	
	public JSONObject getExtend() {
		if(this.extend == null){
			return new JSONObject();
		}
		return extend;
	}

	public void setExtend(JSONObject extend) {
		this.extend = extend;
	}

	@Override
	public String toString() {
		return "Message [type=" + type + ", text=" + text + ", sendId=" + sendId + ", receiveId=" + receiveId
				+ ", time=" + time + ", extend=" + extend + "]";
	}
	
	
}
