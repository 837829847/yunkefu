package com.xnx3.kefu.core.vo;

import java.util.List;
import com.xnx3.j2ee.util.Page;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.kefu.core.vo.bean.ZuoxiBean;

/**
 * 客服坐席列表
 * @author 管雷鸣
 *
 */
public class ZuoxiListVO extends BaseVO{
	private List<ZuoxiBean> list;
	private Page page;
	
	public List<ZuoxiBean> getList() {
		return list;
	}
	public void setList(List<ZuoxiBean> list) {
		this.list = list;
	}
	public Page getPage() {
		return page;
	}
	public void setPage(Page page) {
		this.page = page;
	}
	@Override
	public String toString() {
		return "KefuZuoxiListVO [list=" + list + ", page=" + page + "]";
	}
	
}
