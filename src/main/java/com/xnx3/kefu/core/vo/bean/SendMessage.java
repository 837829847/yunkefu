package com.xnx3.kefu.core.vo.bean;
/**
 * 向别人发送消息，消息体是 {@link Message}，相当于是对 {@link Message}进行格式化,筛选出有用的信息发送到对方socket客户端
 * @author 管雷鸣
 */
public class SendMessage {
	private String username;
	private String avatar;
	private String id;
	private String type;
	private String content;
	private String cid;
	private boolean mine;
	private String fromid;
	private long timestamp;
	
	public SendMessage() {
//		this.username = message.getSendUserName();
//		this.avatar = message.getSendAvatar();
//		this.id = message.getSendId();
//		this.type = message.getReceiveType();
//		this.content = message.getContent();
//		this.cid = message.getid();
//		this.fromid = message.getSendId();
//		this.timestamp = message.getTimestamp();
//		
		this.mine = false;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}



	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public boolean isMine() {
		return mine;
	}

	public void setMine(boolean mine) {
		this.mine = mine;
	}

	public String getFromid() {
		return fromid;
	}

	public void setFromid(String fromid) {
		this.fromid = fromid;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "SendMessage [username=" + username + ", avatar=" + avatar + ", id=" + id + ", type=" + type
				+ ", content=" + content + ", cid=" + cid + ", mine=" + mine + ", fromid=" + fromid + ", timestamp="
				+ timestamp + "]";
	}
	
}
