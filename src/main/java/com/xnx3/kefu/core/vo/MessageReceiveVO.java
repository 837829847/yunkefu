package com.xnx3.kefu.core.vo;

import com.xnx3.kefu.core.vo.bean.Message;

/**
 * 消息接收，将接收到的消息变为此对象
 * @author 管雷鸣
 *
 */
public class MessageReceiveVO extends Message{
	private String token;
	
	public MessageReceiveVO() {
		super();
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public String toString() {
		return "MessageReceiveVO [token=" + token + ", getText()=" + getText() + ", getType()=" + getType()
				+ ", getSendId()=" + getSendId() + ", getReceiveId()=" + getReceiveId() + ", getTime()=" + getTime()
				+ ", getExtend()=" + getExtend() + ", getResult()=" + getResult() + ", getInfo()=" + getInfo() + "]";
	}
	
}
