package com.xnx3.kefu.core.vo.bean;

import com.xnx3.kefu.core.bean.ChatUserBean;

/**
 * 客服后台，查看客服坐席列表使用
 * @author 管雷鸣
 *
 */
public class ZuoxiBean {
	private String nickname;	//坐席昵称
	private String head;		//坐席头像
	private ChatUserBean chatUserBean;	//当前坐席的聊天时的信息
	private boolean online;	//当前是否在线，true在线，默认是false，不在线
	
	public ChatUserBean getChatUserBean() {
		return chatUserBean;
	}
	public void setChatUserBean(ChatUserBean chatUserBean) {
		this.chatUserBean = chatUserBean;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	public boolean isOnline() {
		return online;
	}
	public void setOnline(boolean online) {
		this.online = online;
	}
}
