package com.xnx3.kefu.core.vo.bean;

/**
 * Message 消息的类型
 * @author 管雷鸣
 *
 */
public enum MessageTypeEnum {
	CONNECT("CONNECT"),	//socket刚打开时发送的消息，这个一般是是刚打开socket链接，进行登录，传入token用。客户端向服务器传递消息带的
	HEARTBEAT("HEARTBEAT"),	//心跳类型的消息，此种类型的消息只有 type 、 text 两种属性
	OPEN("OPEN"),		//用户打开一个对话框，准备跟某人聊天时
	AUTO_REPLY("AUTO_REPLY"),	//客服进行自动回复。客户端发起这种类型请求，则是在拉取对方是否有自动回复，如果有，服务端就会给客户端发送过自动回复的信息
	MSG("MSG"),			//正常收发消息沟通，文字、表情等沟通
	EXTEND("EXTEND"),	//扩展。比如发送商品、发送订单
	SYSTEM("SYSTEM"),	//系统提示，如提示 对方已离线
	SET_USER("SET_USER"),	//服务端发送到客户端，用于设置客户端的用户信息。会吧 com.xnx3.yunkefu.core.vo.bean.User 传过去
	CLOSE_SERVICE("CLOSE_SERVICE");	//结束服务，当用户咨询客服沟通完，咨询完时，可以点击一个结束服务的按钮，或者客服点击一个结束服务的按钮，触发，表明当前用户咨询结束。
	
	public final String name;	//类型的名字
	private MessageTypeEnum(String name) { 
		this.name = name;
	}
}
