package com.xnx3.kefu.core.vo;

import com.xnx3.j2ee.vo.BaseVO;

import net.sf.json.JSONArray;

/**
 * 消息列表，也就是历史消息记录，历史聊天记录
 * @author 管雷鸣
 *
 */
public class MessageListVO extends BaseVO{
	private long startTime;
	private long endTime;
	private int number;
	private JSONArray list;
	
	public long getStartTime() {
		return startTime;
	}
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	public long getEndTime() {
		return endTime;
	}
	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public JSONArray getList() {
		return list;
	}
	public void setList(JSONArray list) {
		this.list = list;
	}
	
	
}
