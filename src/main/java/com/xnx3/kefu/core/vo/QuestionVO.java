package com.xnx3.kefu.core.vo;

import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.kefu.core.entity.Question;

/**
 * 常见问题
 * @author 管雷鸣
 */
public class QuestionVO extends BaseVO{
	private Question question;

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	@Override
	public String toString() {
		return "QuestionVO [question=" + question + "]";
	}
	
}
