package com.xnx3.kefu.core.vo;

import com.xnx3.BaseVO;
import com.xnx3.kefu.core.entity.Kefu;
import com.xnx3.kefu.core.entity.KefuJS;

/**
 * kefuJS 的接口返回
 * @author 管雷鸣
 *
 */
public class KefuJSVO extends BaseVO{
	private KefuJS kefuJS;
	private Kefu kefu;

	public KefuJS getKefuJS() {
		return kefuJS;
	}

	public void setKefuJS(KefuJS kefuJS) {
		this.kefuJS = kefuJS;
	}

	public Kefu getKefu() {
		return kefu;
	}

	public void setKefu(Kefu kefu) {
		this.kefu = kefu;
	}
	
}
