package com.xnx3.kefu.core.vo;

import com.xnx3.j2ee.vo.BaseVO;

/**
 * 坐席
 * @author 管雷鸣
 *
 */
public class ZuoxiVO extends BaseVO{
	private String id;			//坐席唯一id，zuoxi.id
	private String nickname;	//坐席昵称
	private String head;		//坐席头型
	
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "ZuoxiVO [id=" + id + ", nickname=" + nickname + ", head=" + head + "]";
	}
	
}
