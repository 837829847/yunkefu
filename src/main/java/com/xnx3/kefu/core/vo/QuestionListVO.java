package com.xnx3.kefu.core.vo;

import java.util.List;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.kefu.core.entity.Question;

/**
 * 常见问题列表
 * @author 管雷鸣
 */
public class QuestionListVO extends BaseVO{
	private List<Question> list;	//问题列表

	public List<Question> getList() {
		return list;
	}

	public void setList(List<Question> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "QuestionListVO [list=" + list + "]";
	}
	
}
