package com.xnx3.kefu.core.bean;

import com.xnx3.j2ee.service.UserService;
import com.xnx3.j2ee.util.ConsoleUtil;
import com.xnx3.j2ee.util.SpringUtil;
import com.xnx3.kefu.core.service.KefuService;

/**
 * 用户信息,客服系统中用户信息记录都是依托于此，而非实体类
 * @author 管雷鸣
 */
public class ChatUserBean implements java.io.Serializable{
	/**
	 * 请使用 chatid
	 * @deprecated 
	 */
	private String id; //对应user.chatid
	private String nickname; //对应user.nickname
	private String head;
	private String type;	//类型。 youke:未登录游客、 user:已登录的用户
	private String chatid;	//user实体类的chatid
	private int userid;		//当前用户对应着的user.id ，如果当前用户是已登录用户的话。（游客肯定是没有的，游客的话这里就是0）

	private boolean busy;	//这个客服当前状态是否是占线，也就是正在解答用户问题服务用户，还是当前没有用户咨询在闲着。true:被占线，在忙着服务客户。默认是false
	private String otherId;	//当前正在跟对方聊天，对方的 chatid。如果这里为null，则当前用户并没有再跟别人聊天，也就是闲着的
	private long startChatTime;	//开始聊天服务的时间，开始聊天时间，如果 busy=true，那么这里肯定是有值的。否则为0。 13位时间戳
	
	private String ip;	//当前此用户的ip。v1.5 增加
	private String userAgent;	//当前此用户的浏览器 user-agent
	private String url;			//当前此用户的再这个客服页面的url，也就是当前所在网页的url	
	private String refererUrl;	//当前此用户，是从哪个页面跳转过来的，来源url
	
	public ChatUserBean() {
		this.busy = false;
		this.userid = 0;
		this.startChatTime = 0;
	}
	
	/**
	 * 直接传入 {@link com.xnx3.j2ee.entity.User} 实体类，来初始化此对象的参数
	 * @param user {@link com.xnx3.j2ee.entity.User}实体类
	 */
	public void setUser(com.xnx3.j2ee.entity.User user){
		if(user == null){
			return;
		}
		this.nickname = user.getNickname();
		
		UserService userService = (UserService) SpringUtil.getBean("userService");
		this.head = userService.getHead(user);
		this.type = "user";
		this.userid = user.getId();
		
		this.chatid = ((KefuService)SpringUtil.getBean("kefuService")).getZuoxiidByUserid(user.getId());
		this.id = this.chatid;
		
		this.busy = false;
		this.otherId = null;
	}
	
	/**
	 * @deprecated
	 * @return
	 */
	public String getId() {
		return getChatid();
	}
	/**
	 * @deprecated
	 * @param id
	 */
	public void setId(String id) {
		ConsoleUtil.log("setId()已经废弃!!. id:"+id); 
		this.id = id.replace("/", "youke").replaceAll(":", "_");
	}
	
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String getChatid() {
		return chatid;
	}

	public void setChatid(String chatid) {
		this.chatid = chatid;
		this.id = chatid;
	}

	public boolean isBusy() {
		return busy;
	}

	public void setBusy(boolean busy) {
		this.busy = busy;
	}
	
	public String getOtherId() {
		return otherId;
	}

	public void setOtherId(String otherId) {
		this.otherId = otherId;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public long getStartChatTime() {
		return startChatTime;
	}

	public void setStartChatTime(long startChatTime) {
		this.startChatTime = startChatTime;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getRefererUrl() {
		return refererUrl;
	}

	public void setRefererUrl(String refererUrl) {
		this.refererUrl = refererUrl;
	}

	@Override
	public String toString() {
		return "ChatUserBean [id=" + id + ", nickname=" + nickname + ", head=" + head + ", type=" + type + ", chatid="
				+ chatid + ", userid=" + userid + ", busy=" + busy + ", otherId=" + otherId + ", startChatTime="
				+ startChatTime + "]";
	}
	
}
