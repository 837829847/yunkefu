package com.xnx3.kefu.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * 坐席。一个客服下面会有多个坐席
 */
@Entity
@Table(name = "kefu_zuoxi", indexes={@Index(name="suoyin_index",columnList="kefuid,userid")})
public class Zuoxi implements java.io.Serializable {
	private String id;			//32位uuid
	private Integer kefuid;			//所属kefu，属于哪个客服平台下的坐席，对应kefu.id
	private Integer userid;		//当前客服坐席对应的User,user.id。是哪个用户所属这个坐席。
	
	@Id
	@Column(name = "id",columnDefinition="char(32)")
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	@Column(name = "kefuid", columnDefinition="int(11) comment '所属kefu，属于哪个客服平台下的坐席，对应kefu.id。按照设想，一个kefu平台下会有多个坐席' ")
	public Integer getKefuid() {
		return kefuid;
	}

	public void setKefuid(Integer kefuid) {
		this.kefuid = kefuid;
	}
	
	@Column(name = "userid", columnDefinition="int(11) comment '当前客服坐席对应的User,user.id。是哪个用户所属这个坐席。' ")
	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

}