package com.xnx3.kefu.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 客服。一个客服如果没有坐席，那么其默认便是一个坐席
 */
@Entity
@Table(name = "kefu_js")
public class KefuJS implements java.io.Serializable {
	private Integer id;			//对应kefu.id
	private String color;		//文字、图片的颜色，十六进制，如： #FFFFFF
	private String backgroundColor;	//背景色的颜色，十六进制，如： #FFFFFF
	
	public KefuJS() {
		this.color = "#000000";
		this.backgroundColor = "#FFFFFF";
	}
	
	@Id
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "color", columnDefinition="char(15) comment '文字、图片的颜色' ")
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	@Column(name = "background_color", columnDefinition="char(15) comment '背景色的颜色，十六进制，如： #FFFFFF' ")
	public String getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(String backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	@Override
	public String toString() {
		return "KefuJS [id=" + id + ", color=" + color + ", backgroundColor=" + backgroundColor + "]";
	}

}