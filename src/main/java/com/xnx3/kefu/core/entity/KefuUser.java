package com.xnx3.kefu.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * kefu - user 关联表
 * 暂未使用到，直接使用了 kefu.userid
 */
@Deprecated
@Entity
@Table(name = "kefu_user", indexes={@Index(name="suoyin_index",columnList="kefuid")})
public class KefuUser implements java.io.Serializable {
	private Integer id;			//user.id ，对应user.id
	private Integer kefuid;		//对应kefu.id
	
	@Id
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "kefuid", columnDefinition="int(11) comment '对应kefu.id' ")
	public Integer getKefuid() {
		return kefuid;
	}

	public void setKefuid(Integer kefuid) {
		this.kefuid = kefuid;
	}

	@Override
	public String toString() {
		return "KefuUser [id=" + id + ", kefuid=" + kefuid + "]";
	}
	
}