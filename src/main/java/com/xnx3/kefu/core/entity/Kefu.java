package com.xnx3.kefu.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * 客服。一个客服如果没有坐席，那么其默认便是一个坐席
 */
@Entity
@Table(name = "kefu", indexes={@Index(name="suoyin_index",columnList="userid,chatid")})
public class Kefu implements java.io.Serializable {
	
	/**
	 * 冻结状态，1 已冻结
	 */
	public static final Short FREEZE_YES = 1;
	/**
	 * 冻结状态，0 未冻结，正常使用
	 */
	public static final Short FREEZE_NO = 0;
	
	private Integer id;		//自动编号
	private Integer userid;		//所属用户，属于哪个用户，对应 User.id
	private String autoReply; 		//客户打开跟此人聊天框时，自动发送给对方的文字。若为空，或者空字符串，则不开启此功能。限制255个字符
	private Short useOffLineEmail;	//是否使用离线时的邮件通知提醒，默认为0，不启用。  1为启用
	private String email;			//接收提醒的邮箱地址，若上面开启邮件提醒通知了，那么这里是必须要填写的，不然是要通知谁
	private Short useKefu;			//是否开启在线客服功能。0不开启，默认；  1开启。
	private Short useOffLineSms;	//是否使用离线时的短信通知提醒，默认为0，不启用。  1为启用
	private String phone;			//手机号，若上面开启短信提醒通知了，那么这里是必须要填写的，不然是要通知谁
	private String offlineAutoRaply;	//无客服在线时，自动回复的文字。若为空，或者空字符串，则不开启此功能。限制255个字符
	private String chatid;		//用于发起客服聊天的id。会根据这个id找到是哪个客服平台，然后自动分配客服平台下的一个坐席对接到客户进行服务。
	
	private Integer autoHelloTime;		//访客打开网站后（也就是执行kefu.init() 进行初始化开始算），几秒后主动跟客户打招呼让客户看到你发送的指定内容。这里是等待的时间。如果是-1(<0)则是不启用
	private String autoHelloText;	//访客打开网站后（也就是执行kefu.init() 进行初始化开始算），几秒后主动跟客户打招呼让客户看到你发送的指定内容。这里是发送的内容
	
	private Short freeze; 	//当前客服平台的冻结状态，1已冻结， 0未冻结
	
	public Kefu() {
		this.autoHelloTime = -1;
		this.autoHelloText = "";
		
		this.offlineAutoRaply = "我当前不在线，有什么问题您可以给我留言";
		this.autoReply = "您好，请问有什么可以帮您？";
		this.freeze = FREEZE_NO;
	}
	
	/**
	 * 是否启用，true：1  启用
	 */
	public static final Short USE_TRUE = 1;
	/**
	 * 是否启用，false：0  不启用
	 */
	public static final Short USE_FALSE = 0;
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "auto_reply", columnDefinition="char(255) comment '客户打开跟此人聊天框时，自动发送给对方的文字。若为空，或者空字符串，则不开启此功能。限制255个字符' ")
	public String getAutoReply() {
		return autoReply;
	}

	public void setAutoReply(String autoReply) {
		this.autoReply = autoReply;
	}
	
	@Column(name = "use_offline_email", columnDefinition="tinyint(2) comment '是否使用离线时的邮件通知提醒，默认为0，不启用。  1为启用' ")
	public Short getUseOffLineEmail() {
		return useOffLineEmail;
	}
	
	public void setUseOffLineEmail(Short useOffLineEmail) {
		this.useOffLineEmail = useOffLineEmail;
	}
	
	@Column(name = "email", columnDefinition="char(30) comment '接收提醒的邮箱地址，若上面开启邮件提醒通知了，那么这里是必须要填写的，不然是要通知谁' ")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name = "userid", columnDefinition="int(11) comment '所属用户，属于哪个用户，对应 User.id' ")
	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	
	@Column(name = "use_kefu", columnDefinition="tinyint(2) comment '是否开启在线客服功能。0不开启，默认；  1开启。' ")
	public Short getUseKefu() {
		return useKefu;
	}

	public void setUseKefu(Short useKefu) {
		this.useKefu = useKefu;
	}
	
	@Column(name = "use_offline_sms", columnDefinition="tinyint(2) comment '是否使用离线时的短信通知提醒，默认为0，不启用。  1为启用' ")
	public Short getUseOffLineSms() {
		return useOffLineSms;
	}

	public void setUseOffLineSms(Short useOffLineSms) {
		this.useOffLineSms = useOffLineSms;
	}
	
	@Column(name = "phone", columnDefinition="char(18) comment '手机号，若上面开启短信提醒通知了，那么这里是必须要填写的，不然是要通知谁' ")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@Column(name = "offline_auto_raply", columnDefinition="char(255) comment '无客服在线时，自动回复的文字。若为空，或者空字符串，则不开启此功能。限制255个字符' ")
	public String getOfflineAutoRaply() {
		return offlineAutoRaply;
	}

	public void setOfflineAutoRaply(String offlineAutoRaply) {
		this.offlineAutoRaply = offlineAutoRaply;
	}
	
	@Column(name = "chatid", columnDefinition="char(32) comment '用于发起客服聊天的id。会根据这个id找到是哪个客服平台，然后自动分配客服平台下的一个坐席对接到客户进行服务。' ")
	public String getChatid() {
		return chatid;
	}

	public void setChatid(String chatid) {
		this.chatid = chatid;
	}

	@Column(name = "auto_hello_time", columnDefinition="int(11)")
	public Integer getAutoHelloTime() {
		if(this.autoHelloTime == null) {
			this.autoHelloTime = -1;
		}
		return autoHelloTime;
	}
	
	
	public void setAutoHelloTime(Integer autoHelloTime) {
		this.autoHelloTime = autoHelloTime;
	}

	@Column(name = "auto_hello_text", columnDefinition="char(200)")
	public String getAutoHelloText() {
		if(this.autoHelloText == null) {
			this.autoHelloText = "";
		}
		return autoHelloText;
	}

	public void setAutoHelloText(String autoHelloText) {
		if(autoHelloText != null && autoHelloText.length() > 200) {
			autoHelloText = autoHelloText.substring(0, 199);
		}
		this.autoHelloText = autoHelloText;
	}

	@Column(name = "freeze", columnDefinition="tinyint(2) comment '当前客服平台的冻结状态，1已冻结， 0未冻结' ")
	public Short getFreeze() {
		if(this.freeze == null) {
			this.freeze = FREEZE_NO;
		}
		return freeze;
	}

	public void setFreeze(Short freeze) {
		this.freeze = freeze;
	}

	@Override
	public String toString() {
		return "Im [id=" + id + ", userid=" + userid + ", autoReply=" + autoReply + ", useOffLineEmail="
				+ useOffLineEmail + ", email=" + email + ", useKefu=" + useKefu + ", useOffLineSms=" + useOffLineSms
				+ ", phone=" + phone + "]";
	}

}