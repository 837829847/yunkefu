package com.xnx3.kefu.core.entity;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * 常见问题
 * @author 管雷鸣
 *
 */
@Entity
@Table(name = "kefu_question", indexes={@Index(name="suoyin_index",columnList="kefuid,updatetime,title")})
public class Question implements java.io.Serializable{
	private Integer id;			//自动编号
	private Integer kefuid;		//该条常见问题所属哪个客服平台
	private String title;		//问题标题，问
	private String answer;		//问题的答案，答
	private Integer updatetime;	//最后修改时间
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "kefuid", columnDefinition="int(11) comment '该条常见问题所属哪个客服平台' ")
	public Integer getKefuid() {
		return kefuid;
	}
	public void setKefuid(Integer kefuid) {
		this.kefuid = kefuid;
	}
	
	@Column(name = "title", columnDefinition="char(50) comment '问题标题，问' ")
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	@Column(name = "answer", columnDefinition="varchar(1000) comment '问题的答案，答' ")
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	@Column(name = "updatetime", columnDefinition="int(11) comment '最后修改时间' ")
	public Integer getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(Integer updatetime) {
		this.updatetime = updatetime;
	}
	@Override
	public String toString() {
		return "Question [id=" + id + ", kefuid=" + kefuid + ", title=" + title + ", answer=" + answer + ", updatetime="
				+ updatetime + "]";
	}
	
}
