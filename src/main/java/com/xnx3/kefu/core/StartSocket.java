package com.xnx3.kefu.core;

import org.apache.shiro.util.ThreadContext;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.stereotype.Component;

import com.xnx3.Lang;
import com.xnx3.j2ee.util.ConsoleUtil;
import com.xnx3.kefu.core.socket.NettyServer;
import com.xnx3.kefu.core.util.KefuPropertiesUtil;

/**
 * 启动socket项目
 * @author 管雷鸣
 *
 */
@Component
public class StartSocket {
	public StartSocket() {
		new Thread(new Runnable() {
			public void run() {
				//将此现成加入shiro,不然会出现  org.apache.shiro.util.ThreadContext or as a vm static singleton
				DefaultWebSecurityManager manager = new DefaultWebSecurityManager();
				ThreadContext.bind(manager);
				
				String portStr = KefuPropertiesUtil.getProperty("kefu.socket.port");
				NettyServer server = new NettyServer(Lang.stringToInt(portStr, 443));
		    	try {
					server.start();
					ConsoleUtil.info("websocket thread start ... ");
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}
	
	public static void main(String[] args) {
		new StartSocket();
	}
}
