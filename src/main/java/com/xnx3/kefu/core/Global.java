package com.xnx3.kefu.core;

import com.xnx3.j2ee.util.ConsoleUtil;
import com.xnx3.kefu.core.util.KefuPropertiesUtil;

//import com.xnx3.j2ee.util.ApplicationPropertiesUtil;
//import com.xnx3.j2ee.util.ConsoleUtil;

/**
 * 全局参数
 * @author 管雷鸣
 *
 */
public class Global {
	/**
	 * 当前的版本号。
	 */
	public static final String VERSION = "2.0";
	
	public static final int ROLE_ID_ADMIN = 1;	//客服后台的权限
	public static final int ROLE_ID_SUPERADMIN = 9;	//总管理后台的权限
	public static final int ROLE_ID_ZUOXI = 10;	//坐席后台的权限
	
	public static boolean imMustLogin = true;	//必须登录后才能使用发起沟通对话
	public static String youkeHead = "https://res.zvo.cn/kefu/images/head.png";		//游客的head头像
	static{
		imMustLogin = KefuPropertiesUtil.getProperty("kefu.imMustLogin").equals("true");
		ConsoleUtil.info("kefu.imMustLogin : "+imMustLogin);
	}
	
	
}
