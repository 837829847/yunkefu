package com.xnx3.kefu.core.pluginManage.interfaces;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import com.xnx3.j2ee.vo.BaseVO;

/**
 * kefu.js 相关，比如追加插件了
 * @author 管雷鸣
 */
public interface KefuJsInterface {
	
	/**
	 * 向kefu.js中追加 js 资源文件。会在kefu.init() 之前被追加。
	 * @return 被追加的js资源文件的url，建议是绝对路径。其中可以用 {kefu.api} 调取 kefu.properties 中配置的信息。如设置为  {kefu.api}/plugin/abcd/plugin.js
	 */
	public List<String> kefuJsAppendJsFile(HttpServletRequest request);
	
	/**
	 * 向kefu.js中追加js命令。 会在 {@link #kefuJsAppendJsFile(HttpServletRequest)} 都（包含别的插件的）执行完后，再执行这里的
	 * @param channel 当前socket通道
	 * @param msg 客户端发送过来的消息对象
	 * @return result 为 success，则info被视为要执行的js进行追加。如果 result为 failure，那么info不管他，忽略。
	 */
	public BaseVO kefuJsAppendJsCode(HttpServletRequest request);
	
}