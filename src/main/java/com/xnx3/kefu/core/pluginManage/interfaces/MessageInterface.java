package com.xnx3.kefu.core.pluginManage.interfaces;

import com.xnx3.kefu.core.bean.ChatUserBean;
import com.xnx3.kefu.core.vo.MessageReceiveVO;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;

/**
 * 消息相关，收发消息等
 * @author 管雷鸣
 */
public interface MessageInterface {
	
	/**
	 * 离线消息
	 * <p>A用户发送给B用户消息，B用户已离线了，触发此接口方法。</p>
	 * @param channel A用户当前的socket通道
	 * @param message A用户发送过来的消息
	 */
	public void offlineMessage(Channel channel, MessageReceiveVO message);
	
	/**
	 * 用户上线，socket建立连接触发。
	 * @param channel 上线用户当前的socket通道
	 */
	public void online(Channel channel);
	
	/**
	 * 当socket通道打开，获取回复语执行之前触发
	 * @param channel 上线用户当前的socket通道
	 * @param otherChatId 跟当前socket用户的对方的chatid，也就是当前要拉取的，自动回复的那个人的chatid，别人的chatid
	 */
	public void autoReply(Channel channel, String otherChatId);
	
	/**
	 * 用户下线，socket断开连接触发。  是先执行此方法，再执行断开连接的其他如接触绑定、销毁等。
	 * @param channel 下线用户当前的socket通道
	 */
	public void offline(Channel channel);

	/**
	 * 当客户端发送正常消息进行交流时触发。这个已经抛除了打开链接、以及常见问题的自动匹配，触发这个的都是用户主动发送的消息
	 * @param channel 当前socket通道
	 * @param msg 客户端发送过来的消息对象
	 * @return 返回true，那么继续向后执行，如果返回false，就不再继续往下执行了 （nettyHandler中注解的后面的代码执行）（注意，这个强烈建议不要返回false，除非是用于指定的extend客服插件定制）
	 */
	public boolean messageReceivedChat(Channel channel, MessageReceiveVO msg);
	
	
}