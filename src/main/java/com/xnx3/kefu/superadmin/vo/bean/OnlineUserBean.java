package com.xnx3.kefu.superadmin.vo.bean;

import java.util.List;

import com.xnx3.kefu.core.bean.ChatUserBean;

/**
 * 在线用户bean
 * @author 管雷鸣
 */
public class OnlineUserBean {
	private ChatUserBean user;	//当前在线用户的信息
	private List<String> channelIdList;	//当前用户使用了哪些通道，这里存储的是通道的id。如果用户登录了多个地方，那么就会产生多个通道
	
	public ChatUserBean getUser() {
		return user;
	}
	public void setUser(ChatUserBean user) {
		this.user = user;
	}
	public List<String> getChannelIdList() {
		return channelIdList;
	}
	public void setChannelIdList(List<String> channelIdList) {
		this.channelIdList = channelIdList;
	}
	
	@Override
	public String toString() {
		return "OnlineUserBean [ChatUserBean=" + user + ", channelIdList=" + channelIdList + "]";
	}
	
}
