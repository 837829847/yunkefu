package com.xnx3.kefu.superadmin.vo;

import java.util.List;
import com.xnx3.BaseVO;
import com.xnx3.kefu.superadmin.vo.bean.OnlineUserBean;

/**
 * 在线用户列表
 * @author 管雷鸣
 *
 */
public class OnlineUserVO extends BaseVO{
	private int onlineUserNumber;		//当前在线的所有用户数（有可能一个用户登录多个）
	private int currentChannelNumber;	//当前活跃的socket通道数量
	private List<OnlineUserBean> onlineUserList;	//在线用户列表
	
	public int getOnlineUserNumber() {
		return onlineUserNumber;
	}
	public void setOnlineUserNumber(int onlineUserNumber) {
		this.onlineUserNumber = onlineUserNumber;
	}
	public int getCurrentChannelNumber() {
		return currentChannelNumber;
	}
	public void setCurrentChannelNumber(int currentChannelNumber) {
		this.currentChannelNumber = currentChannelNumber;
	}
	public List<OnlineUserBean> getOnlineUserList() {
		return onlineUserList;
	}
	public void setOnlineUserList(List<OnlineUserBean> onlineUserList) {
		this.onlineUserList = onlineUserList;
	}
	
}
