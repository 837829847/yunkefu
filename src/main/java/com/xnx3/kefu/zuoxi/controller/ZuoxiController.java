package com.xnx3.kefu.zuoxi.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import com.xnx3.j2ee.util.ActionLogUtil;
import com.xnx3.j2ee.util.SafetyUtil;
import com.xnx3.j2ee.util.SessionUtil;
import com.xnx3.j2ee.controller.BaseController;
import com.xnx3.j2ee.entity.User;
import com.xnx3.j2ee.service.SqlCacheService;
import com.xnx3.j2ee.service.SqlService;
import com.xnx3.j2ee.service.UserService;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.j2ee.vo.UploadFileVO;
import com.xnx3.kefu.core.bean.ChatUserBean;
import com.xnx3.kefu.core.entity.Zuoxi;
import com.xnx3.kefu.core.service.KefuService;
import com.xnx3.kefu.core.util.TokenUtil;
import com.xnx3.kefu.core.util.UserUtil;
import com.xnx3.kefu.core.vo.ZuoxiVO;

/**
 * 坐席设置
 * @author 管雷鸣
 * @deprecated 转移到了 plugin_zuoxi
 */
@Controller(value="KefuZuoxiAdminController")
@RequestMapping("/kefu/zuoxi/")
public class ZuoxiController extends BaseController {
	@Autowired
	private SqlCacheService sqlCacheService;
	@Autowired
	private UserService userService;
	@Autowired
	private SqlService sqlService;
	@Autowired
	private KefuService kefuService;
	

	/**
	 * 获取坐席信息，获取当前登录用户的坐席信息 
	 */
	@ResponseBody
	@RequestMapping("getZuoxi${api.suffix}")
	public ZuoxiVO getZuoxiByUserid(HttpServletRequest request){
		ZuoxiVO vo = new ZuoxiVO();
		User currentUser = getUser();
		
		Zuoxi zuoxi = sqlCacheService.findAloneByProperty(Zuoxi.class, "userid", currentUser.getId());
		if(zuoxi == null){
			vo.setBaseVO(BaseVO.FAILURE, "坐席不存在");
			return vo;
		}
		
		User user = sqlCacheService.findById(User.class, currentUser.getId());
		if(user == null){
			vo.setBaseVO(BaseVO.FAILURE, "用户不存在");
			return vo;
		}
		
		vo.setHead(userService.getHead(user));
		vo.setId(zuoxi.getId());
		vo.setNickname(user.getNickname());
		
		ActionLogUtil.insert(request,user.getId(), "获取坐席信息", vo.toString());
		return vo;
	}
	

	/**
	 * 坐席的名字修改，改动当前登录坐席的名字
	 * @param request 需post/get传入nickname 客服的名字，即user.nickname，不可为空。
	 */
	@RequestMapping("updateNickname${api.suffix}")
	@ResponseBody
	public BaseVO updateNickname(HttpServletRequest request){
		User user = getUser();
		
		//通过userid，获取到这个用户的坐席信息
		Zuoxi zuoxi = sqlCacheService.findAloneByProperty(Zuoxi.class, "userid", user.getId());
		BaseVO vo = userService.updateNickname(request);
		if(vo.getResult() - BaseVO.SUCCESS == 0){
			//成功
			//清理缓存
			sqlCacheService.deleteCacheById(User.class, user.getId());
			//日志
			ActionLogUtil.insert(request, zuoxi.getUserid(), "坐席名字修改", SafetyUtil.filter(getUser().getNickname()));
		}
		return vo;
	}
	

	/**
	 * 更改当前登录坐席的头像
	 * @param head 上传的头像文件
	 */
	@RequestMapping("updateHead${api.suffix}")
	@ResponseBody
	public UploadFileVO headSave(HttpServletRequest request, @RequestParam("head") MultipartFile head){
		UploadFileVO vo = userService.updateHead(head);
		User user = getUser();
		if(vo.getResult() - UploadFileVO.SUCCESS == 0){
			//成功
			//清理缓存
			sqlCacheService.deleteCacheById(User.class, user.getId());
			//日志
			ActionLogUtil.insert(request, user.getId(), "更新坐席的头像", vo.getUrl());
		}
		return vo;
	}
	
	/**
	 * 在线坐席，在线聊天窗口
	 * @return
	 */
	@RequestMapping("onlineChat${url.suffix}")
	public String onlineChat(HttpServletRequest request, Model model){
		String token = request.getSession().getId();
		
		//将当天用户的token加入到 TokenUtil

		//将当前sessionid进行缓存，以便可以根据sessionid直接取User信息
		com.xnx3.kefu.core.util.SessionUtil.setActiveUserForNotRedis(token, SessionUtil.getActiveUser());
		//TokenUtil绑定
		if(TokenUtil.getChatid(token) == null){
			ChatUserBean chatUserBean = new ChatUserBean();
			chatUserBean.setUser(getUser());
			TokenUtil.bind(token, chatUserBean);
		}
		
		
		ActionLogUtil.insert(request, "进入在线坐席");	//日志
		return "kefu/zuoxi/onlineChat";
	}
	
	
}
