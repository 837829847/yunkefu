package com.xnx3.kefu.chat.controller;

import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.j2ee.controller.BaseController;
import com.xnx3.j2ee.service.SqlCacheService;
import com.xnx3.j2ee.service.SqlService;
import com.xnx3.j2ee.service.UserService;
import com.xnx3.kefu.core.bean.ChatUserBean;
import com.xnx3.kefu.core.entity.Kefu;
import com.xnx3.kefu.core.entity.Zuoxi;
import com.xnx3.kefu.core.service.KefuService;
import com.xnx3.kefu.core.util.SessionUtil;
import com.xnx3.kefu.core.util.SocketUtil;
import com.xnx3.kefu.core.util.UserUtil;
import com.xnx3.kefu.chat.vo.UserVO;
import io.netty.channel.Channel;

/**
 * 坐席相关
 * @author 管雷鸣
 */
@Controller(value="kefuChatZuoxiController")
@RequestMapping("/kefu/chat/zuoxi/")
public class ZuoxiController extends BaseController {
	@Resource
	private UserService userService;
	@Resource
	private SqlService sqlService;
	@Resource
	private SqlCacheService sqlCacheService;
	@Resource
	private KefuService kefuService;
	
	/**
	 * 通过坐席id获取 {@link User} 信息
	 * @param id zuoxi.id
	 * @return 如果取不到，那么new一个新的 {@link User} 返回
	 */
	@RequestMapping(value="getUserByZuoxiId.json", method = RequestMethod.POST)
	@ResponseBody
	public UserVO getUserByZuoxiId(HttpServletRequest request,
			@RequestParam(value = "id", required = false , defaultValue="") String id){
		UserVO vo = new UserVO();
		if(id.length() < 1){
			vo.setBaseVO(UserVO.FAILURE, "请传入id");
			return vo;
		}
		
		//潍坊雷鸣云网络的demo，将它转成现有的坐席id
		if(id.equals("464")){
			id = "365fef747a9e493fb631b621ee36eed1";
		}
		
		
		//UserUtil中没有，那么直接从坐席中找，当然坐席id是要符合32位字符串的
		if(id.length() == 32){
			//是坐席人员
			
			Zuoxi zuoxi = sqlCacheService.findById(Zuoxi.class, id);
			if(zuoxi == null){
				vo.setBaseVO(BaseVO.FAILURE, "坐席不存在");
				return vo;
			}
			com.xnx3.j2ee.entity.User user = sqlCacheService.findById(com.xnx3.j2ee.entity.User.class, zuoxi.getUserid());
			if(user == null){
				vo.setBaseVO(BaseVO.FAILURE, "用户不存在");
				return vo;
			}
			ChatUserBean userbean = new ChatUserBean();
			userbean.setUser(user);
			vo.setUser(userbean);
		}else{
			//其他情况，比如这里获取的是游客的信息。
			//先判断一下 UserUtil 中有么有
			ChatUserBean chatUserBean = UserUtil.getUser(id);
			if(chatUserBean != null){
				vo.setUser(chatUserBean);
			}else{
				vo.setBaseVO(BaseVO.FAILURE, "未知异常，对方信息未发现");
			}
		}
		
		//有用户，那么判断一下用户当前是否在线
		if(vo.getUser() != null){
			List<Channel> channelList = SocketUtil.getChannel(vo.getUser().getChatid());
			vo.setOnlineState(channelList.size() == 0? "离线":"在线");
		}else{
			vo.setOnlineState("未知");
		}
		
		//判断当前人员是坐席或客服吗,如果不是，那么不会获取到浏览器信息
		if(!SessionUtil.isKefuZuoxi()) {
			vo.setUser(UserUtil.filterBrowserInfo(vo.getUser()));
		}
		
		return vo;
	}
	

	/**
	 * 通过kefu.chatid获取 一个可接通的闲时的坐席{@link User} 的信息 
	 * 获取这个的，那一定是访客。
	 * @param id kefu.chatid
	 * @return 如果取不到，那么new一个新的 {@link User} 返回
	 */
	@RequestMapping(value="getUserByKefuChatId.json", method = RequestMethod.POST)
	@ResponseBody
	public UserVO getUserByKefuChatId(HttpServletRequest request,
			@RequestParam(value = "id", required = false , defaultValue="") String id){
		UserVO vo = new UserVO();
//		kefuchatid_7fce7dfd06a8464fbd5d80abfc103565
		id = id.replace("kefuchatid_", "");
		if(id.length() != 32){
			vo.setBaseVO(UserVO.FAILURE, "请传如正确的chatid");
			return vo;
		}
		
		//找到当前kefu.chatid 对应的kefu
		Kefu kefu = sqlCacheService.findAloneByProperty(Kefu.class, "chatid", id);
		if(kefu == null){
			vo.setBaseVO(UserVO.FAILURE, "客服不存在");
			return vo;
		}
		
		//找到这个客服下所有的坐席
		List<Zuoxi> zuoxiList = sqlService.findByProperty(Zuoxi.class, "kefuid", kefu.getId());
		//用于标注当前是否都是离线的.这里记录离线的人的数量
		int lixianZuoxiNumber = 0;
		//找到哪个坐席当前闲置，分配一个闲置的坐席
		for (int i = 0; i < zuoxiList.size(); i++) {
			Zuoxi zuoxi = zuoxiList.get(i);
			
			//先判断这个客服是否在线
			if(SocketUtil.getChannel(zuoxi.getId()).size() == 0){
				//这个坐席不在线，那么忽略这个坐席
				lixianZuoxiNumber++;
				continue;
			}
			
			//判断这个坐席当前的空闲状态
			ChatUserBean chatUserBean = UserUtil.getUser(zuoxi.getId());
			if(!chatUserBean.isBusy()){
				//空闲的，那么将他接入给用户
				vo.setUser(UserUtil.filterBrowserInfo(chatUserBean));
				return vo;
			}
		}
		if(lixianZuoxiNumber - zuoxiList.size() == 0) {
			//如果全部坐席人员都离线，那就返回客服平台的这个默认坐席，也就是登录客服平台的用户
			Zuoxi zuoxi = sqlService.findAloneByProperty(Zuoxi.class,"userid", kefu.getUserid());
			vo.setUser(UserUtil.filterBrowserInfo(UserUtil.getUser(zuoxi.getId())));
			vo.setOnlineState("离线");
			return vo;
		}
		
		//执行到这里了，那说明没有分配到合适的坐席(在线，且不是忙碌状态)进行服务
		vo.setBaseVO(BaseVO.FAILURE, "当前无在线且空闲的坐席客服，请稍后再试");
		return vo;
	}
	
}
