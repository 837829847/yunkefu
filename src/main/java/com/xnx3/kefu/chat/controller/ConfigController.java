package com.xnx3.kefu.chat.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xnx3.BaseVO;
import com.xnx3.j2ee.controller.BaseController;
import com.xnx3.j2ee.service.SqlService;
import com.xnx3.kefu.core.pluginManage.interfaces.manage.KefuJsPluginManage;
import com.xnx3.kefu.core.util.KefuPropertiesUtil;
import com.xnx3.kefu.chat.vo.ConfigVO;

/**
 * 配置相关
 * @author 管雷鸣
 */
@Controller(value="kefuChatConfigController")
@RequestMapping("/kefu/chat/config/")
public class ConfigController extends BaseController {
	@Resource
	private SqlService sqlService;

	/**
	 * 获取网络配置，比如websocket、api接口地址
	 * <p>这里获取的是kefu.properties 中配置的参数</p>
	 * @author 管雷鸣
	 * @deprecated 使用 {@link #getConfig(HttpServletRequest)}
	 */
	@RequestMapping(value="getNetConfig.json", method = RequestMethod.POST)
	@ResponseBody
	public ConfigVO getNetConfig(HttpServletRequest request){
		ConfigVO vo = new ConfigVO();
		
		String api = KefuPropertiesUtil.getProperty("kefu.api");
		if(api == null) {
			vo.setBaseVO(BaseVO.FAILURE, "请先设置 kefu.properties 中的 kefu.api 、 kefu.socket");
			return vo;
		}
		//容错，判断api是否末尾多加了/
		if(api.lastIndexOf("/") + 1 == api.length()) {
			//域名最后是以/结尾，那去掉最后的 /
			api = api.substring(0, api.length() -1);
		}
		vo.setApi(api);
		
		vo.setSocket(KefuPropertiesUtil.getProperty("kefu.socket"));
		
		return vo;
	}
	

	/**
	 * 获取配置，比如websocket、api接口地址、kefu.js的扩展插件等
	 * <p>这里获取的是kefu.properties 中配置的参数</p>
	 * @author 管雷鸣
	 */
	@RequestMapping(value="getConfig.json", method = RequestMethod.POST)
	@ResponseBody
	public ConfigVO getConfig(HttpServletRequest request){
		ConfigVO vo = new ConfigVO();
		
		String api = KefuPropertiesUtil.getKefuApi();
		if(api == null) {
			vo.setBaseVO(BaseVO.FAILURE, "请先设置 kefu.properties 中的 kefu.api 、 kefu.socket");
			return vo;
		}
		vo.setApi(api);
		
		vo.setSocket(KefuPropertiesUtil.getProperty("kefu.socket"));
		
		/**** 针对html源码处理插件 ****/
		List<String> pluginAppendJSFileList = KefuJsPluginManage.kefuJsAppendJsFile(request);
		vo.setJsFile(pluginAppendJSFileList);
		String pluginAppendJSCode = KefuJsPluginManage.kefuJsAppendJsCode(request);
		vo.setJsCode(pluginAppendJSCode);
		
		
		return vo;
	}
	
}
