package com.xnx3.kefu.chat.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xnx3.j2ee.controller.BaseController;
import com.xnx3.j2ee.util.ActionLogUtil;
import com.xnx3.kefu.core.util.MessageStorageUtil;
import com.xnx3.kefu.core.util.TokenUtil;
import com.xnx3.kefu.core.vo.MessageListVO;

/**
 * 聊天记录
 * @author 管雷鸣
 *
 */
@Controller(value="KefuChatLogController")
@RequestMapping("/kefu/chat/log")
public class LogController extends BaseController {
	
	/**
	 * 我跟某人历史聊天记录
	 * @param token 当前操作用户的唯一标识 <required>
	 * 				<p>可通过 调用kefu.js中的函数 kefu.token.get();来获得 </p>
	 * @param otherId 跟我聊天对方的chatid
	 * @param time 13位时间戳，要获取这个时间之前的记录
	 * @param number 要获取多少条记录，取值10~200之间 <br/> 不传默认是100条
	 * @param type 类型，取数据的类型，可传入:
	 * 					<ul>
	 * 						<li>before:向前，也就是取<time的记录</li>
	 * 						<li>after:向后,也就是取>time的记录</li>
	 * 					</ul>
	 * 				<p>默认不传则是before</p>
	 * @return {@link com.xnx3.j2ee.vo.BaseVO} info: 信息列表
	 * @throws LogException 
	 * @author 管雷鸣
	 */
	@ResponseBody
	@RequestMapping(value = "/log.json",method = {RequestMethod.POST})
	public MessageListVO chatLog(HttpServletRequest request,
			@RequestParam(value = "token", required = true) String token,
			@RequestParam(value = "otherId", required = true) String otherId,
			@RequestParam(value = "time", required = true) long time,
			@RequestParam(value = "number", required = false, defaultValue = "100") int number,
			@RequestParam(value = "type", required = false, defaultValue = "before") String type
		){
		MessageListVO vo = new MessageListVO();
		
		//根据token获取当前用户的userid
//		ChatUserBean chatUser = TokenUtil.getUser(token);
		String chatid = TokenUtil.getChatid(token);
		if(chatid == null){
			vo.setBaseVO(MessageListVO.FAILURE, "没有发现你这个用户");
			return vo;
		}
		
		vo = MessageStorageUtil.chatLog(chatid, otherId, time, number, type, request);
		
		ActionLogUtil.insert(request, "查看聊天历史日志列表", request.getQueryString());
		return vo;
	}
	
	
}
