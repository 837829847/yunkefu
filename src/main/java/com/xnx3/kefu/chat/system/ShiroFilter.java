package com.xnx3.kefu.chat.system;

import java.util.Map;
import com.xnx3.j2ee.pluginManage.interfaces.ShiroFilterInterface;

/**
 * Shiro方面拦截，放开所有拦截，有springmvc拦截器控制
 * @author 管雷鸣
 *
 */
public class ShiroFilter implements ShiroFilterInterface{
	@Override
	public Map<String, String> shiroFilter(Map<String, String> filterChainDefinitionMap) {
		filterChainDefinitionMap.put("/kefu/chat/**", "anon");
		filterChainDefinitionMap.put("/kefu/chat/zuoxi/**", "anon");
		filterChainDefinitionMap.put("/kefu/chat/user/**", "anon");
		filterChainDefinitionMap.put("/kefu/chat/log/**", "anon");
		filterChainDefinitionMap.put("/kefu/chat/file/**", "anon");
		filterChainDefinitionMap.put("/kefu/chat/config/**", "anon");
		
		filterChainDefinitionMap.put("/kefu/extend/**", "anon");	//图片、语音、文件上传后，点击可以直接下载
		
		filterChainDefinitionMap.put("/dev/**", "anon");	//调用的
		return filterChainDefinitionMap;
	}
}
