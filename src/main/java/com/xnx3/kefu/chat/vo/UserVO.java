package com.xnx3.kefu.chat.vo;

import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.kefu.core.bean.ChatUserBean;

/**
 * 获取某个用户信息使用
 * @author 管雷鸣
 *
 */
public class UserVO extends BaseVO implements java.io.Serializable{
	private ChatUserBean user;
	private String onlineState;	//在线状态， online:在线  offline:离线
	
	public ChatUserBean getUser() {
		return user;
	}
	public void setUser(ChatUserBean user) {
		this.user = user;
	}
	public String getOnlineState() {
		return onlineState;
	}
	public void setOnlineState(String onlineState) {
		this.onlineState = onlineState;
	}
	@Override
	public String toString() {
		return "UserVO [user=" + user + ", onlineState=" + onlineState + "]";
	}
}
