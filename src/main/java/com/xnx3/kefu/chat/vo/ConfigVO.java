package com.xnx3.kefu.chat.vo;

import java.util.ArrayList;
import java.util.List;
import com.xnx3.BaseVO;

/**
 * 获取网络配置，比如websocket、api接口地址
 * @author 管雷鸣
 */
public class ConfigVO extends BaseVO{
	private String api;		//api接口的url，格式如 http://xxxxx.com/
	private String socket; 	//socket的url，格式如 ws://xxxxx.com/
	private String jsCode;	//要执行的js代码
	private List<String> jsFile;	//要引入的js文件的url
	
	public String getApi() {
		return api;
	}
	public void setApi(String api) {
		this.api = api;
	}
	public String getSocket() {
		return socket;
	}
	public void setSocket(String socket) {
		this.socket = socket;
	}
	
	public String getJsCode() {
		if(this.jsCode == null) {
			this.jsCode = "";
		}
		return jsCode;
	}
	public void setJsCode(String jsCode) {
		this.jsCode = jsCode;
	}
	public List<String> getJsFile() {
		if(this.jsFile == null) {
			this.jsFile = new ArrayList<String>();
		}
		return jsFile;
	}
	public void setJsFile(List<String> jsFile) {
		this.jsFile = jsFile;
	}
	@Override
	public String toString() {
		return "ConfigVO [api=" + api + ", socket=" + socket + ", jsCode=" + jsCode + ", jsFile=" + jsFile + "]";
	}
	
}
