package com.xnx3.kefu.chat.vo;

import com.xnx3.j2ee.vo.BaseVO;

import net.sf.json.JSONArray;

/**
 * 跟某人聊天的历史记录
 * @author 管雷鸣
 *
 */
public class ChatHistoryListVO extends BaseVO{
	private JSONArray list;

	public JSONArray getList() {
		return list;
	}

	public void setList(JSONArray list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "ChatHistoryList [list=" + list + "]";
	}
	
}
 