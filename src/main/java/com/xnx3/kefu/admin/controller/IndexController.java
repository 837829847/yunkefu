package com.xnx3.kefu.admin.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xnx3.j2ee.util.ActionLogUtil;
import com.xnx3.j2ee.util.Page;
import com.xnx3.j2ee.util.Sql;
import com.xnx3.j2ee.util.SystemUtil;
import com.xnx3.DateUtil;
import com.xnx3.j2ee.pluginManage.PluginManage;
import com.xnx3.j2ee.pluginManage.PluginRegister;
import com.xnx3.j2ee.service.SqlCacheService;
import com.xnx3.j2ee.service.SqlService;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.kefu.core.entity.Question;
import com.xnx3.kefu.core.vo.QuestionListVO;
import com.xnx3.kefu.core.vo.QuestionVO;

/**
 * 登录进入的首页相关
 * @author 管雷鸣
 */
@Controller(value="KefuIndexController")
@RequestMapping("/kefu/admin/index/")
public class IndexController extends BaseController {
	@Resource
	private SqlService sqlService;
	@Resource
	private SqlCacheService sqlCacheService;
	

	/**
	 * 获取功能插件的菜单
	 */
	@ResponseBody
	@RequestMapping(value="getPluginMenu.json", method = RequestMethod.POST)
	public BaseVO list(HttpServletRequest request,Model model){
		//获取网站后台管理系统有哪些功能插件，也一块列出来,以直接在网站后台中显示出来
		String pluginMenu = "";
		if(PluginManage.cmsSiteClassManage.size() > 0){
			for (Map.Entry<String, PluginRegister> entry : PluginManage.cmsSiteClassManage.entrySet()) {
				PluginRegister plugin = entry.getValue();
				pluginMenu += "<li class=\"layui-nav-item\" id=\""+entry.getKey()+"\">\n"
						+ "			<a href=\"javascript:loadUrl('"+plugin.menuHref()+"');\" class=\"itemA\" style=\"color: rgb(51, 51, 51);\">\n"
						+ "				<i class=\"layui-icon firstMenuIcon\">&#xe63b;</i>\n"
						+ "				<span class=\"firstMenuFont\">"+plugin.menuTitle()+"</span>\n"
						+ "			</a>\n"
						+ "		</li>";
			}
		}
		model.addAttribute("pluginMenu", pluginMenu);
				
				//左侧菜单
//				model.addAttribute("menuHTML", TemplateAdminMenuUtil.getLeftMenuHtml());
				
		return success(pluginMenu);
	}
	
}
