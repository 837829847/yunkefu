package com.xnx3.kefu.admin.controller;
import com.xnx3.j2ee.pluginManage.controller.BasePluginController;
import com.xnx3.kefu.core.entity.Kefu;
import com.xnx3.kefu.admin.util.SessionUtil;

/**
 * 这个包下所有Controller父类
 * @author 管雷鸣
 */
public class BaseController extends BasePluginController{
	
	/**
	 * 获取当前登录客服的 {@link Kefu} 信息
	 */
	public Kefu getKefu(){
		return SessionUtil.getKefu();
	}
	
	/**
	 * 获取当前登录客服的 kefu.id
	 * @return 如果没有，则返回0
	 */
	public int getKefuId(){
		Kefu kefu = getKefu();
		if(kefu == null){
			return 0;
		}else{
			return kefu.getId();
		}
	}
}
