package com.xnx3.kefu.admin.controller;

import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xnx3.j2ee.util.ActionLogUtil;
import com.xnx3.j2ee.util.Page;
import com.xnx3.j2ee.util.Sql;
import com.xnx3.j2ee.util.SystemUtil;
import com.xnx3.DateUtil;
import com.xnx3.j2ee.service.SqlCacheService;
import com.xnx3.j2ee.service.SqlService;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.kefu.core.entity.Question;
import com.xnx3.kefu.core.vo.QuestionListVO;
import com.xnx3.kefu.core.vo.QuestionVO;

/**
 * 常见问题设置
 * @author 管雷鸣
 */
@Controller(value="KefuQuestionController")
@RequestMapping("/kefu/admin/question/")
public class QuestionController extends BaseController {
	@Resource
	private SqlService sqlService;
	@Resource
	private SqlCacheService sqlCacheService;
	

	/**
	 * 常见问题列表
	 */
	@ResponseBody
	@RequestMapping("list${api.suffix}")
	public QuestionListVO list(HttpServletRequest request,Model model){
		QuestionListVO vo = new QuestionListVO();
		
		Sql sql = new Sql(request);
		sql.setSearchColumn(new String[]{"title"});
		sql.appendWhere("kefuid="+getKefuId());
		int count = sqlService.count("kefu_question", sql.getWhere());
		Page page = new Page(count, SystemUtil.getInt("LIST_EVERYPAGE_NUMBER"), request);
		sql.setSelectFromAndPage("SELECT * FROM kefu_question", page);
		sql.setDefaultOrderBy("kefu_question.id DESC");
		List<Question> list = sqlService.findBySql(sql, Question.class);
		
		ActionLogUtil.insert(request, "客服后台查看常见问题列表", "第"+page.getCurrentPageNumber()+"页");
		
		vo.setList(list);
		return vo;
	}
	

	/**
	 * 新增/修改
	 * @param id 要修改的question.id 。 如果是新增，这里传入0或者不传
	 */
	@ResponseBody
	@RequestMapping("edit${api.suffix}")
	public QuestionVO edit(
			@RequestParam(value = "id", required = false, defaultValue="0") int id,
			Model model, HttpServletRequest request){
		QuestionVO vo = new QuestionVO();
		Question question;
		if(id < 1){
			//新增
			question = new Question();
			ActionLogUtil.insert(request, "进入新增分类页面");
		}else{
			//修改
			question = sqlService.findById(Question.class, id);
			if(question == null){
				vo.setBaseVO(QuestionVO.FAILURE, "要修改的问题不存在");
				return vo;
			}
			if(question.getKefuid() - getKefuId() != 0){
				vo.setBaseVO(QuestionVO.FAILURE, "当前问题不属于你，无法修改");
				return vo;
			}
			vo.setQuestion(question);
			ActionLogUtil.insert(request, "进入修改常见问题页面", question.toString());
		}
		
		return vo;
	}
	
	/**
	 * 新增、修改的分类进行保存
	 */
	@RequestMapping(value="save${api.suffix}", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO variableSave(Question inputQuestion, HttpServletRequest request){
		Question question = null;
		
		if(inputQuestion.getId() == null || inputQuestion.getId() < 1){
			//新增
			question = new Question();
			question.setKefuid(getKefuId());
		}else{
			//修改
			question = sqlService.findById(Question.class, inputQuestion.getId());
			if(question == null){
				return error("要修改的问题不存在");
			}
			if(question.getKefuid() - getKefuId() != 0){
				return error("当前问题不属于你，无法修改");
			}
		}
		question.setTitle(inputQuestion.getTitle());
		question.setAnswer(inputQuestion.getAnswer());
		question.setUpdatetime(DateUtil.timeForUnix10());
		sqlService.save(question);
		
		deleteCache(question.getKefuid());
		
		ActionLogUtil.insertUpdateDatabase(request, question.getId(), "保存常见问题", question.toString());
		return success();
	}
	
	/**
	 * 删除常见问题
	 */
	@RequestMapping(value="delete${api.suffix}", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO delete(@RequestParam(value = "id", required = false, defaultValue="0") int id, HttpServletRequest request){
		Question question = sqlService.findById(Question.class, id);
		if(question == null){
			return error("要删除的问题不存在");
		}
		if(question.getKefuid() - getKefuId() != 0){
			return error("当前问题不属于你，无法修改");
		}
		sqlService.delete(question);
		deleteCache(question.getKefuid());
		
		ActionLogUtil.insertUpdateDatabase(request, question.getId(), "删除商品", question.toString());
		return success();
	}
	
	/**
	 * 删除当前客服的常见问题缓存
	 * @param kefuid
	 */
	private void deleteCache(int kefuid){
		sqlCacheService.deleteCacheByProperty(Question.class, "kefuid", kefuid);
	}
}
