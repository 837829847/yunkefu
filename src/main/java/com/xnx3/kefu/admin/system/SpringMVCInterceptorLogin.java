package com.xnx3.kefu.admin.system;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import com.xnx3.j2ee.pluginManage.interfaces.SpringMVCInterceptorInterface;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.kefu.core.entity.Kefu;
import com.xnx3.kefu.admin.util.SessionUtil;

/**
 * 拦截器，拦截是否已登陆，未登录
 * @author 管雷鸣
 *
 */
public class SpringMVCInterceptorLogin implements SpringMVCInterceptorInterface{

	@Override
	public List<String> pathPatterns() {
		List<String> list = new ArrayList<String>();
 		list.add("/kefu/admin/user/**");
 		list.add("/kefu/admin/kefu/**");
 		list.add("/kefu/admin/question/**");
 		list.add("/kefu/admin/zuoxi/**");
 		list.add("/kefu/admin/kefuJS/**");
 		list.add("/kefu/admin/tiyan/**");
 		return list;
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String path = request.getServletPath();
		
		//未登录
		if(!SessionUtil.isLogin()){
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.getWriter().write("{"
    				+ "\"result\":\"" + BaseVO.NOT_LOGIN + "\","
    				+ "\"info\":\"not login\""
				+ "}");
			return false;
		}
		
		Kefu kefu = SessionUtil.getKefu();
		if(kefu == null || kefu.getId() == null || kefu.getId() - 0 == 0){
			//无权使用，自己没有所管理的客服
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.getWriter().write("{"
    				+ "\"result\":\"" + BaseVO.NOT_LOGIN + "\","
    				+ "\"info\":\"403, no authority\""
				+ "}");
			return false;
		}
		
		return SpringMVCInterceptorInterface.super.preHandle(request, response, handler);
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		SpringMVCInterceptorInterface.super.postHandle(request, response, handler, modelAndView);
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		SpringMVCInterceptorInterface.super.afterCompletion(request, response, handler, ex);
	}
	
}
