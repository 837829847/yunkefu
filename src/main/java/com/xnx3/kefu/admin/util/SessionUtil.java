package com.xnx3.kefu.admin.util;

import org.springframework.beans.BeanUtils;

import com.xnx3.j2ee.bean.ActiveUser;
import com.xnx3.j2ee.util.ConsoleUtil;
import com.xnx3.kefu.core.entity.Kefu;

/**
 * sessison
 * @author 管雷鸣
 *
 */
public class SessionUtil extends com.xnx3.j2ee.util.SessionUtil{
	//客服管理后台登录后，存储的客服账户信息
	public static final String PLUGIN_NAME_KEFU = "shop:kefu:data";
	
	/**
	 * 获取当前登录的客服
	 */
	public static Kefu getKefu(){
		Object obj = getPlugin(PLUGIN_NAME_KEFU);
		
		//jar包方式运行，可能是开发环境
		if (obj instanceof Kefu) {
			return (Kefu) obj;
		} else {
//			Gson gson = new Gson();
			Kefu kefu = new Kefu();
			BeanUtils.copyProperties(obj, kefu);
			ConsoleUtil.debug("fuzhi -- "+kefu.toString());
			return kefu;
		}
		
	}
	
	/**
	 * 设置当前登录的客服
	 */
	public static void setKefu(Kefu kefu){
		setPlugin(PLUGIN_NAME_KEFU, kefu);
	}
	
}

