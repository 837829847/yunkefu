/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.31.242-novel
 Source Server Type    : MySQL
 Source Server Version : 50741
 Source Host           : 192.168.31.242
 Source Database       : yunkefu

 Target Server Type    : MySQL
 Target Server Version : 50741
 File Encoding         : utf-8

 Date: 05/10/2023 17:05:04 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `kefu`
-- ----------------------------
DROP TABLE IF EXISTS `kefu`;
CREATE TABLE `kefu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auto_reply` char(255) DEFAULT NULL COMMENT '无客服在线时，自动回复的文字。若为空，或者空字符串，则不开启此功能。限制30个字符',
  `email` char(30) DEFAULT NULL COMMENT '接收提醒的邮箱地址，若上面开启邮件提醒通知了，那么这里是必须要填写的，不然是要通知谁',
  `phone` char(18) DEFAULT NULL COMMENT '手机号，若上面开启短信提醒通知了，那么这里是必须要填写的，不然是要通知谁',
  `use_kefu` tinyint(2) DEFAULT NULL COMMENT '是否开启在线客服功能。0不开启，默认；  1开启。',
  `use_offline_email` tinyint(2) DEFAULT NULL COMMENT '是否使用离线时的邮件通知提醒，默认为0，不启用。  1为启用',
  `use_offline_sms` tinyint(2) DEFAULT NULL COMMENT '是否使用离线时的短信通知提醒，默认为0，不启用。  1为启用',
  `userid` int(11) DEFAULT NULL COMMENT '所属用户，属于哪个用户，对应 User.id',
  `offline_auto_raply` char(255) DEFAULT NULL COMMENT '无客服在线时，自动回复的文字。若为空，或者空字符串，则不开启此功能。限制255个字符',
  `chatid` char(32) DEFAULT NULL COMMENT '用于发起客服聊天的id。会根据这个id找到是哪个客服平台，然后自动分配客服平台下的一个坐席对接到客户进行服务。',
  `auto_hello_text` char(200) DEFAULT NULL,
  `auto_hello_time` int(11) DEFAULT NULL,
  `freeze` tinyint(2) DEFAULT NULL COMMENT '当前客服平台的冻结状态，1已冻结， 0未冻结',
  PRIMARY KEY (`id`),
  KEY `suoyin_index` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=162 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `kefu`
-- ----------------------------
BEGIN;
INSERT INTO `kefu` VALUES ('1', '您好，请问有什么可以帮您？（这句话在坐席后台的客服设置中可自由修改）', null, null, '1', '1', null, '243', '你好，我现在不在线，你可以发过来你的电话或微信，我看到后联系你（这句话在坐席后台的客服设置中可自由修改）', '16d870ad4310470e9424d877121b413c', '', '-1', '0');
COMMIT;

-- ----------------------------
--  Table structure for `kefu_inset`
-- ----------------------------
DROP TABLE IF EXISTS `kefu_inset`;
CREATE TABLE `kefu_inset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kefuid` int(11) DEFAULT NULL COMMENT '对应客服系统的客服平台id，kefu.id',
  `otherid` char(50) DEFAULT NULL COMMENT '别人平台的id，比如每个商家店铺要有一个自己的客服，那这个是商家的id；如果是每个网站都要有一个自己的客服，这里是网站的id',
  `password` char(64) DEFAULT NULL COMMENT '别人平台中，有一项菜单是在线客服， 点击在线客服后就会右侧内容显示区域打开在线客服页面。从别人后台打开在线客服后台时，会传递这个参数，使用这个参数自动登录',
  PRIMARY KEY (`id`),
  KEY `suoyin_index` (`otherid`,`kefuid`,`password`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `kefu_inset_user`
-- ----------------------------
DROP TABLE IF EXISTS `kefu_inset_user`;
CREATE TABLE `kefu_inset_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `other_userid` char(64) DEFAULT NULL COMMENT '别人平台的user.id，限制64个字符或以内的字符',
  `userid` int(11) DEFAULT NULL COMMENT '别人平台的用户对应到自己客服系统的客服平台的user.id.这是客服平台自己的user.id',
  PRIMARY KEY (`id`),
  KEY `suoyin_index` (`userid`,`other_userid`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `kefu_js`
-- ----------------------------
DROP TABLE IF EXISTS `kefu_js`;
CREATE TABLE `kefu_js` (
  `id` int(11) NOT NULL,
  `background_color` char(15) DEFAULT NULL COMMENT '背景色的颜色，十六进制，如： #FFFFFF',
  `color` char(15) DEFAULT NULL COMMENT '文字、图片的颜色',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `kefu_js`
-- ----------------------------
BEGIN;
INSERT INTO `kefu_js` VALUES ('1', '#7687be', '#f5f0f0');
COMMIT;

-- ----------------------------
--  Table structure for `kefu_question`
-- ----------------------------
DROP TABLE IF EXISTS `kefu_question`;
CREATE TABLE `kefu_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer` varchar(1000) DEFAULT NULL COMMENT '问题的答案，答',
  `kefuid` int(11) DEFAULT NULL COMMENT '该条常见问题所属哪个客服平台',
  `title` char(50) DEFAULT NULL COMMENT '问题标题，问',
  `updatetime` int(11) DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`),
  KEY `suoyin_index` (`kefuid`,`updatetime`,`title`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `kefu_question`
-- ----------------------------
BEGIN;
INSERT INTO `kefu_question` VALUES ('2', '当前尚未开源，还在不断调整中，不过我们计划于2021.4月份开源！', '1', '这个客服系统开源吗？', '1610775782'), ('3', '您好，您可通过云客服官网 xxxxxx 在线免费开通使用。', '1', '怎么免费开通使用？', '1610778286'), ('4', '1. 支持手机端、电脑端。[br]2. 聊天记录使用ELK持久化保存。支撑巨量聊天记录无延迟[br]3. 支持离线消息。上线后自动接收离线消息[br]4. 聊天中,上滑可无感加载历史聊天记录[br]5. 用户打开聊天窗口时自动给用户发送欢迎语、常见问题[br]6. 当用户发送消息，但客服不在线时，会自动发送设置好的一段文字给用户[br]7. 对已录入的常见问题进行自动匹配、自动回复。[br]8. 本地消息存储，最新消息的本地缓存，极致流畅度[br]9. 支持图片上传、发送图片、图片放大。[br]10. 支持表情[br]11. 新消息提醒。当我跟A用户正在聊天，B用户给我发来消息，顶部实时显示别的用户发来的新消息提醒[br]12. 支持自定义插件，如发送订单、更多自定义表情、发送商品、发送位置、文件、名片、礼物……都可以用插件方式扩展。[br]13. 支持自定义心跳及心跳包扩展[br]14. 支持断线自动重连[br]15. ...', '1', '这客服有什么功能？', '1627004676');
COMMIT;

-- ----------------------------
--  Table structure for `kefu_user`
-- ----------------------------
DROP TABLE IF EXISTS `kefu_user`;
CREATE TABLE `kefu_user` (
  `id` int(11) NOT NULL,
  `kefuid` int(11) DEFAULT NULL COMMENT '对应kefu.id',
  PRIMARY KEY (`id`),
  KEY `suoyin_index` (`kefuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `kefu_zuoxi`
-- ----------------------------
DROP TABLE IF EXISTS `kefu_zuoxi`;
CREATE TABLE `kefu_zuoxi` (
  `id` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kefuid` int(11) DEFAULT NULL COMMENT '所属kefu，属于哪个客服平台下的坐席，对应kefu.id。按照设想，一个kefu平台下会有多个坐席',
  `userid` int(11) DEFAULT NULL COMMENT '当前客服坐席对应的User,user.id。是哪个用户所属这个坐席。',
  PRIMARY KEY (`id`),
  KEY `suoyin_index` (`kefuid`,`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `kefu_zuoxi`
-- ----------------------------
BEGIN;
INSERT INTO `kefu_zuoxi` VALUES ('9752d379cbe641039de8c68974319bb9', '1', '243'), ('f347a8b360ec471ab590cf67b8a6e448', '1', '573');
COMMIT;

-- ----------------------------
--  Table structure for `permission`
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` char(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '描述信息，备注，只是给后台设置权限的人看的',
  `url` char(80) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '资源url',
  `name` char(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '名字，菜单的名字，显示给用户的',
  `parent_id` int(11) DEFAULT NULL COMMENT '上级资源的id',
  `percode` char(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `menu` smallint(6) DEFAULT NULL,
  `rank` int(11) DEFAULT '0' COMMENT '排序，数字越小越靠前',
  `icon` char(100) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '图标字符，这里是layui 的图标 ， https://www.layui.com/doc/element/icon.html ，这里存的是 unicode  字符，如  &#xe60c;',
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`url`,`name`,`percode`),
  KEY `parent_id` (`parent_id`),
  KEY `suoyin_index` (`menu`,`rank`)
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='Shiro权限管理中的资源';

-- ----------------------------
--  Records of `permission`
-- ----------------------------
BEGIN;
INSERT INTO `permission` VALUES ('12', '后台的用户管理', '/admin/user/list.do', '用户管理', '0', 'adminUser', '1', '1', '&#xe612;'), ('13', '后台用户管理下的菜单', '/admin/user/list.do', '用户列表', '12', 'adminUserList', null, '0', ''), ('14', '后台用户管理下的菜单', '/admin/user/delete.do', '删除用户', '12', 'adminUserDelete', null, '0', ''), ('15', '管理后台－系统管理栏目', '/admin/system/index.do', '系统管理', '0', 'adminSystem', '0', '4', '&#xe614;'), ('16', '管理后台－系统管理－系统参数、系统变量', '/admin/system/variableList.do', '系统变量', '15', 'adminSystemVariable', '1', '0', ''), ('18', '退出登录，注销登录状态', '/user/logout.do', '退出登录', '0', 'userLogout', '1', '1000002', '&#xe633;'), ('21', '更改当前登录的密码', 'javascript:updatePassword();', '更改密码', '0', 'adminUserUpdatePassword', '1', '1000001', '&#xe642;'), ('44', '后台，权限管理', '/admin/role/roleList.do', '权限管理', '0', 'adminRole', '0', '3', '&#xe628;'), ('46', '后台，权限管理，新增、编辑角色', '/admin/role/editRole.do', '编辑角色', '44', 'adminRoleRole', null, '101', ''), ('48', '后台，权限管理，角色列表', '/admin/role/roleList.do', '角色管理', '44', 'adminRoleRoleList', '1', '1', ''), ('49', '后台，权限管理，删除角色', '/admin/role/deleteRole.do', '删除角色', '44', 'adminRoleDeleteRole', null, '102', ''), ('51', '后台，权限管理，资源Permission的添加、编辑功能', '/admin/role/editPermission.do', '编辑资源', '44', 'adminRolePermission', null, '103', ''), ('53', '后台，权限管理，资源Permission列表', '/admin/role/permissionList.do', '资源管理', '44', 'adminRolePermissionList', '1', '2', ''), ('54', '后台，权限管理，删除资源Permission', '/admin/role/deletePermission.do', '删除资源', '44', 'adminRoleDeletePermission', null, '104', ''), ('55', '后台，权限管理，编辑角色下资源', '/admin/role/editRolePermission.do', '编辑角色下资源', '44', 'adminRoleEditRolePermission', null, '105', ''), ('56', '后台，权限管理，编辑用户所属角色', '/admin/role/editUserRole.do', '编辑用户所属角色', '44', 'adminRoleEditUserRole', null, '106', ''), ('71', '后台，日志管理', '/admin/log/list.do', '日志统计', '0', 'adminLog', '1', '5', '&#xe62c;'), ('72', '后台，日志管理，用户动作的日志列表', '/admin/log/list.do', '用户动作', '71', 'adminLogList', '1', '1', ''), ('74', '管理后台－系统管理，新增、修改系统的全局变量', '/admin/system/variable.do', '修改变量', '15', 'adminSystemVariable', null, '0', ''), ('80', '后台，用户管理，查看用户详情', '/admin/user/view.do', '用户详情', '12', 'adminUserView', null, '0', ''), ('81', '后台，用户管理，冻结、解除冻结会员。冻结后用户将不能登录', '/admin/user/updateFreeze.do', '冻结用户', '12', 'adminUserUpdateFreeze', null, '0', ''), ('82', '后台，历史发送的短信验证码', '/admin/smslog/list.do', '短信验证', '0', 'adminSmsLogList', '1', '2', '&#xe63a;'), ('114', '后台管理首页，登录后台的话，需要授权此项，不然登录成功后仍然无法进入后台，被此页给拦截了', null, '管理后台', '0', 'adminIndex', null, '0', ''), ('115', '管理后台首页', '', '后台首页', '114', 'adminIndexIndex', null, '0', ''), ('116', '删除系统变量', 'admin/system/deleteVariable.do', '删除变量', '15', 'adminSystemDeleteVariable', null, '0', ''), ('117', '后台，日志管理，所有动作的日志图表', '/admin/log/cartogram.do', '动作统计', '71', 'adminLogCartogram', '1', '2', ''), ('120', '可以将某个资源设置为菜单是菜单项', '/admin/role/editPermissionMenu.do', '设为菜单', '44', 'adminRoleEditPermissionMenu', '0', '107', ''), ('121', '对资源进行排序', '/admin/role/savePermissionRank.do', '资源排序', '44', 'adminRoleEditPermissionRank', '0', '108', ''), ('122', '客服的坐席后台中的坐席设置', '/kefu/zuoxi/set.jsp', '坐席设置', '0', 'shezhizuoxi', '1', '10001', '&#xe620;'), ('123', '在线坐席，只要客服在这个中，就能随时收到客户的咨询', '/kefu/admin/zuoxi/pc.jsp', '在线坐席', '0', 'zaixianzuoxi', '1', '10002', '&#xe63a;'), ('124', '开通、管理客服', '/kefu/superadmin/list.do', '客服管理', '0', 'adminKefu', '1', '1', '&#xe770;');
COMMIT;

-- ----------------------------
--  Table structure for `plugin_offlineweixinnotification`
-- ----------------------------
DROP TABLE IF EXISTS `plugin_offlineweixinnotification`;
CREATE TABLE `plugin_offlineweixinnotification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `addtime` int(11) DEFAULT NULL,
  `chatid` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kefuid` int(11) DEFAULT NULL COMMENT '该条常见问题所属哪个客服平台',
  `openid` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `suoyin_index` (`kefuid`,`openid`,`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Table structure for `role`
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '角色名',
  `description` char(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '角色说明',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='Shiro权限管理中的角色表';

-- ----------------------------
--  Records of `role`
-- ----------------------------
BEGIN;
INSERT INTO `role` VALUES ('1', '客服平台', '客服管理后台'), ('9', '总管理', '总后台管理，超级管理员'), ('10', '坐席后台', '客服平台下开通的某个坐席使用的后台');
COMMIT;

-- ----------------------------
--  Table structure for `role_permission`
-- ----------------------------
DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE `role_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roleid` int(11) DEFAULT NULL COMMENT '角色id，role.id，一个角色可以拥有多个permission资源',
  `permissionid` int(11) DEFAULT NULL COMMENT '资源id，permission.id，一个角色可以拥有多个permission资源',
  PRIMARY KEY (`id`),
  KEY `roleid` (`roleid`)
) ENGINE=InnoDB AUTO_INCREMENT=226 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='Shiro权限管理中，角色所拥有哪些资源的操作权限';

-- ----------------------------
--  Records of `role_permission`
-- ----------------------------
BEGIN;
INSERT INTO `role_permission` VALUES ('17', '9', '15'), ('18', '9', '16'), ('20', '9', '18'), ('23', '9', '21'), ('49', '9', '44'), ('51', '9', '46'), ('53', '9', '48'), ('54', '9', '49'), ('56', '9', '51'), ('58', '9', '53'), ('59', '9', '54'), ('60', '9', '55'), ('77', '9', '74'), ('204', '9', '114'), ('205', '9', '115'), ('209', '9', '116'), ('212', '9', '120'), ('213', '9', '121'), ('214', '9', '82'), ('216', '11', '114'), ('217', '11', '115'), ('218', '10', '18'), ('219', '10', '21'), ('220', '10', '114'), ('221', '10', '115'), ('222', '10', '122'), ('223', '10', '123'), ('224', '9', '56'), ('225', '9', '124');
COMMIT;

-- ----------------------------
--  Table structure for `sms_log`
-- ----------------------------
DROP TABLE IF EXISTS `sms_log`;
CREATE TABLE `sms_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` char(6) CHARACTER SET utf8 DEFAULT NULL COMMENT '发送的验证码，6位数字',
  `userid` int(11) DEFAULT NULL COMMENT '使用此验证码的用户编号，user.id',
  `used` tinyint(2) DEFAULT '0' COMMENT '是否使用，0未使用，1已使用',
  `type` tinyint(3) DEFAULT NULL COMMENT '验证码所属功能类型，  1:登录  ； 2:找回密码',
  `addtime` int(11) DEFAULT NULL COMMENT '创建添加时间，linux时间戳10位',
  `phone` char(11) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '接收短信的手机号',
  `ip` char(15) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '触发发送操作的客户ip地址',
  PRIMARY KEY (`id`),
  KEY `code` (`code`,`userid`,`used`,`type`,`addtime`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='短信验证码发送的日志记录';

-- ----------------------------
--  Table structure for `system`
-- ----------------------------
DROP TABLE IF EXISTS `system`;
CREATE TABLE `system` (
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '参数名,程序内调用',
  `description` char(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '说明描述',
  `value` varchar(2000) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '值',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lasttime` int(11) DEFAULT '0' COMMENT '最后修改时间，10位时间戳',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10035 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统变量，系统的一些参数相关，比如系统名字等';

-- ----------------------------
--  Records of `system`
-- ----------------------------
BEGIN;
INSERT INTO `system` VALUES ('USER_REG_ROLE', '用户注册后的权限，其值对应角色 role.id', '1', '6', '1506333513'), ('SITE_NAME', '网站名称1', '云客服', '7', '1619410690'), ('SITE_KEYWORDS', '网站SEO搜索的关键字，首页根内页没有设置description的都默认用此.', '雷鸣云客服系统', '8', '1619410682'), ('SITE_DESCRIPTION', '网站SEO描述，首页根内页没有设置description的都默认用此', '管雷鸣', '9', null), ('CURRENCY_NAME', '站内货币名字', '仙玉', '10', null), ('ROLE_USER_ID', '普通用户的角色id，其值对应角色 role.id', '1', '15', '1506333544'), ('ROLE_SUPERADMIN_ID', '超级管理员的角色id，其值对应角色 role.id', '9', '16', '1506333534'), ('USER_HEAD_PATH', '用户头像(User.head)上传OSS或服务器进行存储的路径，存储于哪个文件夹中。<br/><b>注意</b><br/>1.这里最前面不要加/，最后要带/，如 head/<br/>2.使用中时，中途最好别改动，不然改动之前的用户设置好的头像就都没了', 'head/', '21', '1506481173'), ('ALLOW_USER_REG', '是否允许用户自行注册。<br/>1：允许用户自行注册<br/>0：禁止用户自行注册', '1', '22', '1507537911'), ('LIST_EVERYPAGE_NUMBER', '所有列表页面，每页显示的列表条数。', '15', '23', '1507538582'), ('SERVICE_MAIL', '网站管理员的邮箱。<br/>当网站出现什么问题，或者什么提醒时，会自动向管理员邮箱发送提示信息', '123456@qq.com', '24', '1511934294'), ('MASTER_SITE_URL', '设置当前建站系统的域名。如建站系统的登录地址为 http://wang.market/login.do ，那么就将 http://wang.market/  填写到此处。', '', '134', '1515401613'), ('ATTACHMENT_FILE_URL', '设置当前建站系统中，上传的图片、附件的访问域名。若后续想要将附件转到云上存储、或开通CDN加速，可平滑上云使用。设置的值格式如： https://www.xxx.com/', '/', '135', '1619410593'), ('ATTACHMENT_FILE_MODE', '当前文件附件存储使用的模式，用的阿里云oss，还是服务器本身磁盘进行存储。<br/>可选一：aliyunOSS：阿里云OSS模式存储<br/>可选二：localFile：服务器本身磁盘进行附件存储', 'localFile', '136', '1619410557'), ('STATIC_RESOURCE_PATH', '系统静态资源如css、js等调用的路径。填写如:  //res.weiunity.com/   默认是/ 则是调取当前项目的资源，以相对路径调用', '//res.zvo.cn/', '150', '1540972613'), ('ROLE_ADMIN_SHOW', '总管理后台中，是否显示权限管理菜单。1为显示，0为不显示', '0', '151', '1540972613'), ('FEN_GE_XIAN', '分割线，系统变量，若您自己添加，请使用id为 10000以后的数字。 10000以前的数字为系统预留。', '10000', '10000', '1540972613');
COMMIT;

-- ----------------------------
--  Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户id编号',
  `username` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '用户名',
  `email` char(40) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '邮箱',
  `password` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '加密后的密码',
  `head` char(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '头像',
  `nickname` char(30) COLLATE utf8_unicode_ci NOT NULL COMMENT '姓名、昵称',
  `authority` char(20) COLLATE utf8_unicode_ci NOT NULL COMMENT '用户权限,主要纪录表再user_role表，一个用户可以有多个权限。多个权限id用,分割，如2,3,5',
  `regtime` int(10) unsigned NOT NULL COMMENT '注册时间,时间戳',
  `lasttime` int(10) unsigned NOT NULL COMMENT '最后登录时间,时间戳',
  `regip` char(15) COLLATE utf8_unicode_ci NOT NULL COMMENT '注册ip',
  `salt` char(6) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'shiro加密使用',
  `phone` char(11) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '手机号,11位',
  `currency` int(11) DEFAULT '0' COMMENT '资金，可以是积分、金币、等等站内虚拟货币',
  `referrerid` int(11) DEFAULT '0' COMMENT '推荐人的用户id。若没有推荐人则默认为0',
  `freezemoney` float(8,2) DEFAULT '0.00' COMMENT '账户冻结余额，金钱,RMB，单位：元',
  `lastip` char(15) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '最后一次登陆的ip',
  `isfreeze` tinyint(2) DEFAULT '0' COMMENT '是否已冻结，1已冻结（拉入黑名单），0正常',
  `money` float(8,2) DEFAULT '0.00' COMMENT '账户可用余额，金钱,RMB，单位：元',
  `sign` char(80) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '个人签名',
  `sex` char(4) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '男、女、未知',
  `version` int(11) DEFAULT '0',
  `kefuid` int(11) DEFAULT NULL COMMENT '此用户是属于哪个客服管理下的坐席，对应 Kefu.id',
  `chatid` char(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '此用户是属于哪个客服管理下的坐席，对应 Kefu.id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`,`username`,`phone`) USING BTREE,
  KEY `username` (`username`,`email`,`phone`,`isfreeze`) USING BTREE,
  KEY `suoyin_index` (`chatid`,`username`,`email`,`lasttime`)
) ENGINE=InnoDB AUTO_INCREMENT=575 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='用户信息表。系统登陆的用户信息都在此处';

-- ----------------------------
--  Records of `user`
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES ('1', 'admin', '', '94940b4491a87f15333ed68cc0cdf833', 'default.png', '总管理', '9', '1512818402', '1682251135', '127.0.0.1', '9738', '17000000002', '0', '0', '0.00', '192.168.31.95', '0', '0.00', null, null, '1', null, '3f806a0bf4d04687b7a759b69d041221'), ('243', 'kefu', '', '70cf924445c5d2cb4df42bfabae897f0', 'https://res.zvo.cn/kefu/images/head.png', '雷鸣云网络', '1', '1488446743', '1682256074', '218.56.88.231', '6922', '', '0', '1', '0.00', '192.168.31.95', '0', '0.00', null, null, '286', '1', '9752d379cbe641039de8c68974319bb9'), ('573', 'zuoxi', null, '9d900f1cbf8ff593f0aed4d846ca12f1', 'https://res.zvo.cn/kefu/images/head.png', 'zuoxiwe', '10', '1629423160', '1682256592', '127.0.0.1', '0898', null, '0', '243', null, '192.168.31.95', '0', null, null, null, '2', null, null);
COMMIT;

-- ----------------------------
--  Table structure for `user_role`
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL COMMENT '用户的id，user.id,一个用户可以有多个角色',
  `roleid` int(11) DEFAULT NULL COMMENT '角色的id，role.id ，一个用户可以有多个角色',
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=597 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='用户拥有哪些角色';

-- ----------------------------
--  Records of `user_role`
-- ----------------------------
BEGIN;
INSERT INTO `user_role` VALUES ('257', '243', '1'), ('412', '392', '10'), ('413', '1', '9'), ('415', '394', '1'), ('416', '395', '1'), ('417', '396', '1'), ('418', '397', '1'), ('419', '398', '1'), ('420', '399', '1'), ('421', '400', '1'), ('422', '401', '1'), ('423', '402', '1'), ('424', '403', '1'), ('425', '404', '1'), ('426', '405', '1'), ('427', '406', '1'), ('428', '407', '1'), ('429', '408', '1'), ('430', '409', '1'), ('431', '410', '1'), ('432', '411', '1'), ('433', '412', '1'), ('434', '413', '1'), ('435', '414', '1'), ('436', '415', '1'), ('437', '416', '1'), ('438', '417', '1'), ('439', '418', '1'), ('440', '419', '1'), ('441', '420', '1'), ('442', '421', '1'), ('443', '422', '1'), ('444', '423', '1'), ('445', '424', '1'), ('446', '425', '1'), ('447', '426', '1'), ('448', '427', '1'), ('449', '428', '1'), ('450', '429', '1'), ('451', '430', '1'), ('452', '431', '1'), ('453', '432', '1'), ('454', '433', '1'), ('455', '434', '1'), ('456', '435', '1'), ('457', '436', '1'), ('458', '437', '1'), ('459', '438', '1'), ('460', '439', '1'), ('461', '440', '1'), ('462', '441', '1'), ('463', '442', '1'), ('464', '443', '1'), ('465', '444', '1'), ('466', '445', '1'), ('467', '446', '1'), ('468', '447', '1'), ('469', '448', '1'), ('470', '449', '1'), ('471', '450', '1'), ('472', '451', '1'), ('473', '452', '1'), ('474', '453', '1'), ('475', '454', '1'), ('476', '455', '1'), ('477', '456', '1'), ('478', '457', '1'), ('479', '458', '1'), ('480', '459', '1'), ('481', '460', '1'), ('482', '461', '1'), ('483', '462', '1'), ('484', '463', '1'), ('485', '464', '1'), ('486', '465', '1'), ('487', '466', '1'), ('488', '467', '1'), ('489', '468', '1'), ('490', '469', '1'), ('491', '470', '1'), ('492', '471', '1'), ('493', '472', '1'), ('494', '473', '1'), ('495', '474', '1'), ('496', '475', '1'), ('497', '476', '1'), ('498', '477', '1'), ('499', '478', '1'), ('500', '479', '1'), ('501', '480', '1'), ('502', '481', '1'), ('503', '482', '1'), ('504', '483', '1'), ('505', '484', '1'), ('506', '485', '1'), ('507', '486', '1'), ('508', '487', '1'), ('509', '488', '1'), ('510', '489', '1'), ('511', '490', '1'), ('512', '491', '1'), ('513', '492', '1'), ('514', '493', '1'), ('515', '494', '1'), ('516', '495', '1'), ('517', '496', '1'), ('518', '497', '1'), ('519', '498', '1'), ('520', '499', '1'), ('521', '500', '1'), ('522', '501', '1'), ('523', '502', '1'), ('524', '503', '1'), ('525', '504', '1'), ('526', '505', '1'), ('527', '506', '1'), ('528', '507', '1'), ('529', '508', '1'), ('530', '509', '1'), ('531', '510', '1'), ('532', '511', '1'), ('533', '512', '1'), ('534', '513', '1'), ('535', '514', '1'), ('536', '515', '1'), ('537', '516', '1'), ('538', '517', '1'), ('539', '518', '1'), ('540', '519', '1'), ('541', '520', '1'), ('542', '521', '1'), ('543', '522', '1'), ('544', '523', '1'), ('545', '524', '1'), ('546', '525', '1'), ('547', '526', '1'), ('548', '527', '1'), ('549', '528', '1'), ('550', '529', '1'), ('551', '530', '1'), ('552', '531', '1'), ('553', '532', '1'), ('554', '533', '1'), ('555', '534', '1'), ('556', '535', '1'), ('557', '536', '1'), ('558', '537', '1'), ('559', '538', '1'), ('560', '539', '1'), ('561', '540', '1'), ('562', '541', '1'), ('563', '542', '1'), ('564', '543', '1'), ('565', '544', '1'), ('566', '545', '1'), ('567', '546', '1'), ('568', '547', '1'), ('569', '548', '1'), ('570', '549', '1'), ('571', '550', '1'), ('572', '551', '1'), ('573', '552', '1'), ('574', '553', '1'), ('575', '554', '1'), ('576', '555', '1'), ('577', '556', '1'), ('578', '557', '1'), ('579', '558', '1'), ('580', '559', '1'), ('581', '560', '1'), ('582', '561', '1'), ('583', '562', '1'), ('584', '563', '1'), ('585', '564', '1'), ('586', '565', '1'), ('587', '566', '1'), ('588', '567', '1'), ('589', '568', '1'), ('590', '569', '1'), ('591', '570', '1'), ('592', '571', '1'), ('593', '572', '1'), ('594', '393', '1'), ('595', '573', '1'), ('596', '574', '1');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
