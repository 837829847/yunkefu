| ![输入图片说明](https://cdn.weiunity.com/site/341/news/6659bb2111794cd8b70c90616cbd434b.png "在这里输入图片标题")  |  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0306/171852_eab00bf0_429922.png "在这里输入图片标题") | ![输入图片说明](https://images.gitee.com/uploads/images/2021/0306/172305_300ad448_429922.png "在这里输入图片标题") |
|---|---|---|

## 简介
SAAS客服系统，后台在线开通客服，每个客服完全独立管理。可用于PC网站、手机网站、小程序、APP等，使之快速拥有客服对话能力。

## 功能
1. 支持手机端、电脑端。
1. 聊天记录使用ELK持久化保存。支撑巨量聊天记录无延迟
1. 支持离线消息。上线后自动接收离线消息
1. 聊天中,上滑可无感加载历史聊天记录
1. 用户打开聊天窗口时自动给用户发送欢迎语、常见问题
1. 当用户发送消息，但客服不在线时，会自动发送设置好的一段文字给用户
1. 对已录入的常见问题进行自动匹配、自动回复。
1. 本地消息存储，最新消息的本地缓存，极致流畅度
1. 支持图片上传、发送图片、图片放大。
1. 支持表情
1. 新消息提醒。当我跟A用户正在聊天，B用户给我发来消息，顶部实时显示别的用户发来的新消息提醒
1. 支持自定义插件，如发送订单、更多自定义表情、发送商品、发送位置、文件、名片、礼物……都可以用插件方式扩展。
1. 支持自定义心跳及心跳包扩展
1. 支持断线自动重连

#### 客服设置
客服可以自由设置自己的客服名字、头像、欢迎语（用户打开跟客服的聊天窗口时，自动发送给用户的文字）、以及离线自动回复（当用户给客服发消息，但客服不在线时，客服自动回复用户的文字）。

#### 常见问题库
每个客服平台都可以设置自己的常见问题库（也就是问、答），这里可以设置多少条常见问题，当用户发起跟自己的聊天时，会自动将常见问题的提问列表发给用户，用户点击某一条问题，系统自动回复给用户答案。

#### 客服JS
客服可以生成JS调用路径，非常方便的给第三方平台调用。比如，将这个js文件引入网站，便可让网站快速拥有客服功能！同时客服还可以设置第三方平台中，显示的客服入口，比如客服入口的背景色、文字及图片颜色等。

#### 在线坐席
客服没事就点开在线坐席页面，当有用户提问时，如果你在此页面中，那么就会实时将用户的问题显示出来，客服就可以跟用户实时进行交流。

## 快速体验
可以进入我们这个客服项目的网站，快速测试体验：  
[www.kefu.zvo.cn](http://www.kefu.zvo.cn)

## 二次开发
#### 基本环境
jdk8、maven3、mysql5.7（版本一定是5.7！不要用8.0等其他版本）
#### 运行方式
1. 下载Ecipse。不知道如何下载，可参考 https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3600899&doc_id=1101390  
2. eclipse中导入本git项目。 不会导入，可参考 https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3600882&doc_id=1101390  
3. 安装mysql 5.7  
4. 导入mysql数据库。数据库文件位于 else/kefu.sql
5. 修改数据库配置文件，修改方式参考 https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3634642&doc_id=1101390  
6. 修改文件上传存储，在 application.properties中的 fileupload.domain、fileupload.storage.local.path 这两个配置项
6. 运行。
这里提供了一个Eclipse中运行的步骤，具体可参考 https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3600882&doc_id=1101390

#### 测试体验
跑起来后访问  
http://localhost:8080/demo.html  
即可看到效果。  
更多的说明，比如后台地址、账号密码了、怎么使用了，可以参考此文档 https://gitee.com/leimingyun/dashboard/wikis/leimingyun/kefuxitongshiyongshuoming/preview?sort_id=4272405&doc_id=1542820

#### 包结构
com.xnx3.kefu.core 客服核心实现  
com.xnx3.kefu.admin 客服管理后台，每个客服都有一个自己管理后台（客服分多个坐席）  
com.xnx3.kefu.superadmin 超级管理后台  
com.xnx3.yunkefu.inset 第三方系统接入云客服，让第三方系统快速拥有客服功能  

## 服务器安装部署文档
https://gitee.com/leimingyun/dashboard/wikis/leimingyun/kefuxitongshiyongshuoming/preview?sort_id=7954273&doc_id=1542820

## 跟第三方系统对接文档
开放接口，让你本身的系统快速拥有在线客服的功能。  
https://e.gitee.com/leimingyun/doc/share/60f7bc4cb4fb2338/?sub_id=7957633


## 前端 kefu.js 自由定制聊天窗口
https://gitee.com/mail_osc/kefu.js
 
## 升级
v2.0 2023.2.x
v1.x升级到 v2.0，系统底层支持发生了比较大的变化，将一些组件模块化，如网络请求、存储、日志等全部模块化，更容易扩展，同时极大降低了应用包体积，降低了内存占用。  
application.properties 中，elasticsearch的配置由原本的  

````
wm.elasticsearch.xxxxxx
````

变为

````

````

## 其他
作者：管雷鸣  
微信：xnx3com  
产品官网：www.kefu.zvo.cn  
公司官网：www.leimingyun.com  
QQ交流群：763362335  


## 开源项目

致力于开源基础化信息建设，如有需要，可直接拿去使用。这里列出了我部分开源项目：

| 项目| star数量 | 简介 |  
| --- | --- | --- |
|[wangmarket CMS](https://gitee.com/mail_osc/wangmarket) | ![](https://gitee.com/mail_osc/wangmarket/badge/star.svg?theme=white) | [私有部署自己的SAAS建站系统](https://gitee.com/mail_osc/wangmarket)  |
|[obs-datax-plugins](https://gitee.com/HuaweiCloudDeveloper/obs-datax-plugins) | ![](https://gitee.com/HuaweiCloudDeveloper/obs-datax-plugins/badge/star.svg?theme=white) | [Datax 的 华为云OBS 插件](https://gitee.com/HuaweiCloudDeveloper/obs-datax-plugins) |
| [templatespider](https://gitee.com/mail_osc/templatespider) | ![](https://gitee.com/mail_osc/templatespider/badge/star.svg?theme=white) | [扒网站工具，所见网站皆可为我所用](https://gitee.com/mail_osc/templatespider) |
|[FileUpload](https://gitee.com/mail_osc/FileUpload)| ![](https://gitee.com/mail_osc/FileUpload/badge/star.svg?theme=white ) | [文件上传，各种存储任意切换](https://gitee.com/mail_osc/FileUpload) |
| [cms client](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-obs-website-wangmarket-cms) | ![](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-obs-website-wangmarket-cms/badge/star.svg?theme=white) | [云服务深度结合无服务器建站](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-obs-website-wangmarket-cms)  |
| [kefu.js](https://gitee.com/mail_osc/kefu.js) | ![](https://gitee.com/mail_osc/kefu.js/badge/star.svg?theme=white ) | https://gitee.com/mail_osc/kefu.js | [在线聊天的前端框架](https://gitee.com/mail_osc/kefu.js)  | 
| [msg.js](https://gitee.com/mail_osc) | ![](https://gitee.com/mail_osc/msg/badge/star.svg?theme=white ) | [轻量级js消息提醒组件](https://gitee.com/mail_osc)  | 
| [translate.js](https://gitee.com/mail_osc/translate) | ![](https://gitee.com/mail_osc/translate/badge/star.svg?theme=white )  | [三行js实现 html 全自动翻译](https://gitee.com/mail_osc/translate)  | 
| [WriteCode](https://gitee.com/mail_osc/writecode) | ![](https://gitee.com/mail_osc/writecode/badge/star.svg?theme=white ) | [代码生成器，自动写代码](https://gitee.com/mail_osc/writecode)  | 
| [log](https://gitee.com/mail_osc/log) | ![](https://gitee.com/mail_osc/log/badge/star.svg?theme=white ) | [Java日志存储及读取](https://gitee.com/mail_osc/log) | 
| [layui translate](https://gitee.com/mail_osc/translate_layui) |  ![](https://gitee.com/mail_osc/translate_layui/badge/star.svg?theme=white ) | [Layui的国际化支持组件](https://gitee.com/mail_osc/translate_layui) |
| [http.java](https://gitee.com/mail_osc/http.java) |  ![](https://gitee.com/mail_osc/http.java/badge/star.svg?theme=white ) | [Java8轻量级http请求类](https://gitee.com/mail_osc/http.java) |
| [xnx3](https://gitee.com/mail_osc/xnx3) |  ![](https://gitee.com/mail_osc/xnx3/badge/star.svg?theme=white ) | [Java版按键精灵，游戏辅助开发](https://gitee.com/mail_osc/xnx3) |  
| [websocket.js](https://gitee.com/mail_osc/websocket.js)  | ![](https://gitee.com/mail_osc/websocket.js/badge/star.svg?theme=white ) | [js的WebSocket框架封装](https://gitee.com/mail_osc/websocket.js) |
| [email.java](https://gitee.com/mail_osc/email.java) | ![](https://gitee.com/mail_osc/email.java/badge/star.svg?theme=white ) | [邮件发送](https://gitee.com/mail_osc/email.java) | 
| [notification.js](https://gitee.com/mail_osc/notification.js) | ![](https://gitee.com/mail_osc/notification.js/badge/star.svg?theme=white ) | [浏览器通知提醒工具类](https://gitee.com/mail_osc/notification.js) | 
| [pinyin.js](https://gitee.com/mail_osc/pinyin.js) | ![](https://gitee.com/mail_osc/pinyin.js/badge/star.svg?theme=white ) | [JS中文转拼音工具类](https://gitee.com/mail_osc/pinyin.js) |
| [xnx3_weixin](https://gitee.com/mail_osc/xnx3_weixin) | ![](https://gitee.com/mail_osc/xnx3_weixin/badge/star.svg?theme=white ) | [Java 微信常用工具类](https://gitee.com/mail_osc/xnx3_weixin) |
| [xunxian](https://gitee.com/mail_osc/xunxian) | ![](https://gitee.com/mail_osc/xunxian/badge/star.svg?theme=white ) | [QQ寻仙的游戏辅助软件](https://gitee.com/mail_osc/xunxian) | 
| [wangmarket_shop](https://gitee.com/leimingyun/wangmarket_shop) | ![](https://gitee.com/leimingyun/wangmarket_shop/badge/star.svg?theme=white ) | [私有化部署自己的 SAAS 商城](https://gitee.com/leimingyun/wangmarket_shop) |
| [wm](https://gitee.com/leimingyun/wm) | ![](https://gitee.com/leimingyun/wm/badge/star.svg?theme=white ) | [Java开发框架及规章约束](https://gitee.com/leimingyun/wm) |
| [yunkefu](https://gitee.com/leimingyun/yunkefu) | ![](https://gitee.com/leimingyun/yunkefu/badge/star.svg?theme=white ) | [私有化部署自己的SAAS客服系统](https://gitee.com/leimingyun/yunkefu) |
| [javadoc](https://gitee.com/leimingyun/javadoc) | ![](https://gitee.com/leimingyun/javadoc/badge/star.svg?theme=white) | [根据标准的 JavaDoc 生成接口文档 ](https://gitee.com/leimingyun/javadoc) |
| [elasticsearch util](https://gitee.com/leimingyun/elasticsearch) | ![](https://gitee.com/leimingyun/elasticsearch/badge/star.svg?theme=white ) | [用sql方式使用Elasticsearch](https://gitee.com/leimingyun/elasticsearch) |
| [AutoPublish](https://gitee.com/leimingyun/sftp-ssh-autopublish) | ![](https://gitee.com/leimingyun/sftp-ssh-autopublish/badge/star.svg?theme=white ) | [Java应用全自动部署及更新](https://gitee.com/leimingyun/sftp-ssh-autopublish) |
| [aichat](https://gitee.com/leimingyun/aichat) | ![](https://gitee.com/leimingyun/aichat/badge/star.svg?theme=white ) | [智能聊天机器人](https://gitee.com/leimingyun/aichat) | 
| [yunbackups](https://gitee.com/leimingyun/yunbackups) | ![](https://gitee.com/leimingyun/yunbackups/badge/star.svg?theme=white ) | [自动备份文件到云存储及FTP等](https://gitee.com/leimingyun/yunbackups) |
| [chatbot](https://gitee.com/leimingyun/chatbot) | ![](https://gitee.com/leimingyun/chatbot/badge/star.svg?theme=white) | [智能客服机器人](https://gitee.com/leimingyun/chatbot) |
| [java print](https://gitee.com/leimingyun/printJframe) | ![](https://gitee.com/leimingyun/printJframe/badge/star.svg?theme=white ) | [Java打印及预览的工具类](https://gitee.com/leimingyun/printJframe) |
…………
